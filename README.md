# kachra_user_app

Kachra Mobile App for Users

## Getting Started

Kchra App was built to simplify the way people get rid of junk, like any unwanted furniture, old appliances, yard waste, or items leftover from a remodelling job etc. Kchra means old furniture, appliances, carpets or any junk that is no longer needed.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
