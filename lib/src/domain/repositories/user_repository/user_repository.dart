import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/data/models/auth/auth_model.dart';
import 'package:kachra_user_app/src/data/models/auth/register_auth_model.dart';

abstract class UserRepository {
  // API methods
  Future<DataState<AuthModel>> login(AuthParams? params);
  Future<DataState<RegisterAuthModel>> register(RegisterAuthParams? registerAuthParams);
  Future<DataState<AuthModel>> loginGoogle(String? params);
  Future<DataState<RegisterAuthModel>> registerUsingGoogle(RegisterUsingGoogleAuthParams? registerUsingGoogleAuthParams);
  Future<DataState<AuthModel>> updateUserPhoneNumber(String? phoneNumber);
  Future<DataState<AuthModel>> registerAsGuest(RegisterAsGuestAuthParams? registerAsGuestAuthParams);
  Future<DataState<AuthModel>> loginPhoneNumber(String? phoneNumber);
}
