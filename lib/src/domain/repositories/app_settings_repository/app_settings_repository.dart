import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/data/models/app_settings/app_settings_model.dart';

abstract class AppSettingsRepository {
  // API methods
  Future<DataState<AppSettingsModel>> getAppSettings();
}
