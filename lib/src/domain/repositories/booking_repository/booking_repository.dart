import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/booking/list_booking_model.dart';
import 'package:kachra_user_app/src/data/models/general_response.dart';
import 'package:kachra_user_app/src/data/models/notifications/notifications_model.dart';

abstract class BookingRepository {
  // API methods
  Future<DataState<BookingModel>> createBooking(BookingParams? params);
  Future<DataState<List<ListBookingModel>>> getBookings(String? userId);
  Future<DataState<GeneralResponse>> acceptDeclineBooking(AcceptDeclineBookingParams? params);
  Future<DataState<GeneralResponse>> setPaymentMethod(SetPaymentBookingParams? params);
  Future<DataState<BookingModel>> findBooking(String? params);
  Future<DataState<List<NotificationsModel>>> getNotifications(String? userId);
  Future<DataState<BookingModel>> setPreferredPickupDate(SetPreferredPickupDateParams? params);
  Future<DataState<BookingModel>> changeBookingPurpose(ChangeBookingPurposeParams? params);
}
