import 'package:kachra_user_app/src/core/params/site_visit_schedule_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/data/models/site_visit_schedule/site_visit_schedule_model.dart';

abstract class SiteVisitRepository {
  // API methods
  Future<DataState<SiteVisitScheduleModel>> createSiteVisitSchedule(SiteVisitScheduleParams? params);
}
