import 'package:kachra_user_app/src/core/params/site_visit_schedule_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/core/usecase/usecase.dart';
import 'package:kachra_user_app/src/data/models/site_visit_schedule/site_visit_schedule_model.dart';
import 'package:kachra_user_app/src/domain/repositories/site_visit_repository/site_visit_repository.dart';

class CreateSiteVisitScheduleUseCase implements UseCase<DataState<SiteVisitScheduleModel>, SiteVisitScheduleParams?> {
  final SiteVisitRepository siteVisitRepository;

  CreateSiteVisitScheduleUseCase(this.siteVisitRepository);

  @override
  Future<DataState<SiteVisitScheduleModel>> call({SiteVisitScheduleParams? params}) {
    return siteVisitRepository.createSiteVisitSchedule(params);
  }
}