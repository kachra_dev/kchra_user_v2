import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/core/usecase/usecase.dart';
import 'package:kachra_user_app/src/data/models/app_settings/app_settings_model.dart';
import 'package:kachra_user_app/src/domain/repositories/app_settings_repository/app_settings_repository.dart';

class AppSettingsUseCase implements UseCase<DataState<AppSettingsModel>, void> {
  final AppSettingsRepository appSettingsRepository;

  AppSettingsUseCase(this.appSettingsRepository);

  @override
  Future<DataState<AppSettingsModel>> call({void params}) {
    return appSettingsRepository.getAppSettings();
  }
}