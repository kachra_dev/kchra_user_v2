import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/core/usecase/usecase.dart';
import 'package:kachra_user_app/src/data/models/auth/auth_model.dart';
import 'package:kachra_user_app/src/data/models/auth/register_auth_model.dart';
import 'package:kachra_user_app/src/domain/repositories/user_repository/user_repository.dart';

class UserLoginUseCase implements UseCase<DataState<AuthModel>, AuthParams?> {
  final UserRepository userRepository;

  UserLoginUseCase(this.userRepository);
  @override
  Future<DataState<AuthModel>> call({AuthParams? params}) {
    return userRepository.login(params);
  }
}

class UserRegisterUseCase implements UseCase<DataState<RegisterAuthModel>, RegisterAuthParams?> {
  final UserRepository userRepository;

  UserRegisterUseCase(this.userRepository);

  @override
  Future<DataState<RegisterAuthModel>> call({RegisterAuthParams? params}) {
    return userRepository.register(params);
  }
}

class UserLoginGoogleUseCase implements UseCase<DataState<AuthModel>, String?> {
  final UserRepository userRepository;

  UserLoginGoogleUseCase(this.userRepository);

  @override
  Future<DataState<AuthModel>> call({String? params}) {
    return userRepository.loginGoogle(params);
  }
}

class UserRegisterUsingGoogleUseCase implements UseCase<DataState<RegisterAuthModel>, RegisterUsingGoogleAuthParams?> {
  final UserRepository userRepository;

  UserRegisterUsingGoogleUseCase(this.userRepository);

  @override
  Future<DataState<RegisterAuthModel>> call({RegisterUsingGoogleAuthParams? params}) {
    return userRepository.registerUsingGoogle(params);
  }
}

class UserRegisterAsGuestUseCase implements UseCase<DataState<AuthModel>, RegisterAsGuestAuthParams?> {
  final UserRepository userRepository;

  UserRegisterAsGuestUseCase(this.userRepository);

  @override
  Future<DataState<AuthModel>> call({RegisterAsGuestAuthParams? params}) {
    return userRepository.registerAsGuest(params);
  }
}

class UpdatePhoneNumberUseCase implements UseCase<DataState<AuthModel>, String?> {
  final UserRepository userRepository;

  UpdatePhoneNumberUseCase(this.userRepository);

  @override
  Future<DataState<AuthModel>> call({String? params}) {
    return userRepository.updateUserPhoneNumber(params);
  }
}

class LoginPhoneNumberUseCase implements UseCase<DataState<AuthModel>, String?> {
  final UserRepository userRepository;

  LoginPhoneNumberUseCase(this.userRepository);

  @override
  Future<DataState<AuthModel>> call({String? params}) {
    return userRepository.loginPhoneNumber(params);
  }
}
