import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/core/usecase/usecase.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/booking/list_booking_model.dart';
import 'package:kachra_user_app/src/data/models/general_response.dart';
import 'package:kachra_user_app/src/data/models/notifications/notifications_model.dart';
import 'package:kachra_user_app/src/domain/repositories/booking_repository/booking_repository.dart';

class CreateBookingUseCase implements UseCase<DataState<BookingModel>, BookingParams?> {
  final BookingRepository bookingRepository;

  CreateBookingUseCase(this.bookingRepository);

  @override
  Future<DataState<BookingModel>> call({BookingParams? params}) {
    return bookingRepository.createBooking(params);
  }
}

class GetBookingsUseCase implements UseCase<DataState<List<ListBookingModel>>, String?> {
  final BookingRepository bookingRepository;

  GetBookingsUseCase(this.bookingRepository);

  @override
  Future<DataState<List<ListBookingModel>>> call({String? params}) {
    return bookingRepository.getBookings(params);
  }
}

class AcceptDeclineBookingUseCase implements UseCase<DataState<GeneralResponse>, AcceptDeclineBookingParams?> {
  final BookingRepository bookingRepository;

  AcceptDeclineBookingUseCase(this.bookingRepository);

  @override
  Future<DataState<GeneralResponse>> call({AcceptDeclineBookingParams? params}) {
    return bookingRepository.acceptDeclineBooking(params);
  }
}

class SetPaymentMethodBookingUseCase implements UseCase<DataState<GeneralResponse>, SetPaymentBookingParams?> {
  final BookingRepository bookingRepository;

  SetPaymentMethodBookingUseCase(this.bookingRepository);

  @override
  Future<DataState<GeneralResponse>> call({SetPaymentBookingParams? params}) {
    return bookingRepository.setPaymentMethod(params);
  }
}

class FindBookingUseCase implements UseCase<DataState<BookingModel>, String?> {
  final BookingRepository bookingRepository;

  FindBookingUseCase(this.bookingRepository);

  @override
  Future<DataState<BookingModel>> call({String? params}) {
    return bookingRepository.findBooking(params);
  }
}

class GetNotificationsUseCase implements UseCase<DataState<List<NotificationsModel>>, String?> {
  final BookingRepository bookingRepository;

  GetNotificationsUseCase(this.bookingRepository);

  @override
  Future<DataState<List<NotificationsModel>>> call({String? params}) {
    return bookingRepository.getNotifications(params);
  }
}

class SetPreferredPickupDateUseCase implements UseCase<DataState<BookingModel>, SetPreferredPickupDateParams?> {
  final BookingRepository bookingRepository;

  SetPreferredPickupDateUseCase(this.bookingRepository);

  @override
  Future<DataState<BookingModel>> call({SetPreferredPickupDateParams? params}) {
    return bookingRepository.setPreferredPickupDate(params);
  }
}

class ChangeBookingPurposeUseCase implements UseCase<DataState<BookingModel>, ChangeBookingPurposeParams?> {
  final BookingRepository bookingRepository;

  ChangeBookingPurposeUseCase(this.bookingRepository);

  @override
  Future<DataState<BookingModel>> call({ChangeBookingPurposeParams? params}) {
    return bookingRepository.changeBookingPurpose(params);
  }
}