part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthLoadingState extends AuthState {
  const AuthLoadingState();
}

class UnauthenticatedState extends AuthState {
  const UnauthenticatedState();
}

class AuthenticatedState extends AuthState {
  const AuthenticatedState();
}

class RegisteredSuccessState extends AuthState {
  const RegisteredSuccessState();
}

class RegisteredErrorState extends AuthState {
  const RegisteredErrorState();
}
