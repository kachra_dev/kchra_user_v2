part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

class LoginAuthEvent extends AuthEvent {
  final AuthParams? authParams;
  final BuildContext context;
  const LoginAuthEvent(this.authParams, this.context);
}

class EnterAsGuestEvent extends AuthEvent {
  final RegisterAsGuestAuthParams? registerAsGuestAuthParams;
  const EnterAsGuestEvent(this.registerAsGuestAuthParams);
}

class LoginGoogleAuthEvent extends AuthEvent {
  final String? params;
  final BuildContext context;
  const LoginGoogleAuthEvent(this.params, this.context);
}

class LoginPhoneNumberEvent extends AuthEvent {
  final String? phoneNumber;
  const LoginPhoneNumberEvent(this.phoneNumber);
}

class GetStoredAuthEvent extends AuthEvent {
  const GetStoredAuthEvent();
}

class RegisterAuthEvent extends AuthEvent {
  final RegisterAuthParams registerAuthParams;
  final BuildContext context;
  const RegisterAuthEvent(this.registerAuthParams, this.context);
}

class RegisterUsingGoogleAuthEvent extends AuthEvent {
  final RegisterUsingGoogleAuthParams registerUsingGoogleAuthParams;
  final BuildContext context;
  const RegisterUsingGoogleAuthEvent(this.registerUsingGoogleAuthParams, this.context);
}

class RegisterAsGuestAuthEvent extends AuthEvent {
  final RegisterAsGuestAuthParams registerAsGuestAuthParams;
  const RegisterAsGuestAuthEvent(this.registerAsGuestAuthParams);
}

class UpdateUserPhoneNumberEvent extends AuthEvent{
  final String? phoneNumber;
  const UpdateUserPhoneNumberEvent(this.phoneNumber);
}

class LogOutEvent extends AuthEvent{
  const LogOutEvent();
}