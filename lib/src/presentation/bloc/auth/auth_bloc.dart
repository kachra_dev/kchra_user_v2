import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/add_hive.dart';
import 'package:kachra_user_app/src/core/utils/hive/edit_hive.dart';
import 'package:kachra_user_app/src/core/utils/log_out/log_out.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/core/utils/messages/success_message.dart';
import 'package:kachra_user_app/src/core/utils/service/firebase_service.dart';
import 'package:kachra_user_app/src/core/utils/shared_preferences/get_preference.dart';
import 'package:kachra_user_app/src/core/utils/shared_preferences/store_preference.dart';
import 'package:kachra_user_app/src/data/models/auth/auth_model.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/data/models/profile/profile_model.dart';
import 'package:kachra_user_app/src/domain/usecases/user_usecase.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/auth_checker.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/login/guest/enter_as_guest.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/registration/activation_code_checker.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserLoginUseCase userLoginUseCase;
  final UserRegisterUseCase userRegisterUseCase;
  final UserLoginGoogleUseCase userLoginGoogleUseCase;
  final UserRegisterUsingGoogleUseCase userRegisterUsingGoogleUseCase;
  final UserRegisterAsGuestUseCase userRegisterAsGuestUseCase;
  final UpdatePhoneNumberUseCase updatePhoneNumberUseCase;
  final LoginPhoneNumberUseCase loginPhoneNumberUseCase;
  AuthBloc(this.userLoginUseCase, this.userRegisterUseCase, this.userLoginGoogleUseCase, this.userRegisterUsingGoogleUseCase, this.userRegisterAsGuestUseCase, this.updatePhoneNumberUseCase, this.loginPhoneNumberUseCase)
      : super(const AuthLoadingState());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is LoginAuthEvent) {
      yield* _loginAuth(event.authParams);
    }

    if (event is LoginGoogleAuthEvent) {
      yield* _loginGoogleAuth(event.params);
    }

    if (event is GetStoredAuthEvent) {
      yield* _getStoredAuthEvent();
    }

    if (event is RegisterAuthEvent) {
      yield* _registerAuth(event.registerAuthParams);
    }

    if (event is RegisterUsingGoogleAuthEvent) {
      yield* _registerUsingGoogleAuth(event.registerUsingGoogleAuthParams);
    }

    if(event is EnterAsGuestEvent){
      yield* _enterAsGuest(event.registerAsGuestAuthParams);
    }

    if(event is UpdateUserPhoneNumberEvent){
      yield* _updateUserPhoneNumber(event.phoneNumber);
    }

    if(event is LoginPhoneNumberEvent){
      yield* _loginPhoneNumberAuth(event.phoneNumber);
    }

    if(event is LogOutEvent){
      yield* _logOut();
    }
  }

  Stream<AuthState> _loginAuth(AuthParams? authParams) async* {
    yield const AuthLoadingState();

    final dataState = await userLoginUseCase(params: authParams);

    if (authParams != null) {
      if (dataState.data != null) {
        String accessToken = dataState.data!.accessToken!;
        ProfileModel? profile = dataState.data!.user;

        String? role = dataState.data!.user!.role;

        if(role != 'Client'){
          showDialog(
              barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.error_logging_in_user_is_not_a_client.tr(),
                // onPressedFunction: () => Navigator.pop(context)
              )
          );

          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

          Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
          yield const UnauthenticatedState();
        } else {
          ///add accessToken
          StorePreference().storeAccessToken(accessToken);
          ///add user
          Map<String, dynamic> profileJson = profile!.toJson();
          ProfileHive profileHive = ProfileHive.fromJson(profileJson);

          AddHive().addUser(profileHive);

          final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
          bookingBloc.add(const GetBookingsEvent());

          showDialog(
            barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: '${LocaleKeys.successfully_logged_in.tr()}. \n${LocaleKeys.email.tr()}: ${profileHive.email} \n${LocaleKeys.password.tr()}: **********',
                // onPressedFunction: (){
                //   Navigator.of(context, rootNavigator: true).pop();
                //
                //   final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
                //   bookingBloc.add(const GetBookingsEvent());
                //
                // }
              )
          );

          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

          Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

          yield const AuthenticatedState();
        }
      } else {
        Map<String, dynamic> mapError = dataState.error!.response!.data! as Map<String, dynamic>;
        List<dynamic> errorValues = mapError.values.toList().map((e) => e.toString()).toList();

        if(dataState.error!.response!.statusCode == 401){
          showDialog(
              barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => ErrorMessage(
                title: 'Unauthorized:',
                content: errorValues.toString(),
                // onPressedFunction: (){
                //   Navigator.push(context, MaterialPageRoute(builder: (context) => ActivationCodeChecker(authParams: authParams)));
                // }
              )
          );
          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

          Navigator.push(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => ActivationCodeChecker(authParams: authParams)));
          yield const UnauthenticatedState();
        } else {
          Widget errorMessage = TextButton(
              onPressed: () async {
                showDialog(
                  barrierDismissible: false,
                    context: MyApp.navigatorKey.currentContext!,
                    builder: (context) => ErrorMessage(
                      title: LocaleKeys.error.tr(),
                      content: errorValues.toString(),
                      // onPressedFunction: () => Navigator.pop(context)
                    ));

                await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

                Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
              },
              child: const Text('View error message.')
          );

          showDialog(
            barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => Directionality(
                textDirection: ltrTextDirection,
                child: ErrorMessage(
                  title: LocaleKeys.error.tr(),
                  content: LocaleKeys.error_logging_in.tr(),
                  errorMessage: errorMessage,
                  // onPressedFunction: () => Navigator.pop(context)
                ),
              ));

          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

          Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
          
          yield const UnauthenticatedState();
        }
      }
    } else {
      showDialog(
        barrierDismissible: false,
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => ErrorMessage(
            title: LocaleKeys.error.tr(),
            content: LocaleKeys.error_logging_in_please_check_your_internet_connection_or_contact_your_administrator.tr(),
            // onPressedFunction: () => Navigator.pop(context)
          )
      );
      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

      Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
      
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _loginPhoneNumberAuth(String? phoneNumber) async* {
    yield const AuthLoadingState();

    final dataState = await loginPhoneNumberUseCase(params: phoneNumber);

    if (phoneNumber != null) {
      if (dataState.data != null) {
        String accessToken = dataState.data!.accessToken!;
        ProfileModel? profile = dataState.data!.user;

        String? role = dataState.data!.user!.role;

        if(role != 'Client'){
          showDialog(
            barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.error_logging_in_user_is_not_a_client.tr(),
                // onPressedFunction: () => Navigator.pop(context)
              )
          );

          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

          Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
          yield const UnauthenticatedState();
        } else {
          ///add accessToken
          StorePreference().storeAccessToken(accessToken);
          ///add user
          Map<String, dynamic> profileJson = profile!.toJson();
          ProfileHive profileHive = ProfileHive.fromJson(profileJson);

          AddHive().addUser(profileHive);

          final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
          bookingBloc.add(const GetBookingsEvent());

          yield const AuthenticatedState();
        }
      } else {
        Map<String, dynamic> mapError = dataState.error!.response!.data! as Map<String, dynamic>;
        List<dynamic> errorValues = mapError.values.toList().map((e) => e.toString()).toList();

        Widget errorMessage = TextButton(
            onPressed: () async {
              showDialog(
                barrierDismissible: false,
                  context: MyApp.navigatorKey.currentContext!,
                  builder: (context) => ErrorMessage(
                    title: LocaleKeys.error.tr(),
                    content: errorValues.toString(),
                    // onPressedFunction: () => Navigator.pop(context)
                  )
              );

              await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

              Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
            },
            child: const Text('View error message.')
        );

        showDialog(
          barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => Directionality(
              textDirection: ltrTextDirection,
              child: ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.error_logging_in.tr(),
                errorMessage: errorMessage,
                // onPressedFunction: () => Navigator.pop(context)
              ),
            ));

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
        
        yield const UnauthenticatedState();
      }
    } else {
      showDialog(
        barrierDismissible: false,
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => ErrorMessage(
            title: LocaleKeys.error.tr(),
            content: LocaleKeys.error_logging_in_please_check_your_internet_connection_or_contact_your_administrator.tr(),
            // onPressedFunction: () => Navigator.pop(context)
          )
      );
      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

      Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
      
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _enterAsGuest(RegisterAsGuestAuthParams? registerAsGuestAuthParams) async* {
    yield const AuthLoadingState();

    if(registerAsGuestAuthParams != null){
      final dataState = await userRegisterAsGuestUseCase(params: registerAsGuestAuthParams);

      if(dataState.data != null){
        AuthModel authModel = dataState.data!;
        ProfileModel profile = authModel.user!;

        ///add accessToken
        StorePreference().storeAccessToken(profile.id!);

        ///add user
        Map<String, dynamic> profileJson = profile.toJson();
        ProfileHive profileHive = ProfileHive.fromJson(profileJson);

        AddHive().addUser(profileHive);

        final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
        bookingBloc.add(const GetBookingsEvent());

        yield const AuthenticatedState();
      } else {
        // Map<String, dynamic> mapError = dataState.error!.response!.data! as Map<String, dynamic>;
        // List<dynamic> errorValues = mapError.values.toList().map((e) => e.toString()).toList();

        showDialog(
          barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => Directionality(
              textDirection: ltrTextDirection,
              child: ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.phone_number_already_exists.tr(),
                // errorMessage: errorMessage,
                // onPressedFunction: () => pushNewScreen(context, screen: const EnterAsNewGuestRequest(), withNavBar: false)
              ),
            ));

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

        pushNewScreen(MyApp.navigatorKey.currentContext!, screen: const EnterAsNewGuestRequest(), withNavBar: false);
        
        yield const UnauthenticatedState();
      }
    } else {
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _loginGoogleAuth(String? params) async* {
    yield const AuthLoadingState();

    final dataState = await userLoginGoogleUseCase(params: params);

    if (params != null) {
      if (dataState.data != null) {
        String accessToken = dataState.data!.accessToken!;
        ProfileModel? profile = dataState.data!.user;

        String? role = dataState.data!.user!.role;

        if(role != 'Client'){
          showDialog(
            barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.error_logging_in_user_is_not_a_client.tr(),
                // onPressedFunction: () => Navigator.pop(context)
              ));
          await Future.delayed(const Duration(seconds:  futureDelayTimeConst), (){});

          Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
          
          await FirebaseService().signOutFromGoogle();
          yield const UnauthenticatedState();
        } else {
          ///add accessToken
          StorePreference().storeAccessToken(accessToken);
          ///add user
          Map<String, dynamic> profileJson = profile!.toJson();
          ProfileHive profileHive = ProfileHive.fromJson(profileJson);

          AddHive().addUser(profileHive);

          showDialog(
            barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: '${LocaleKeys.successfully_logged_in.tr()}. \n${LocaleKeys.email.tr()}: ${profileHive.email} \n${LocaleKeys.password.tr()}: **********',
                // onPressedFunction: (){
                //   Navigator.of(context, rootNavigator: true).pop();
                //
                //   final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
                //   bookingBloc.add(const GetBookingsEvent());
                //
                // }
              )
          );

          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

          Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

          final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
          bookingBloc.add(const GetBookingsEvent());
          
          yield const AuthenticatedState();
        }
      } else {
        Map<String, dynamic> mapError = dataState.error!.response!.data! as Map<String, dynamic>;
        List<dynamic> errorValues = mapError.values.toList().map((e) => e.toString()).toList();

        Widget errorMessage = TextButton(
            onPressed: () async {
              showDialog(
                barrierDismissible: false,
                  context: MyApp.navigatorKey.currentContext!,
                  builder: (context) => ErrorMessage(
                    title: LocaleKeys.error.tr(),
                    content: errorValues.toString(),
                    // onPressedFunction: () => Navigator.pop(context)
                  ));

              await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

              Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
            },
            child: const Text('View error message.')
        );

        showDialog(
          barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => Directionality(
              textDirection: ltrTextDirection,
              child: ErrorMessage(
                title: LocaleKeys.error.tr(),
                content: LocaleKeys.error_logging_in.tr(),
                errorMessage: errorMessage,
                // onPressedFunction: () => Navigator.pop(context)
              ),
            ));

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

        await FirebaseService().signOutFromGoogle();
        yield const UnauthenticatedState();
      }
    } else {
      showDialog(
        barrierDismissible: false,
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => ErrorMessage(
            title: LocaleKeys.error.tr(),
            content: LocaleKeys.error_logging_in_please_check_your_internet_connection_or_contact_your_administrator.tr(),
            // onPressedFunction: () => Navigator.pop(context)
          ));

      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

      Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

      await FirebaseService().signOutFromGoogle();
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _getStoredAuthEvent() async* {
    String? accessToken = await GetPreference().getAccessToken();

    if(accessToken != null){
      yield const AuthenticatedState();
    } else {
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _registerAuth(RegisterAuthParams? registerAuthParams) async* {
    yield const AuthLoadingState();

    final dataState = await userRegisterUseCase(params: registerAuthParams);

    if (dataState.data != null) {
      showDialog(
        barrierDismissible: false,
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => SuccessMessage(
            title: LocaleKeys.success.tr(),
            content: 'Successfully registered user!',
            // onPressedFunction: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker()))
          )
      );

      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

      Navigator.pushReplacement(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => const AuthChecker()));

      yield const RegisteredSuccessState();
    } else {
      showDialog(
        barrierDismissible: false,
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => ErrorMessage(
            title: LocaleKeys.error.tr(),
            content: LocaleKeys.this_account_already_exists_please_try_another_email_or_fill_in_all_the_fields_correctly.tr(),
            // onPressedFunction: () => Navigator.pop(context)
          ));
      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

      Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

      yield const RegisteredErrorState();
    }
  }

  Stream<AuthState> _registerUsingGoogleAuth(RegisterUsingGoogleAuthParams? registerUsingGoogleAuthParams) async* {
    yield const AuthLoadingState();

    final dataState = await userRegisterUsingGoogleUseCase(params: registerUsingGoogleAuthParams);

    if (dataState.data != null) {
      showDialog(
        barrierDismissible: false,
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => SuccessMessage(
            title: LocaleKeys.success.tr(),
            content: 'Successfully registered user!',
            // onPressedFunction: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker()))
          )
      );

      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

      Navigator.pushReplacement(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => const AuthChecker()));

      yield const RegisteredSuccessState();
    } else {
      showDialog(
        barrierDismissible: false,
          context: MyApp.navigatorKey.currentContext!,
          builder: (context) => ErrorMessage(
            title: LocaleKeys.error.tr(),
            content: LocaleKeys.this_account_already_exists_please_try_another_email_or_fill_in_all_the_fields_correctly.tr(),
            // onPressedFunction: () => Navigator.pop(context)
          ));
      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

      Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

      yield const RegisteredErrorState();
    }
  }

  Stream<AuthState> _updateUserPhoneNumber(String? phoneNumber) async*{
    yield const AuthLoadingState();

    if(phoneNumber != null){
      final dataState = await updatePhoneNumberUseCase(params: phoneNumber);

      if(dataState.data != null){
        AuthModel authModel = dataState.data!;
        ProfileModel profile = authModel.user!;

        Map<String, dynamic> profileJson = profile.toJson();
        ProfileHive profileHive = ProfileHive.fromJson(profileJson);

        ///edit user; add phone number to current profile
        EditHive().editUser(profileHive);

        yield const AuthenticatedState();

        showDialog(
            barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => SuccessMessage(
              title: LocaleKeys.success.tr(),
              content: LocaleKeys.successfully_updated_phone_number.tr(),
              // onPressedFunction: () => Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst)
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){});

        Navigator.popUntil(MyApp.navigatorKey.currentContext!, (Route<dynamic> route) => route.isFirst);

      } else {
        yield const UnauthenticatedState();

        showDialog(
          barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => ErrorMessage(
              title: LocaleKeys.error.tr(),
              content: 'Error updating phone number',
              // onPressedFunction: () => Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst)
            ));

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.popUntil(MyApp.navigatorKey.currentContext!, (Route<dynamic> route) => route.isFirst);
      }
    } else {
      yield const UnauthenticatedState();
    }
  }

  Stream<AuthState> _logOut() async*{
    await LogOut().logOut();
    Navigator.popUntil(MyApp.navigatorKey.currentContext!, (Route<dynamic> route) => route.isFirst);
    yield const UnauthenticatedState();
  }
}
