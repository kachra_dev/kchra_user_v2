part of 'app_settings_bloc.dart';

abstract class AppSettingsState extends Equatable {
  final AppSettingsModel? appSettingsModelString;
  const AppSettingsState({this.appSettingsModelString});

  @override
  List<Object?> get props => [];
}

class AppSettingsInitial extends AppSettingsState {
  const AppSettingsInitial();
}

class LoadingAppSettingsState extends AppSettingsState{
  const LoadingAppSettingsState();
}

class SuccessAppSettingsState extends AppSettingsState{
  final AppSettingsModel? appSettingsModel;
  const SuccessAppSettingsState(this.appSettingsModel) : super(appSettingsModelString: appSettingsModel);
}

class ErrorAppSettingsState extends AppSettingsState{
  const ErrorAppSettingsState();
}