import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kachra_user_app/src/data/models/app_settings/app_settings_model.dart';
import 'package:kachra_user_app/src/domain/usecases/app_settings_usecase.dart';

part 'app_settings_event.dart';
part 'app_settings_state.dart';

class AppSettingsBloc extends Bloc<AppSettingsEvent, AppSettingsState> {
  final AppSettingsUseCase appSettingsUseCase;
  AppSettingsBloc(this.appSettingsUseCase) : super(const AppSettingsInitial());

  @override
  Stream<AppSettingsState> mapEventToState(AppSettingsEvent event) async* {
    if(event is GetAppSettingsEvent){
      yield* _getAppSettings();
    }
  }

  Stream<AppSettingsState> _getAppSettings() async*{
    yield const LoadingAppSettingsState();

    final dataState = await appSettingsUseCase();

    if(dataState.data != null){
      AppSettingsModel appSettings = dataState.data!;

      yield SuccessAppSettingsState(appSettings);
    } else {
      yield const ErrorAppSettingsState();
    }
  }
}
