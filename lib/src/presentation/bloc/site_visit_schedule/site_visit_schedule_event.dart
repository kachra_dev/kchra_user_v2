part of 'site_visit_schedule_bloc.dart';

abstract class SiteVisitScheduleEvent extends Equatable {
  const SiteVisitScheduleEvent();

  @override
  List<Object?> get props => [];
}

class CreateSiteVisitScheduleEvent extends SiteVisitScheduleEvent{
  final SiteVisitScheduleParams? siteVisitScheduleParams;
  const CreateSiteVisitScheduleEvent(this.siteVisitScheduleParams);
}
