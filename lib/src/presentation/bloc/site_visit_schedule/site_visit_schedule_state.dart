part of 'site_visit_schedule_bloc.dart';

abstract class SiteVisitScheduleState extends Equatable {
  final SiteVisitScheduleModel? siteVisitSchedule;
  const SiteVisitScheduleState({this.siteVisitSchedule});

  @override
  List<Object?> get props => [];
}

class SiteVisitScheduleInitial extends SiteVisitScheduleState {
  const SiteVisitScheduleInitial();
}

class LoadingSiteVisitScheduleState extends SiteVisitScheduleState {
  const LoadingSiteVisitScheduleState();
}

class SuccessSiteVisitScheduleState extends SiteVisitScheduleState {
  final SiteVisitScheduleModel? siteVisitScheduleModel;
  const SuccessSiteVisitScheduleState(this.siteVisitScheduleModel) : super(siteVisitSchedule: siteVisitScheduleModel);
}

class ErrorSiteVisitScheduleState extends SiteVisitScheduleState {
  const ErrorSiteVisitScheduleState();
}
