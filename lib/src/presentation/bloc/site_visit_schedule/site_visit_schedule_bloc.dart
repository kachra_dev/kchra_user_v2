import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/site_visit_schedule_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/core/utils/messages/success_message.dart';
import 'package:kachra_user_app/src/data/models/site_visit_schedule/site_visit_schedule_model.dart';
import 'package:kachra_user_app/src/domain/usecases/site_visit_repository_usecase.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

part 'site_visit_schedule_event.dart';
part 'site_visit_schedule_state.dart';

class SiteVisitScheduleBloc extends Bloc<SiteVisitScheduleEvent, SiteVisitScheduleState> {
  final CreateSiteVisitScheduleUseCase createSiteVisitScheduleUseCase;

  SiteVisitScheduleBloc(this.createSiteVisitScheduleUseCase)
      : super(const SiteVisitScheduleInitial());

  @override
  Stream<SiteVisitScheduleState> mapEventToState(
      SiteVisitScheduleEvent event) async* {
    if (event is CreateSiteVisitScheduleEvent) {
      yield* _createSiteVisitSchedule(event.siteVisitScheduleParams);
    }
  }

  Stream<SiteVisitScheduleState> _createSiteVisitSchedule(SiteVisitScheduleParams? siteVisitScheduleParams) async* {
    yield const LoadingSiteVisitScheduleState();

    if (siteVisitScheduleParams != null) {
      final dataState = await createSiteVisitScheduleUseCase(
          params: siteVisitScheduleParams);

      if (dataState.data != null) {
        SiteVisitScheduleModel siteVisitScheduleModel = dataState.data!;

        showDialog(
            context: MyApp.navigatorKey.currentContext!,
            barrierDismissible: false,
            builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: 'Successfully Scheduled site visit with an Estimator.',
              // onPressedFunction: () => Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst)
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.popUntil(MyApp.navigatorKey.currentContext!, (Route<dynamic> route) => route.isFirst);

        yield SuccessSiteVisitScheduleState(siteVisitScheduleModel);
      } else {
        showDialog(
            context: MyApp.navigatorKey.currentContext!,
            barrierDismissible: false,
            builder: (context) => ErrorMessage(title: LocaleKeys.error.tr(), content: 'Error scheduling site visit.',
              // onPressedFunction: (){
              //   Navigator.pop(context);
              //   Navigator.pop(context);
              // }
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

        yield const ErrorSiteVisitScheduleState();
      }
    } else {
      yield const ErrorSiteVisitScheduleState();
    }
  }
}