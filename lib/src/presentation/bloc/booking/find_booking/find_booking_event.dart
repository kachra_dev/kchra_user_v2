part of 'find_booking_bloc.dart';

abstract class FindBookingEvent extends Equatable {
  const FindBookingEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class FindBooking extends FindBookingEvent{
  final String? bookingId;
  const FindBooking(this.bookingId);
}
