import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kachra_user_app/src/core/utils/temp_value_holder.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';

part 'find_booking_event.dart';
part 'find_booking_state.dart';

class FindBookingBloc extends Bloc<FindBookingEvent, FindBookingState> {
  final FindBookingUseCase findBookingUseCase;
  FindBookingBloc(this.findBookingUseCase) : super(const FindBookingInitial());

  @override
  Stream<FindBookingState> mapEventToState(FindBookingEvent event) async* {
    if(event is FindBooking){
      yield* _findBookingEvent(event.bookingId);
    }
  }

  Stream<FindBookingState> _findBookingEvent(String? params) async*{
    yield const LoadingFindBookingState();

    if(params != null){
      final dataState = await findBookingUseCase(params: params);

      if(dataState.data != null){
        BookingModel bookingModel = dataState.data!;

        TempValueHolderHelper.tempValue.value = TempValueHolder(params);

        yield SuccessFindBookingState(bookingModel);
      } else {
        yield const ErrorFindBookingState();
      }
    } else {
      yield const ErrorFindBookingState();
    }
  }
}
