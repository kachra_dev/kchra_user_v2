part of 'find_booking_bloc.dart';

abstract class FindBookingState extends Equatable {
  final BookingModel? bookingModelList;
  const FindBookingState({this.bookingModelList});

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class FindBookingInitial extends FindBookingState {
  const FindBookingInitial();
}

class LoadingFindBookingState extends FindBookingState{
  const LoadingFindBookingState();
}

class SuccessFindBookingState extends FindBookingState{
  final BookingModel? bookingModel;
  const SuccessFindBookingState(this.bookingModel) : super(bookingModelList: bookingModel);
}

class ErrorFindBookingState extends FindBookingState{
  const ErrorFindBookingState();
}