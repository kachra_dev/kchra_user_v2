import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/messages/success_message.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/payment/set_preferred_pickup_date.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

part 'accept_decline_booking_event.dart';
part 'accept_decline_booking_state.dart';

class AcceptDeclineBookingBloc extends Bloc<AcceptDeclineBookingEvent, AcceptDeclineBookingState> {
  final AcceptDeclineBookingUseCase acceptDeclineBookingUseCase;

  AcceptDeclineBookingBloc(this.acceptDeclineBookingUseCase) : super(const AcceptDeclineBookingInitial());

  @override
  Stream<AcceptDeclineBookingState> mapEventToState(AcceptDeclineBookingEvent event) async* {
    if (event is AcceptDeclineEvent) {
      yield* _acceptDeclineBooking(event.params, event.bookingModel);
    }

    if(event is InitialEvent){
      yield* _initialEvent();
    }

    if(event is LoadingEvent){
      yield* _loadingEvent();
    }

    if(event is ErrorEvent){
      yield* _errorEvent();
    }
  }

  Stream<AcceptDeclineBookingState> _initialEvent() async*{
    yield const AcceptDeclineBookingInitial();
  }

  Stream<AcceptDeclineBookingState> _loadingEvent() async*{
    yield const LoadingBookingAcceptDeclineState();
  }

  Stream<AcceptDeclineBookingState> _errorEvent() async*{
    yield const ErrorBookingAcceptDeclineState();
  }

  Stream<AcceptDeclineBookingState> _acceptDeclineBooking(AcceptDeclineBookingParams? params, BookingModel? bookingModel) async*{
    if(params != null){
      yield const LoadingBookingAcceptDeclineState();
      final dataState = await acceptDeclineBookingUseCase(params: params);

      if(dataState.data != null){

        if(params.bookingQuoteStatus == 'Paid'){
          showDialog(
              barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: LocaleKeys.successfully_paid_QAR_thank_you.tr(args: [params.amount.toString()]),
                // onPressedFunction: () => Navigator.of(context, rootNavigator: true).pop()
              )
          );

          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

          });

          Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

          if(bookingModel != null){
            Navigator.pushReplacement(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => SetPreferredPickupDate(booking: bookingModel)));
            // pushNewScreen(MyApp.navigatorKey.currentContext!, screen: SetPreferredPickupDate(booking: bookingModel));
          }

        } else if (params.bookingQuoteStatus == 'Declined'){
          showDialog(
              barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: LocaleKeys.successfully_declined_the_estimate_amount,
                // onPressedFunction: (){
                //   final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
                //   bookingBloc.add(const GetBookingsEvent());
                //
                //   final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                //   findBookingBloc.add(FindBooking(params.bookingId));
                //
                //   Navigator.pop(context);
                //   Navigator.pop(context);
                // }
              )
          );

          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

          });

          final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
          bookingBloc.add(const GetBookingsEvent());

          final findBookingBloc = BlocProvider.of<FindBookingBloc>(MyApp.navigatorKey.currentContext!);
          findBookingBloc.add(FindBooking(params.bookingId));

          Navigator.pop(MyApp.navigatorKey.currentContext!);
          Navigator.pop(MyApp.navigatorKey.currentContext!);

        } else if (params.bookingQuoteStatus == 'Accepted'){
          showDialog(
              barrierDismissible: false,
              context: MyApp.navigatorKey.currentContext!,
              builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: LocaleKeys.successfully_accepted_estimate_amount.tr(),
                // onPressedFunction: (){
                //   final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
                //   bookingBloc.add(const GetBookingsEvent());
                //
                //   final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                //   findBookingBloc.add(FindBooking(params.bookingId));
                //
                //   Navigator.pop(context);
                //   Navigator.pop(context);
                // }
              )
          );
          await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

          });

          final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
          bookingBloc.add(const GetBookingsEvent());

          final findBookingBloc = BlocProvider.of<FindBookingBloc>(MyApp.navigatorKey.currentContext!);
          findBookingBloc.add(FindBooking(params.bookingId));

          Navigator.pop(MyApp.navigatorKey.currentContext!);
          Navigator.pop(MyApp.navigatorKey.currentContext!);
        }
        yield const SuccessBookingAcceptDeclineState();
      } else {
        yield const ErrorBookingAcceptDeclineState();
      }
    } else {
      yield const ErrorBookingAcceptDeclineState();
    }
  }
}
