part of 'accept_decline_booking_bloc.dart';

abstract class AcceptDeclineBookingEvent extends Equatable {
  const AcceptDeclineBookingEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];

  @override
  // TODO: implement stringify
  bool? get stringify => true;
}

class AcceptDeclineEvent extends AcceptDeclineBookingEvent{
  final AcceptDeclineBookingParams? params;
  final BookingModel? bookingModel;
  const AcceptDeclineEvent({this.params, this.bookingModel});
}

class InitialEvent extends AcceptDeclineBookingEvent{
  const InitialEvent();
}

class LoadingEvent extends AcceptDeclineBookingEvent{
  const LoadingEvent();
}

class ErrorEvent extends AcceptDeclineBookingEvent{
  const ErrorEvent();
}