part of 'accept_decline_booking_bloc.dart';

abstract class AcceptDeclineBookingState extends Equatable {
  const AcceptDeclineBookingState();

  @override
  List<Object> get props => [];

  @override
  bool? get stringify => true;
}

class AcceptDeclineBookingInitial extends AcceptDeclineBookingState {
  const AcceptDeclineBookingInitial();
}

class LoadingBookingAcceptDeclineState extends AcceptDeclineBookingState {
  const LoadingBookingAcceptDeclineState();
}

class SuccessBookingAcceptDeclineState extends AcceptDeclineBookingState{
  const SuccessBookingAcceptDeclineState();
}

class ErrorBookingAcceptDeclineState extends AcceptDeclineBookingState{
  const ErrorBookingAcceptDeclineState();
}