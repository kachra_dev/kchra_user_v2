import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kachra_user_app/src/core/utils/helper/token_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/booking/list_booking_model.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';

part 'get_booking_event.dart';
part 'get_booking_state.dart';

class GetBookingBloc extends Bloc<GetBookingEvent, GetBookingState> {
  final GetBookingsUseCase getBookingsUseCase;

  GetBookingBloc(this.getBookingsUseCase) : super(const GetBookingInitial());

  @override
  Stream<GetBookingState> mapEventToState(GetBookingEvent event) async* {
    if (event is GetBookingsEvent) {
      yield* _getBookings();
    }
  }

  Stream<GetBookingState> _getBookings() async*{
    yield const LoadingBookingState();

    ProfileHive profile = await GetHive().getCurrentUserProfile();

    final dataState = await getBookingsUseCase(params: profile.id);

    if(dataState.data != null){
      List<ListBookingModel> bookingList = dataState.data!;
      yield SuccessGetBookingState(bookingList);
    } else {
      yield const ErrorGetBookingState();
    }
  }
}
