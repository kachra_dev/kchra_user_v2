part of 'get_booking_bloc.dart';

abstract class GetBookingState extends Equatable {
  final List<ListBookingModel>? bookingModel;
  const GetBookingState({this.bookingModel});

  @override
  List<Object?> get props => [];
}

class GetBookingInitial extends GetBookingState {
  const GetBookingInitial();
}

class LoadingBookingState extends GetBookingState {
  const LoadingBookingState();
}

class SuccessGetBookingState extends GetBookingState{
  final List<ListBookingModel>? bookingList;
  const SuccessGetBookingState(this.bookingList):super(bookingModel: bookingList);
}

class ErrorGetBookingState extends GetBookingState{
  const ErrorGetBookingState();
}