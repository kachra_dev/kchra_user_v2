part of 'get_booking_bloc.dart';

abstract class GetBookingEvent extends Equatable {
  const GetBookingEvent();

  @override
  List<Object?> get props => [];
}
class GetBookingsEvent extends GetBookingEvent{
  const GetBookingsEvent();
}