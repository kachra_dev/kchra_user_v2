import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/hive/add_hive.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/core/utils/messages/success_message.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/hive/locally_saved_booking_assets_hive.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';
import 'package:kachra_user_app/src/presentation/views/auth_checker.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/dashboard.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';

import 'get_booking/get_booking_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
part 'booking_event.dart';
part 'booking_state.dart';

class BookingBloc extends Bloc<BookingEvent, BookingState> {
  final CreateBookingUseCase createBookingUseCase;
  final GetBookingsUseCase getBookingsUseCase;

  BookingBloc(this.createBookingUseCase, this.getBookingsUseCase) : super(const LoadingBookingState());

  @override
  Stream<BookingState> mapEventToState(BookingEvent event) async* {
    if(event is InitialBookingEvent){
      yield* _initialBookingEvent();
    }

    if (event is CreateBookingEvent) {
      yield* _createBooking(event.bookingParams, event.assetBytes);
    }
  }

  Stream<BookingState> _initialBookingEvent() async*{
    yield const ErrorCreateBookingState();
  }

  Stream<BookingState> _createBooking(BookingParams? bookingParams, String? assetBytes) async*{
    yield const LoadingBookingState();

    final dataState = await createBookingUseCase(params: bookingParams);

    if(dataState.data != null){
      yield const SuccessCreateBookingState();

      showDialog(
          context: MyApp.navigatorKey.currentContext!,
          barrierDismissible: false,
          builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: LocaleKeys.successfully_sent_kchra.tr(),
            // onPressedFunction: (){
            //   // Navigator.of(context, rootNavigator: true).pop();
            //
            //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const Dashboard(bottomNavbarIndex: 0)));
            //   // Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
            //
            //   final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
            //   bookingBloc.add(const GetBookingsEvent());
            // }
          )
      );

      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

      });

      Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

      Navigator.pushReplacement(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => const Dashboard(bottomNavbarIndex: 0)));
      // Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);

      final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
      bookingBloc.add(const GetBookingsEvent());

      BookingModel? booking = dataState.data;

      LocallySavedBookingAssetsHive locallySavedBookingAssetsHive = LocallySavedBookingAssetsHive(
          bookingId: booking!.id,
          asset: assetBytes,
          assetType: booking.assetType
      );

      AddHive().addLocallySavedBookingAsset(locallySavedBookingAssetsHive);
    } else {
      if(bookingParams == null){
        yield const InitialCreateBookingState();
      } else {
        yield const ErrorCreateBookingState();

        showDialog(
            context: MyApp.navigatorKey.currentContext!,
            barrierDismissible: false,
            builder: (context) => ErrorMessage(title: LocaleKeys.error.tr(), content: LocaleKeys.error_sending_data_to_the_server.tr(),
              // onPressedFunction: (){
              //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker(bottomNavbarIndex: 0)));
              // }
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.pushReplacement(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => const AuthChecker(bottomNavbarIndex: 0)));
      }
    }
  }
}
