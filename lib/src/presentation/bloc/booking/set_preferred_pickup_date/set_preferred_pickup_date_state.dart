part of 'set_preferred_pickup_date_bloc.dart';

abstract class SetPreferredPickupDateState extends Equatable {
  const SetPreferredPickupDateState();

  @override
  List<Object?> get props => [];
}

class SetPreferredPickupDateInitial extends SetPreferredPickupDateState {
  const SetPreferredPickupDateInitial();
}

class LoadingSetPreferredPickupDateState extends SetPreferredPickupDateState{
  const LoadingSetPreferredPickupDateState();
}

class SuccessSetPreferredPickupDateState extends SetPreferredPickupDateState{
  const SuccessSetPreferredPickupDateState();
}

class ErrorSetPreferredPickupDateState extends SetPreferredPickupDateState{
  const ErrorSetPreferredPickupDateState();
}