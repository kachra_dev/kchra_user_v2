import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/core/utils/messages/success_message.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/auth_checker.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

part 'set_preferred_pickup_date_event.dart';
part 'set_preferred_pickup_date_state.dart';

class SetPreferredPickupDateBloc extends Bloc<SetPreferredPickupDateEvent, SetPreferredPickupDateState> {
  final SetPreferredPickupDateUseCase setPreferredPickupDateUseCase;
  SetPreferredPickupDateBloc(this.setPreferredPickupDateUseCase) : super(const SetPreferredPickupDateInitial());

  @override
  Stream<SetPreferredPickupDateState> mapEventToState(SetPreferredPickupDateEvent event) async* {
    if(event is SetPreferredDateEvent){
      yield* _setPreferredPickupDate(event.setPreferredPickupDateParams);
    }
  }

  Stream<SetPreferredPickupDateState> _setPreferredPickupDate(SetPreferredPickupDateParams? setPreferredPickupDateParams) async*{
    yield const LoadingSetPreferredPickupDateState();

    if(setPreferredPickupDateParams != null){
      final dataState = await setPreferredPickupDateUseCase(params: setPreferredPickupDateParams);

      if(dataState.data != null){
        showDialog(
            barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: 'Successfully updated preferred pickup schedule',
              // onPressedFunction: (){
              //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker()));
              // }
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.pushReplacement(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => const AuthChecker()));

        final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
        bookingBloc.add(const GetBookingsEvent());

        yield const SuccessSetPreferredPickupDateState();
      } else {
        showDialog(
          barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => ErrorMessage(title: LocaleKeys.error.tr(), content: 'Error updating preferred pickup schedule',
              // onPressedFunction: (){
              //   Navigator.of(context, rootNavigator: true).pop();
              // }
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

        yield const ErrorSetPreferredPickupDateState();
      }
    } else {
      yield const ErrorSetPreferredPickupDateState();
    }
  }
}
