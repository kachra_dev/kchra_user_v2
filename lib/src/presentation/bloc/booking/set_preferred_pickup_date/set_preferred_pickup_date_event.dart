part of 'set_preferred_pickup_date_bloc.dart';

abstract class SetPreferredPickupDateEvent extends Equatable {
  const SetPreferredPickupDateEvent();

  @override
  List<Object?> get props => [];
}

class SetPreferredDateEvent extends SetPreferredPickupDateEvent{
  final SetPreferredPickupDateParams? setPreferredPickupDateParams;
  const SetPreferredDateEvent(this.setPreferredPickupDateParams);
}
