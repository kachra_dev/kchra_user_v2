part of 'booking_bloc.dart';

abstract class BookingEvent extends Equatable {
  const BookingEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class InitialBookingEvent extends BookingEvent{
  const InitialBookingEvent();
}

class CreateBookingEvent extends BookingEvent{
  final BookingParams? bookingParams;
  final String? assetBytes;
  const CreateBookingEvent(this.bookingParams, this.assetBytes);
}

