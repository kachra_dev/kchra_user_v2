part of 'get_notifications_bloc.dart';

abstract class GetNotificationsEvent extends Equatable {
  const GetNotificationsEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class GetBookingNotificationsEvent extends GetNotificationsEvent{
  const GetBookingNotificationsEvent();
}
