part of 'get_notifications_bloc.dart';

abstract class GetNotificationsState extends Equatable {
  final List<NotificationsModel>? notificationsListModel;
  const GetNotificationsState({this.notificationsListModel});

  @override
  List<Object?> get props => [];
}

class GetNotificationsInitial extends GetNotificationsState {
  const  GetNotificationsInitial();
}

class LoadingGetNotificationsState extends GetNotificationsState{
  const LoadingGetNotificationsState();
}

class SuccessGetNotificationsState extends GetNotificationsState{
  final List<NotificationsModel>? notificationsList;
  const SuccessGetNotificationsState(this.notificationsList) : super(notificationsListModel: notificationsList);
}
class ErrorGetNotificationsState extends GetNotificationsState{
  const ErrorGetNotificationsState();
}