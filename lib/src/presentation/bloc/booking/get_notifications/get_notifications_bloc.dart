import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/data/models/notifications/notifications_model.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';

part 'get_notifications_event.dart';
part 'get_notifications_state.dart';

class GetNotificationsBloc extends Bloc<GetNotificationsEvent, GetNotificationsState> {
  final GetNotificationsUseCase getNotificationsUseCase;
  GetNotificationsBloc(this.getNotificationsUseCase) : super(const GetNotificationsInitial());

  @override
  Stream<GetNotificationsState> mapEventToState(GetNotificationsEvent event) async* {
    if (event is GetBookingNotificationsEvent) {
      yield* _getNotifications();
    }
  }

  Stream<GetNotificationsState> _getNotifications() async*{
    yield const LoadingGetNotificationsState();

    ProfileHive profile = await GetHive().getCurrentUserProfile();

    final dataState = await getNotificationsUseCase(params: profile.id);

    if(dataState.data != null){
      List<NotificationsModel> notificationsList = dataState.data!;
      yield SuccessGetNotificationsState(notificationsList);
    } else {
      yield const ErrorGetNotificationsState();
    }
  }

}
