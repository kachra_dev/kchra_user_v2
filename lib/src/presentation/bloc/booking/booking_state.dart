part of 'booking_bloc.dart';

abstract class BookingState extends Equatable {
  final List<BookingModel>? bookingModel;
  const BookingState({this.bookingModel});

  @override
  List<Object> get props => [];
}

class LoadingBookingState extends BookingState {
  const LoadingBookingState();
}

class InitialCreateBookingState extends BookingState{
  const InitialCreateBookingState();
}

class SuccessCreateBookingState extends BookingState{
  const SuccessCreateBookingState();
}

class ErrorCreateBookingState extends BookingState{
  const ErrorCreateBookingState();
}
