part of 'change_booking_purpose_bloc.dart';

abstract class ChangeBookingPurposeState extends Equatable {
  final BookingModel? bookingModelResponse;
  const ChangeBookingPurposeState({this.bookingModelResponse});

  @override
  List<Object> get props => [];
}

class ChangeBookingPurposeInitial extends ChangeBookingPurposeState {
  const ChangeBookingPurposeInitial();
}

class LoadingChangeBookingPurposeState extends ChangeBookingPurposeState{
  const LoadingChangeBookingPurposeState();
}

class SuccessChangeBookingPurposeState extends ChangeBookingPurposeState{
  final BookingModel? bookingModel;
  const SuccessChangeBookingPurposeState(this.bookingModel) : super(bookingModelResponse: bookingModel);
}

class ErrorChangeBookingPurposeState extends ChangeBookingPurposeState{
  const ErrorChangeBookingPurposeState();
}