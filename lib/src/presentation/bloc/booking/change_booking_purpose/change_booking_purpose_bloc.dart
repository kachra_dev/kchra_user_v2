import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/messages/success_message.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

part 'change_booking_purpose_event.dart';
part 'change_booking_purpose_state.dart';

class ChangeBookingPurposeBloc extends Bloc<ChangeBookingPurposeEvent, ChangeBookingPurposeState> {
  final ChangeBookingPurposeUseCase changeBookingPurposeUseCase;
  ChangeBookingPurposeBloc(this.changeBookingPurposeUseCase) : super(const ChangeBookingPurposeInitial());

  @override
  Stream<ChangeBookingPurposeState> mapEventToState(ChangeBookingPurposeEvent event) async* {
    if (event is ChangePurposeEvent) {
      yield* _changeBookingPurpose(event.changeBookingPurposeParams);
    }
  }

  Stream<ChangeBookingPurposeState> _changeBookingPurpose(ChangeBookingPurposeParams? params) async*{
    yield const LoadingChangeBookingPurposeState();

    if(params != null){
      final dataState = await changeBookingPurposeUseCase(params: params);

      if(dataState.data != null){
        final BookingModel bookingData = dataState.data!;

        showDialog(
            barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => SuccessMessage(title: LocaleKeys.success.tr(), content: "Successfully changed booking purpose to 'For ${params.bookingPurpose}'",
              // onPressedFunction: (){
              //   Navigator.pop(context);
              //   Navigator.pop(context);
              // }
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();
        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

        final bookingBloc = BlocProvider.of<GetBookingBloc>(MyApp.navigatorKey.currentContext!);
        bookingBloc.add(const GetBookingsEvent());

        final findBookingBloc = BlocProvider.of<FindBookingBloc>(MyApp.navigatorKey.currentContext!);
        findBookingBloc.add(FindBooking(params.bookingId));

        yield SuccessChangeBookingPurposeState(bookingData);
      } else {
        showDialog(
          barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => SuccessMessage(title: LocaleKeys.error.tr(), content: "Error changing booking purpose.",
              // onPressedFunction: () => Navigator.pop(context)
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

        });

        Navigator.of(MyApp.navigatorKey.currentContext!, rootNavigator: true).pop();

        yield const ErrorChangeBookingPurposeState();
      }
    } else {
      yield const ErrorChangeBookingPurposeState();
    }
  }
}
