part of 'change_booking_purpose_bloc.dart';

abstract class ChangeBookingPurposeEvent extends Equatable {
  const ChangeBookingPurposeEvent();

  @override
  List<Object?> get props => [];
}

class ChangePurposeEvent extends ChangeBookingPurposeEvent{
  final ChangeBookingPurposeParams? changeBookingPurposeParams;
  const ChangePurposeEvent(this.changeBookingPurposeParams);
}
