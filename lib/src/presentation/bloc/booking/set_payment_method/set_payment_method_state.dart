part of 'set_payment_method_bloc.dart';

abstract class SetPaymentMethodState extends Equatable {
  const SetPaymentMethodState();

  @override
  List<Object> get props => [];
}

class SetPaymentMethodInitial extends SetPaymentMethodState {
  const SetPaymentMethodInitial();
}

class LoadingSetPaymentMethodState extends SetPaymentMethodState{
  const LoadingSetPaymentMethodState();
}

class SuccessSetPaymentMethodState extends SetPaymentMethodState{
  const SuccessSetPaymentMethodState();
}

class ErrorSetPaymentMethodState extends SetPaymentMethodState{
  const ErrorSetPaymentMethodState();
}