import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/payment/cash_on_delivery/cash_on_delivery.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:easy_localization/easy_localization.dart';

part 'set_payment_method_event.dart';
part 'set_payment_method_state.dart';

class SetPaymentMethodBloc extends Bloc<SetPaymentMethodEvent, SetPaymentMethodState> {
  final SetPaymentMethodBookingUseCase setPaymentMethodBookingUseCase;
  SetPaymentMethodBloc(this.setPaymentMethodBookingUseCase) : super(const SetPaymentMethodInitial());

  @override
  Stream<SetPaymentMethodState> mapEventToState(SetPaymentMethodEvent event) async* {
    if(event is SetPaymentEvent){
      yield* _setPaymentMethod(event.setPaymentBookingParams, event.bookingModel);
    }
  }

  Stream<SetPaymentMethodState> _setPaymentMethod(SetPaymentBookingParams? params, BookingModel? bookingModel) async*{
    yield const LoadingSetPaymentMethodState();

    if(params != null){
      final dataState = await setPaymentMethodBookingUseCase(params: params);

      if(dataState.data != null){
        if(params.paymentMethod == 'Cash-On-Delivery' && bookingModel != null){
          Navigator.pushReplacement(MyApp.navigatorKey.currentContext!, MaterialPageRoute(builder: (context) => CashOnDelivery(booking: bookingModel)));
          // pushNewScreen(MyApp.navigatorKey.currentContext!, screen: CashOnDelivery(booking: bookingModel));
        }
        yield const SuccessSetPaymentMethodState();
      } else {
        yield const ErrorSetPaymentMethodState();

        showDialog(
          barrierDismissible: false,
            context: MyApp.navigatorKey.currentContext!,
            builder: (context) => ErrorMessage(title: LocaleKeys.error.tr(), content: 'Error setting payment method.',
              // onPressedFunction: () => Navigator.of(context,rootNavigator: true).pop()
            )
        );

        await Future.delayed(const Duration(seconds: futureDelayTimeConst),(){

        });

        Navigator.of(MyApp.navigatorKey.currentContext!,rootNavigator: true).pop();
      }
    } else {
      yield const ErrorSetPaymentMethodState();
    }
  }
}
