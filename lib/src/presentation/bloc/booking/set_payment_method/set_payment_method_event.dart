part of 'set_payment_method_bloc.dart';

abstract class SetPaymentMethodEvent extends Equatable {
  const SetPaymentMethodEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];

  @override
  // TODO: implement stringify
  bool? get stringify => true;
}

class SetPaymentEvent extends SetPaymentMethodEvent {
  final SetPaymentBookingParams? setPaymentBookingParams;
  final BookingModel? bookingModel;
  const SetPaymentEvent({this.setPaymentBookingParams, this.bookingModel});
}
