import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/app_settings/app_settings_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/dashboard.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/menu_page.dart';
import 'package:kachra_user_app/src/presentation/views/download_page.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/login/login.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';

class AuthChecker extends StatefulWidget {
  final int? bottomNavbarIndex;

  const AuthChecker({Key? key, this.bottomNavbarIndex}) : super(key: key);

  @override
  _AuthCheckerState createState() => _AuthCheckerState();
}

class _AuthCheckerState extends State<AuthChecker> {
  late String _localPath;

  Future<Map<String, dynamic>> initApp() async {
    //open boxes
    await Hive.openBox('userProfile');
    await Hive.openBox('userSavedAddresses');
    await Hive.openBox('locallySavedBookingAssets');
    await Hive.openBox('appAsset');

    //create local path
    await _prepareSaveDir();
    PackageInfo info = await PackageInfo.fromPlatform();

    ///FOR DEV PURPOSE ONLY
    // Hive.box('appAsset').clear();

    AppAssetHive? appAsset = await GetHive().getAppAssets();

    Map<String, dynamic> initResult = {
      "info": info,
      "appAsset": appAsset
    };

    return initResult;
  }

  Future<void> _prepareSaveDir() async {
    _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        Directory dir = Directory('/storage/emulated/0/Download');
        externalStorageDirPath = dir.path;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath = (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: initApp(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if(snapshot.hasData) {
              Map<String, dynamic> initData = snapshot.data as Map<String, dynamic>;

              PackageInfo info = initData['info'] as PackageInfo;
              AppAssetHive? appAsset = initData['appAsset'] as AppAssetHive?;

              return BlocListener<AppSettingsBloc, AppSettingsState>(
                listener: (context, state) {
                  if (state is SuccessAppSettingsState) {
                    String latestVersion = state.appSettingsModel!.mobileUserVersion!;
                    String? latestUrlLink = state.appSettingsModel!.mobileUserUrlLink;

                    String currentVersion = info.version;

                    if (latestVersion != currentVersion && latestUrlLink != null) {
                      showDialog(
                          context: MyApp.navigatorKey.currentContext!,
                          builder: (context) => AlertDialog(
                            title: const Text('Update available'),
                            content: Text('Update to $latestVersion is available.'),
                            actions: [
                              TextButton(
                                onPressed: () async {
                                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DownloadPage(latestUrlLink: latestUrlLink, description: 'Downloading the latest version. Please wait.', isAppAsset: false,)));
                                },
                                child: const Text('Update now')
                              ),
                              TextButton(
                                  onPressed: () => Navigator.pop(context),
                                  child: const Text('Cancel'))
                            ],
                          ));
                    }

                    if(appAsset == null){
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const DownloadPage(description: 'Getting the app ready for the first time. Please wait a moment.', isAppAsset: true,)));
                    }
                  }
                },
                child: BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
                  if (state is AuthenticatedState) {
                    // return Dashboard(bottomNavbarIndex: widget.bottomNavbarIndex);
                    return MenuPage(bottomNavbarIndex: widget.bottomNavbarIndex);
                  } else {
                    return const Login();
                  }
                }),
              );
            } else {
              return Scaffold(
                body: Center(
                  child: Image.asset('assets/img/logo_real.png', scale: 2.0),
                ),
              );
            }
          } else {
            return Scaffold(
              body: Center(
                child: Image.asset('assets/img/logo_real.png', scale: 2.0),
              ),
            );
          }
        });
  }
}
