import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/shared_preferences/store_preference.dart';
import 'package:kachra_user_app/src/core/utils/temp_value_holder.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/dashboard.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:video_player/video_player.dart';
import 'package:visibility_detector/visibility_detector.dart';

import 'booking_drawer.dart';

class MenuPage extends StatefulWidget {
  final int? bottomNavbarIndex;
  const MenuPage({Key? key, this.bottomNavbarIndex}) : super(key: key);

  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  Future<ProfileHive?> getProfile() async {
    await Future.delayed(const Duration(milliseconds: 400), (){});
    return GetHive().getCurrentUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          iconTheme: const IconThemeData(color: Colors.black)
        ),
        // drawer: const BookingDrawer(),
        body: FutureBuilder(
          future: getProfile(),
          builder: (context, snapshot) {
            if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
              ProfileHive profile = snapshot.data as ProfileHive;

              return Stack(
                children: [
                  SizedBox(
                    height: 40.0.h,
                    width: 100.0.w,
                    // decoration: BoxDecoration(
                    //   // color: Colors.limeAccent,
                    //   image: const DecorationImage(image: AssetImage('assets/img/bkground-front.png'), fit: BoxFit.cover),
                    //   border: Border.all(color: Colors.black, width: 1.0)
                    // ),
                    child: Image.asset('assets/img/logo_real.png', scale: 2.0),
                  ),
                  Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 30.0.h,
                          width: 100.0.w,
                          // child: Image.asset('assets/img/logo_real.png', scale: 11.0)
                        ),
                        const SizedBox(height: 20.0),
                        Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                          child: Align(
                              alignment: Alignment.center,
                              child: Directionality(
                                textDirection: ltrTextDirection,
                                child: Text(LocaleKeys.what_would_you_like_to_do.tr(), style: const TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.w700)),
                              )
                          ),
                        ),
                        const SizedBox(height: 30.0),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Directionality(
                            textDirection: ltrTextDirection,
                            child: Row(
                              children: [
                                GestureDetector(
                                  onTap: () async {
                                    await StorePreference().storeIsForRemoval(false);
                                    Future.delayed(const Duration(seconds: 0), (){
                                      pushNewScreen(context, screen: const Dashboard(bottomNavbarIndex: 1));
                                    });
                                  },
                                  child: Card(
                                    color: const Color(0xFFF7F962),
                                    child: SizedBox(
                                      height: 120.0,
                                      width: 95.0,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Image.asset('assets/img/sell.png', scale: 4.0),
                                          const SizedBox(height: 10.0),
                                          Text(LocaleKeys.sell_kchra.tr(), style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 10.0)),
                                          const SizedBox(height: 5.0),
                                        ],
                                      ),
                                    ),
                                    elevation: 4.0,
                                  ),
                                ),
                                const SizedBox(width: 50.0),
                                GestureDetector(
                                  onTap: () async {
                                    await StorePreference().storeIsForRemoval(true);
                                    Future.delayed(const Duration(seconds: 0), (){
                                      pushNewScreen(context, screen: const Dashboard(bottomNavbarIndex: 1));
                                    });

                                  },
                                  child: Card(
                                    color: const Color(0xFFF7F962),
                                    child: SizedBox(
                                      height: 120.0,
                                      width: 95.0,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Image.asset('assets/img/trash.png', scale: 4.0),
                                          const SizedBox(height: 10.0),
                                          Padding(
                                            padding: EdgeInsets.only(right: MyApp.navigatorKey.currentContext!.locale == const Locale('ar') ? 9.0 : 0.0),
                                            child: Text(LocaleKeys.remove_kchra.tr(), style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 10.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                    elevation: 4.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(height: 10.0),
                        // Align(
                        //     alignment: Alignment.centerLeft,
                        //     child: Directionality(
                        //       textDirection: ltrTextDirection,
                        //       child: Row(
                        //         children: [
                        //           Text(LocaleKeys.explore_the_app.tr(), style: const TextStyle(color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w700)),
                        //           const Spacer(),
                        //           const Padding(
                        //             padding: EdgeInsets.only(right: 15.0),
                        //             child: Icon(Icons.keyboard_arrow_right, color: Colors.black),
                        //           )
                        //         ],
                        //       ),
                        //     )
                        // ),
                        const SizedBox(height: 20.0),
                        // Container(
                        //   height: 200.0,
                        //   width: 90.0.w,
                        //   decoration: BoxDecoration(
                        //     color: Colors.black,
                        //     image: DecorationImage(image: const AssetImage('assets/img/logo_real.png'), fit: BoxFit.contain, colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop)),
                        //   ),
                        //   child: FutureBuilder(
                        //     future: GetHive().getAppAssets(),
                        //     builder: (context, snapshot) {
                        //       if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
                        //         AppAssetHive appAsset = snapshot.data as AppAssetHive;
                        //
                        //         return SizedBox(
                        //           width: 100.0.w,
                        //           child: VideoCreator(base64: MyApp.navigatorKey.currentContext!.locale == const Locale('ar') ? appAsset.frontPageAssetArabic : appAsset.frontPageAssetEnglish, newKey: UniqueKey())
                        //         );
                        //       } else {
                        //         return const Center(
                        //           child: CircularProgressIndicator(color: Colors.white),
                        //         );
                        //       }
                        //     }
                        //   )
                        // ),
                        Expanded(
                          child: Container(
                            width: 100.0.w,
                            color: const Color(0xFFF0F708),
                            child: Image.asset('assets/img/menu-page-front.png')
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              );
            } else {
              return Center(
                child: Image.asset('assets/img/logo_real.png', scale: 2.0),
              );
            }
          }
        ),
      ),
    );
  }
}

class VideoCreator extends StatefulWidget {
  final String? base64;
  final UniqueKey? newKey;

  const VideoCreator({Key? key, this.base64, this.newKey}) : super(key: newKey);

  @override
  _VideoCreatorState createState() => _VideoCreatorState();
}

class _VideoCreatorState extends State<VideoCreator> {
  VideoPlayerController? _controller;
  ChewieController? chewieController;

  bool isPlaying = false;

  StreamController<bool> streamController = StreamController();
  StreamController<bool> streamVideoPlayerController = StreamController();

  @override
  void initState() {
    super.initState();
    createVideoController();
  }

  Future<VideoPlayerController?> createVideoController() async {
    streamController.add(false);

    var rng = Random();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File file = File(tempPath+ (rng.nextInt(100)).toString() +'.mp4');

    file.writeAsBytes(base64.decode(widget.base64!));

    _controller = VideoPlayerController.file(file);

    chewieController = ChewieController(
        videoPlayerController: _controller!,
        allowFullScreen: false,
        showControls: true,
        looping: false,
        autoPlay: false,
        autoInitialize: true,
        allowPlaybackSpeedChanging: false,
        allowMuting: true,
        showControlsOnInitialize: false,
        aspectRatio: 3.2/2
    );

    _controller!.addListener(() {
      if(_controller!.value.isPlaying){
        streamController.add(true);
      } else {
        streamController.add(false);
      }
    });

    return _controller;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: createVideoController(),
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
            return VisibilityDetector(
              key: const Key("menuPageAsset"),
              onVisibilityChanged: (visibility){
                if(visibility.visibleFraction == 0 && chewieController != null){
                  chewieController!.pause();
                } else{
                  if(_controller != null && _controller!.value.duration != _controller!.value.position && chewieController != null){
                    chewieController!.play();
                  }
                }
              },
              child: AspectRatio(
                  aspectRatio: _controller!.value.aspectRatio,
                  child: Stack(
                    children: [
                      Chewie(
                          controller: chewieController!
                      ),
                      StreamBuilder(
                          stream: streamController.stream,
                          builder: (context, snapshot){
                            bool? isPlay = snapshot.data as bool?;

                            return isPlay == null || !isPlay ? Padding(
                              padding: const EdgeInsets.only(top: 80.0),
                              child: Align(
                                  alignment: Alignment.center,
                                  child: Text(LocaleKeys.watch_video.tr(), style: const TextStyle(color: Colors.white,fontSize: 15.0, fontWeight: FontWeight.w700))
                              ),
                            ) : const SizedBox(height: 0);
                          }
                      )
                    ],
                  )
              ),
            );
          } else {
            return const SizedBox(
                height: 50,
                child: CircularProgressIndicator(color: Colors.white)
            );
          }
        }
    );
  }

  @override
  void dispose() {
    _controller!.dispose();
    chewieController!.dispose();

    _controller = null;
    chewieController = null;
    super.dispose();
  }
}
