import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/completed/completed_booking.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'content/booking/booking.dart';
import 'content/booking/on-going/on-going-unapproved/on_going_booking.dart';
import 'package:easy_localization/easy_localization.dart';

class BodyDashboard extends StatefulWidget {
  final int? bottomNavbarIndex;
  const BodyDashboard({Key? key, this.bottomNavbarIndex}) : super(key: key);

  @override
  _BodyDashboardState createState() => _BodyDashboardState();
}

class _BodyDashboardState extends State<BodyDashboard> {
  List<Widget> _buildScreens() {
    return [
      const OnGoingBooking(),
      const Booking(),
      const CompletedBooking()
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.location),
        title: (LocaleKeys.on_going.tr()),
        activeColorPrimary: Colors.green,
        inactiveColorPrimary: CupertinoColors.white,
      ),
      PersistentBottomNavBarItem(
          icon: const Icon(CupertinoIcons.photo_camera, color: Colors.white),
          title: (LocaleKeys.booking.tr()),
          activeColorPrimary: Colors.green,
          inactiveColorPrimary: CupertinoColors.white),
      PersistentBottomNavBarItem(
          icon: const Icon(CupertinoIcons.check_mark_circled),
          title: (LocaleKeys.completed.tr()),
          activeColorPrimary: Colors.green,
          inactiveColorPrimary: CupertinoColors.white),
    ];
  }

  @override
  Widget build(BuildContext context) {
    PersistentTabController _controller;
    _controller = PersistentTabController(initialIndex: widget.bottomNavbarIndex ?? 1);

    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: const Color(0xFF146356),// Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset:
          true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true, // Default is true.
      hideNavigationBarWhenKeyboardShows:
          true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(0.0),
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: const ItemAnimationProperties(
        // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: const ScreenTransitionAnimation(
        // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style16, // Choose the nav bar style with this property.
    );
  }
}
