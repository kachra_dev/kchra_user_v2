import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/utils/helper/notification_api.dart';
import 'package:kachra_user_app/src/core/utils/helper/token_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/temp_value_holder.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_notifications/get_notifications_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/auth_checker.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/body_dashboard.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on_going_booking_preview.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pusher_client/pusher_client.dart';

class Dashboard extends StatefulWidget {
  final int? bottomNavbarIndex;
  const Dashboard({Key? key, this.bottomNavbarIndex}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  @override
  void initState() {
    super.initState();
    getNotifications();
    NotificationApi().initNotification();
    listenNotification();
  }

  listenNotification() => NotificationApi.onNotifications.stream.listen((String? payload){
    if(payload != null) {
      final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
      findBookingBloc.add(FindBooking(payload));

      pushNewScreen(context, screen: const OnGoingBookingPreview(), withNavBar: false);
    }
  });

  PusherClient? pusher;
  Channel? channel;

  Future<void> getNotifications() async {
    final authBloc = BlocProvider.of<AuthBloc>(context);

    if(authBloc.state is AuthenticatedState){
      ProfileHive profile = await GetHive().getCurrentUserProfile();
      String currentUserId = profile.id.toString();

      pusher = PusherClient("5f6d5a6407197f144d57", PusherOptions(cluster: 'ap2'));

      channel = pusher!.subscribe("kachra-channel");

      channel!.bind('kachra', (PusherEvent? event) {
        Map<String, dynamic> jsonNotification = jsonDecode(event!.data.toString());

        String? userId = jsonNotification['userID'];
        String? purpose = jsonNotification['purpose'];

        var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
        DateTime resDateTime = DateTime.now();

        String dateTime = outputFormat.format(resDateTime).toString();

        if (currentUserId == userId) {
          if (purpose == 'New Amount') {
            Map<String,
                dynamic> jsonNotificationMessage = jsonNotification['message'];
            String bookingId = jsonNotificationMessage['id'];

            FlutterLocalNotificationModel flutterLocalNotificationModel =
            FlutterLocalNotificationModel(
                title: 'A new amount has been sent.',
                body: dateTime,
                payload: bookingId
            );

            NotificationApi().showNotification(flutterLocalNotificationModel);

            final bookingBloc = BlocProvider.of<GetBookingBloc>(
                MyApp.navigatorKey.currentContext!);
            bookingBloc.add(const GetBookingsEvent());

            final findBookingBloc = BlocProvider.of<FindBookingBloc>(
                MyApp.navigatorKey.currentContext!);
            findBookingBloc.add(FindBooking(bookingId));

            Navigator.of(
                MyApp.navigatorKey.currentContext!, rootNavigator: true)
                .pushAndRemoveUntil(MaterialPageRoute(
                builder: (context) => const OnGoingBookingPreview()), (
                Route<dynamic> route) => route.isFirst);
          } else {
            final bookingBloc = BlocProvider.of<GetBookingBloc>(
                MyApp.navigatorKey.currentContext!);
            bookingBloc.add(const GetBookingsEvent());

            final findBookingBloc = BlocProvider.of<FindBookingBloc>(
                MyApp.navigatorKey.currentContext!);
            findBookingBloc.add(
                FindBooking(TempValueHolderHelper.tempValue.value.bookingId));

            final getNotificationsBloc = BlocProvider.of<GetNotificationsBloc>(
                MyApp.navigatorKey.currentContext!);
            getNotificationsBloc.add(const GetBookingNotificationsEvent());
          }
        }

        if(mounted){
          setState(() {});
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker()));
        return false;
      },
      child: Scaffold(
        extendBodyBehindAppBar: true,
        body: BodyDashboard(bottomNavbarIndex: widget.bottomNavbarIndex),
      ),
    );
  }
}
