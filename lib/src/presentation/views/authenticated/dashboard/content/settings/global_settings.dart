import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/core/utils/shared_preferences/get_preference.dart';
import 'package:kachra_user_app/src/core/utils/shared_preferences/store_preference.dart';
import 'package:sizer/sizer.dart';

class GlobalSettings extends StatefulWidget {
  const GlobalSettings({Key? key}) : super(key: key);

  @override
  _GlobalSettingsState createState() => _GlobalSettingsState();
}

class _GlobalSettingsState extends State<GlobalSettings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings', style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: FutureBuilder(
        future: GetPreference().getSaveToPhoneSetting(),
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.done){
            bool? saveToPhonePreference= snapshot.data as bool?;
            bool saveToPhoneSetting = saveToPhonePreference ?? false;
            return SizedBox(
              height: 100.0.h,
              child: ListView(
                children: [
                  ListTile(
                    title: const Text('Save Records'),
                    subtitle: const Text('Save recorded image/video on phone everytime.'),
                    trailing: Switch(
                        value: saveToPhoneSetting,
                        activeColor: Colors.green,
                        onChanged: (val) async {
                          await StorePreference().storeSaveToPhoneSetting(!saveToPhoneSetting);
                          setState(() {});
                        }
                    ),
                  )
                ],
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      )
    );
  }
}
