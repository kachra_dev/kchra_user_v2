import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/messages/ask_phone_number_message.dart';
import 'package:kachra_user_app/src/core/utils/shared_preferences/get_preference.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/address_and_details_preview.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:video_player/video_player.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class BookingPreview extends StatefulWidget {
  final bool isVideo;
  final List<XFile> file;
  final String assetTypeSelected;
  VideoPlayerController? controller;

  BookingPreview(
      {Key? key,
      required this.isVideo,
      required this.file,
      required this.assetTypeSelected,
      this.controller})
      : super(key: key);

  @override
  _BookingPreviewState createState() => _BookingPreviewState();
}

class _BookingPreviewState extends State<BookingPreview> {
  VideoPlayerController? _toBeDisposed;
  String? _retrieveDataError;

  Widget _previewVideo() {
    final Text? retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (widget.controller == null) {
      return const Text(
        'Pick a trash now!',
        textAlign: TextAlign.center,
      );
    }
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: AspectRatioVideo(widget.controller),
    );
  }

  Widget _previewImages() {
    final Text? retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    return Semantics(
        child: ListView.builder(
          key: UniqueKey(),
          itemBuilder: (context, index) {
            return Semantics(
              label: 'image_picker_example_picked_image',
              child: Image.file(File(widget.file[index].path)),
            );
          },
          itemCount: widget.file.length,
        ),
        label: 'image_picker_example_picked_images');
  }

  @override
  void deactivate() {
    if (widget.controller != null) {
      widget.controller!.setVolume(0.0);
      widget.controller!.pause();
    }
    super.deactivate();
  }

  @override
  void dispose() {
    _disposeVideoController();
    super.dispose();
  }

  Future<void> _disposeVideoController() async {
    if (_toBeDisposed != null) {
      await _toBeDisposed!.dispose();
    }
    _toBeDisposed = widget.controller;
    widget.controller = null;
  }

  Text? _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError!);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.delayed(const Duration(seconds: 1), (){}),
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.done){
          return Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              title: const Text(
                'Junk Preview',
                style: TextStyle(color: Colors.black),
              ),
              iconTheme: const IconThemeData(color: Colors.black),
            ),
            body: Center(
              child: Container(
                height: 70.0.h,
                width: 80.0.w,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(22.0),
                    border: Border.all(color: Colors.black, width: 2.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 60.0.h,
                      width: 80.0.w,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0)),
                      ),
                      child: widget.isVideo
                          ? ClipRRect(
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                          child: _previewVideo())
                          : ClipRRect(
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              topRight: Radius.circular(20.0)),
                          child: _previewImages()),
                    ),
                    const Spacer(),
                    widget.file.isEmpty
                        ? const SizedBox(height: 0)
                        : GestureDetector(
                      onTap: () async {
                        bool? isForRemoval = await GetPreference().getIsForRemoval();
                        Navigator.of(context, rootNavigator: false).push(MaterialPageRoute(builder: (context) => AddressAndDetails(isVideo: widget.isVideo, file: widget.file, assetTypeSelected: widget.assetTypeSelected, isForRemoval: isForRemoval ?? true)));
                        // pushNewScreen(context, screen: AddressAndDetails(isVideo: widget.isVideo, file: widget.file, assetTypeSelected: widget.assetTypeSelected, isForRemoval: isForRemoval ?? true), withNavBar: false);
                      },
                      child: Container(
                          height: 8.0.h,
                          width: 100.0.w,
                          decoration: const BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20.0),
                                bottomRight: Radius.circular(20.0)),
                          ),
                          child: Center(
                              child: Text(
                                LocaleKeys.select_this_junk_now.tr(),
                                style: const TextStyle(color: Colors.white),
                              ))),
                    )
                  ],
                ),
              ),
            ),
          );
        } else {
          return Scaffold(
            body: Center(
              child: Image.asset('assets/img/logo_real.png', scale: 2.0),
            ),
          );
        }
      }
    );
  }
}

typedef OnPickImageCallback = void Function(double? maxWidth, double? maxHeight, int? quality);

class AspectRatioVideo extends StatefulWidget {
  const AspectRatioVideo(this.controller, {Key? key}) : super(key: key);

  final VideoPlayerController? controller;

  @override
  AspectRatioVideoState createState() => AspectRatioVideoState();
}

class AspectRatioVideoState extends State<AspectRatioVideo> {
  ChewieController? chewieController;
  VideoPlayerController? get controller => widget.controller;
  bool initialized = false;

  void _onVideoControllerUpdate() {
    if (!mounted) {
      return;
    }
    if (initialized != controller!.value.isInitialized) {
      initialized = controller!.value.isInitialized;
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    controller!.addListener(_onVideoControllerUpdate);
  }

  @override
  void dispose() {
    controller!.removeListener(_onVideoControllerUpdate);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    chewieController = ChewieController(
        videoPlayerController: controller!,
        allowFullScreen: false,
        showControls: true,
        looping: false,
        autoPlay: false,
        deviceOrientationsOnEnterFullScreen: [
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);


    if (initialized) {
      return Center(
        child: AspectRatio(
          aspectRatio: controller!.value.aspectRatio,
          // child: VideoPlayer(controller!),
          child: Chewie(
            controller: chewieController!,
          )
        ),
      );
    } else {
      return Container();
    }
  }
}
