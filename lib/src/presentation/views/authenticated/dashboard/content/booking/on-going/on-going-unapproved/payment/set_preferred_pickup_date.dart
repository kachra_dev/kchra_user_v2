import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_preferred_pickup_date/set_preferred_pickup_date_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../../../../../../auth_checker.dart';

class SetPreferredPickupDate extends StatefulWidget {
  final BookingModel booking;
  const SetPreferredPickupDate({Key? key, required this.booking}) : super(key: key);

  @override
  _SetPreferredPickupDateState createState() => _SetPreferredPickupDateState();
}

class _SetPreferredPickupDateState extends State<SetPreferredPickupDate> {
  DateTime currentValue = DateTime.now();

  @override
  Widget build(BuildContext context) {
    String month = DateFormat('MMMM').format(DateTime(0, currentValue.month)).toString();
    String day = currentValue.day.toString();
    String weekDay = DateFormat('EEEE').format(currentValue);
    String time = DateFormat.jm().format(currentValue);

    return WillPopScope(
      onWillPop: () async {
        Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(LocaleKeys.set_preferred_pickup_date.tr(), style: const TextStyle(color: Colors.black)),
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: SingleChildScrollView(
                child: SizedBox(
                  height: 80.0.h,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          const Icon(Icons.access_time, color: Colors.green),
                          const SizedBox(width: 10.0),
                          Expanded(
                            child: Text(
                                LocaleKeys.choose_your_preferred_date_for_pickup.tr(),
                                style: const TextStyle(
                                    color: Colors.green,
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.w600)),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10.0),
                      Container(
                        height: 470,
                        width: 80.0.w,
                        decoration: BoxDecoration(
                            color: Colors.green[100],
                            borderRadius: BorderRadius.circular(15.0)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: 380,
                                width: 100.0.w,
                                decoration: BoxDecoration(
                                    color: Colors.green[900],
                                    borderRadius: BorderRadius.circular(15.0)
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    // crossAxisAlignment: CrossAxisAlignment.center,
                                    // mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const SizedBox(
                                        height: 100,
                                        child: Icon(Icons.access_time, color: Colors.white, size: 80.0)
                                      ),
                                      Text(
                                        month,
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 30.0,
                                          fontWeight: FontWeight.w600
                                        ),
                                      ),
                                      Text(
                                        day,
                                        style: const TextStyle(
                                            color: Colors.orange,
                                            fontSize: 50.0,
                                            fontWeight: FontWeight.w600
                                        ),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.only(left: 30.0, right: 30.0),
                                        child: Divider(color: Colors.white, thickness: 2.0),
                                      ),
                                      const SizedBox(height: 10.0),
                                      Text(
                                        weekDay,
                                        style: const TextStyle(
                                            color: Colors.white,
                                            fontSize: 30.0,
                                            fontWeight: FontWeight.w600
                                        ),
                                      ),
                                      Text(
                                        time,
                                        style: const TextStyle(
                                            color: Colors.orange,
                                            fontSize: 30.0,
                                            fontWeight: FontWeight.w600
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const Spacer(),
                              GestureDetector(
                                onTap: () async {
                                  final date = await showDatePicker(
                                      context: context,
                                      firstDate: DateTime(DateTime.now().year),
                                      initialDate: currentValue,
                                      lastDate: DateTime(DateTime.now().year + 1));
                                  if (date != null) {
                                    final time = await showTimePicker(
                                      context: context,
                                      initialTime: TimeOfDay.fromDateTime(currentValue),
                                    );

                                    DateTime dateSchedule = DateTimeField.combine(date, time);
                                    setState(() {
                                      currentValue = dateSchedule;
                                    });
                                  }
                                },
                                child: Container(
                                  height: 50,
                                  width: 100.0.w,
                                  decoration: BoxDecoration(
                                    color: Colors.blue,
                                      borderRadius: BorderRadius.circular(15.0)
                                  ),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(Icons.access_time, color: Colors.white),
                                      const SizedBox(width: 10.0),
                                      Center(child: Text(LocaleKeys.change_date.tr(), style: const TextStyle(color: Colors.white))),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: (){
                  showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (context) => AlertDialog(
                      title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
                      content: Text(LocaleKeys.are_you_sure_about_the_pickup_schedule.tr()),
                      actions: [
                        BlocBuilder<SetPreferredPickupDateBloc, SetPreferredPickupDateState>(
                          builder: (context, state) {
                            if(state is LoadingSetPreferredPickupDateState){
                              return const CircularProgressIndicator();
                            } else {
                              return TextButton(
                                  onPressed: (){
                                    SetPreferredPickupDateParams setPreferredPickupDateParams = SetPreferredPickupDateParams(
                                        bookingId: widget.booking.id!,
                                        preferredPickupDate: currentValue.toString()
                                    );

                                    final setPreferredPickupDateBloc = BlocProvider.of<SetPreferredPickupDateBloc>(context);
                                    setPreferredPickupDateBloc.add(SetPreferredDateEvent(setPreferredPickupDateParams));
                                  },
                                  child: Text('${LocaleKeys.yes.tr()} $yesEmoji')
                              );
                            }
                          }
                        ),
                        BlocBuilder<SetPreferredPickupDateBloc, SetPreferredPickupDateState>(
                          builder: (context, state) {
                            if(state is LoadingSetPreferredPickupDateState){
                              return const SizedBox(height: 0);
                            } else {
                              return TextButton(
                                  onPressed: () => Navigator.pop(context),
                                  child: Text('${LocaleKeys.cancel.tr()} $noEmoji')
                              );
                            }
                          }
                        )
                      ],
                    )
                  );
                },
                child: Container(
                  height: 100,
                  width: 100.0.w,
                  color: Colors.green,
                  child: Center(
                    child: Text(LocaleKeys.set_schedule.tr(), style: const TextStyle(color: Colors.white, fontSize: 15.0),),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}