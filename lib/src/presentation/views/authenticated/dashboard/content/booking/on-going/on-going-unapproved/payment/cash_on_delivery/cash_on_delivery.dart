import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/payment/set_preferred_pickup_date.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class CashOnDelivery extends StatefulWidget {
  final BookingModel booking;
  const CashOnDelivery({Key? key, required this.booking}) : super(key: key);

  @override
  _CashOnDeliveryState createState() => _CashOnDeliveryState();
}

class _CashOnDeliveryState extends State<CashOnDelivery> {
  DateTime currentValue = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cash On Delivery', style: TextStyle(color: Colors.black)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset('assets/img/cash-on-delivery.gif', scale: 4.0,)
              ),
              const SizedBox(height: 30.0),
              Text(
                LocaleKeys.please_prepare_your_cash.tr(),
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 15.0.sp
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 30.0),
              Text(
                LocaleKeys.admin_is_assigning_you_a_driver_to_pick_up_your_kchra.tr(),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 30.0),
              TextButton(
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SetPreferredPickupDate(booking: widget.booking)));
                  // pushNewScreen(context, screen: SetPreferredPickupDate(booking: widget.booking));
                },
                child: Text(LocaleKeys.set_preferred_pickup_schedule.tr())
              ),
              const SizedBox(height: 15.0),
              Container(
                height: 90,
                width: 80.0.w,
                decoration: BoxDecoration(
                    color: Colors.green[100],
                    borderRadius: BorderRadius.circular(15.0)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Container(
                        height: 70,
                        width: 60.0.w,
                        decoration: BoxDecoration(
                            color: Colors.green[900],
                            borderRadius: BorderRadius.circular(15.0)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 50.0,
                                child: Column(
                                  children: [
                                    Text(
                                      DateFormat('MMM').format(DateTime(0, currentValue.month)).toString(),
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500
                                      ),
                                    ),
                                    Text(
                                      currentValue.day.toString(),
                                      style: TextStyle(
                                          color: Colors.orange[200],
                                          fontWeight: FontWeight.w700,
                                          fontSize: 18.0
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              const VerticalDivider(color: Colors.white,thickness: 1.0),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      DateFormat('EEEE').format(currentValue),
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 15.0
                                      ),
                                    ),
                                    Text(
                                      DateFormat.jm().format(currentValue),
                                      style: TextStyle(
                                          color: Colors.orange[200],
                                          fontWeight: FontWeight.w500
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      IconButton(
                          onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SetPreferredPickupDate(booking: widget.booking))),
                          icon: const Icon(Icons.calendar_today_outlined, color: Colors.blue)
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
