import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/config.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/messages/registration_required_message.dart';
import 'package:kachra_user_app/src/core/utils/temp_value_holder.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/accept_decline_booking/accept_decline_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_payment_method/set_payment_method_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/payment/stripe_payment/widget/loading_button.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:logger/logger.dart';
import 'package:myfatoorah_flutter/model/initsession/SDKInitSessionResponse.dart';
import 'package:myfatoorah_flutter/myfatoorah_flutter.dart';
import 'package:myfatoorah_flutter/utils/MFCountry.dart';
import 'package:myfatoorah_flutter/utils/MFEnvironment.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class PaymentSheetScreen extends StatefulWidget {
  final BookingModel booking;
  final int amount;
  final ProfileHive profile;

  const PaymentSheetScreen(
      {Key? key, required this.amount, required this.booking, required this.profile})
      : super(key: key);

  @override
  _PaymentSheetScreenState createState() => _PaymentSheetScreenState();
}

class _PaymentSheetScreenState extends State<PaymentSheetScreen> {
  TextEditingController emailController = TextEditingController();
  final controller = CardEditController();
  int step = 0;

  late MFPaymentCardView mfPaymentCardView;

  @override
  void initState() {
    super.initState();

    // TODO, don't forget to init the MyFatoorah Plugin with the following line
    MFSDK.init(myFatoorahTestToken, MFCountry.QATAR, MFEnvironment.TEST);

    initiateSession();
  }

  void initiateSession() {
    var logger = Logger();

    MFSDK.initiateSession((MFResult<MFInitiateSessionResponse> result) =>
    {
      if (result.isSuccess())
        {
          // This for embedded payment view
          mfPaymentCardView.load(result.response!)
        }
      else
        {
          logger.i(result.response)
        }
    });
  }

  createPaymentCardView() {
    mfPaymentCardView = MFPaymentCardView();
    return mfPaymentCardView;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pay thru Stripe',
            style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CreditCardWidget(
              cardType: CardType.mastercard,
              cardNumber: '4242424242424242',
              expiryDate: '4/26',
              cardHolderName: '42142424242',
              cvvCode: '42142424242',
              showBackView: false,
              onCreditCardWidgetChange: (creditCardBrand) {}, //true when you want to show cvv(back) view
            ),
            Container(
              width: 90.0.w,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(color: Colors.black)),
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Column(
                  children: [
                    Text(LocaleKeys.you_need_to_pay.tr()),
                    const SizedBox(height: 10.0),
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: widget.amount.toString(),
                          style: const TextStyle(
                              fontWeight: FontWeight.w900,
                              color: Colors.black,
                              fontSize: 40.0)),
                      const TextSpan(
                          text: ' QAR',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                              fontSize: 20.0)),
                    ])),
                  ],
                ),
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.all(20.0),
            //   child: TextField(
            //     controller: emailController,
            //     decoration: InputDecoration(
            //         label: Text(LocaleKeys.please_input_your_email.tr())),
            //   ),
            // ),
            createPaymentCardView(),
            // Padding(
            //   padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            //   child: Container(
            //     decoration: BoxDecoration(
            //         border: Border.all(color: Colors.black),
            //         borderRadius: BorderRadius.circular(5.0)),
            //     child: CardField(
            //       controller: controller,
            //     ),
            //   ),
            // ),
            FutureBuilder(
              future: Future.delayed(const Duration(seconds: 5)),
              builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.done){
                  return Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
                    child: LoadingButton(
                      onPressed: () {
                        showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (context) => AlertDialog(
                              title: Text(
                                  '${LocaleKeys.confirm.tr()} $confirmEmoji'),
                              content:
                              BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                builder: (context, state) {
                                  if(state is LoadingBookingAcceptDeclineState){
                                    return Text(LocaleKeys.please_wait.tr());
                                  } else {
                                    return Text(LocaleKeys
                                        .do_you_want_to_pay_QAR_for_this_booking
                                        .tr(args: [
                                      widget.amount.toString()
                                    ]));
                                  }
                                },
                              ),
                              actions: [
                                BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                    builder: (context, state) {
                                      if (state is LoadingBookingAcceptDeclineState) {
                                        return const CircularProgressIndicator();
                                      } else {
                                        return TextButton(
                                            onPressed: () =>
                                                _handlePayPress(),
                                            child: Text(
                                                '${LocaleKeys.yes.tr()} $yesEmoji'));
                                      }
                                    }),
                                BlocBuilder<AcceptDeclineBookingBloc,
                                    AcceptDeclineBookingState>(
                                    builder: (context, state) {
                                      if (state is LoadingBookingAcceptDeclineState) {
                                        return const SizedBox(height: 0);
                                      } else {
                                        return TextButton(
                                            onPressed: () => Navigator.of(
                                                context,
                                                rootNavigator: true)
                                                .pop(),
                                            child: Text(
                                                '${LocaleKeys.no.tr()} $noEmoji'));
                                      }
                                    })
                              ],
                            ));
                      },
                      text: LocaleKeys.pay_now.tr(),
                    ),
                  );
                } else{
                  return const CircularProgressIndicator();
                }
              }
            ),
            // const SizedBox(height: 20.0),
            GestureDetector(
              onTap: () {
                if(widget.profile.isGuest == 0){
                  final BuildContext dialogContext = context;
                  final BuildContext thisContext = context;

                  showDialog(
                      context: dialogContext,
                      builder: (context) => AlertDialog(
                        title: Text(
                            '${LocaleKeys.confirm.tr()} $confirmEmoji'),
                        content: Text(LocaleKeys
                            .are_you_sure_you_want_to_choose_cash_on_delivery_as_a_payment_method
                            .tr()),
                        actions: [
                          BlocBuilder<SetPaymentMethodBloc,
                              SetPaymentMethodState>(
                              builder: (context, state) {
                                if (state is LoadingSetPaymentMethodState) {
                                  return const CircularProgressIndicator();
                                } else {
                                  return TextButton(
                                      onPressed: () {
                                        SetPaymentBookingParams
                                        setPaymentBookingParams =
                                        SetPaymentBookingParams(
                                            bookingId: widget.booking.id!,
                                            paymentMethod:
                                            'Cash-On-Delivery');

                                        final setPaymentMethodBloc =
                                        BlocProvider.of<
                                            SetPaymentMethodBloc>(
                                            thisContext);
                                        setPaymentMethodBloc.add(
                                            SetPaymentEvent(
                                                setPaymentBookingParams: setPaymentBookingParams,
                                                bookingModel: widget.booking
                                            ));

                                        final findBookingBloc =
                                        BlocProvider.of<FindBookingBloc>(
                                            context);
                                        findBookingBloc.add(FindBooking(
                                            TempValueHolderHelper
                                                .tempValue.value.bookingId));
                                      },
                                      child: Text(
                                          '${LocaleKeys.yes.tr()} $yesEmoji'));
                                }
                              }),
                          BlocBuilder<SetPaymentMethodBloc,
                              SetPaymentMethodState>(
                              builder: (context, state) {
                                if (state is LoadingSetPaymentMethodState) {
                                  return const SizedBox(height: 0);
                                } else {
                                  return TextButton(
                                    onPressed: () => Navigator.of(context,
                                        rootNavigator: true)
                                        .pop(),
                                    child: Text(
                                        '${LocaleKeys.cancel.tr()} $noEmoji'),
                                  );
                                }
                              }),
                        ],
                      ));
                } else {
                  showDialog(context: context, builder: (context) => const RegistrationRequiredMessage());
                }
              },
              child: RichText(
                  text: TextSpan(children: [
                TextSpan(
                    text: LocaleKeys.no_card_do_cash_on_delivery_instead.tr(),
                    style: const TextStyle(color: Colors.blue)),
              ])),
            )
          ],
        ),
      ),
    );
  }

  ///payment modal sheet functions
  // Future<Map<String, dynamic>> _createTestPaymentSheet() async {
  //   final url = Uri.parse('$kApiUrl/payment-sheet');
  //   final response = await http.post(
  //     url,
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: json.encode({
  //       'a': 'a',
  //       'amount': widget.amount,
  //       'currency': 'qar',
  //       'email': emailController.text
  //     }),
  //   );
  //   final body = json.decode(response.body);
  //   if (body['error'] != null) {
  //     throw Exception(body['error']);
  //   }
  //   return body;
  // }
  //
  // Future<void> initPaymentSheet() async {
  //   try {
  //     // 1. create payment intent on the server
  //     final data = await _createTestPaymentSheet();
  //
  //     // create some billingdetails
  //     const billingDetails = BillingDetails(
  //       email: 'email@stripe.com',
  //       phone: '+48888000888',
  //       address: Address(
  //         city: 'Houston',
  //         country: 'US',
  //         line1: '1459  Circle Drive',
  //         line2: '',
  //         state: 'Texas',
  //         postalCode: '77063',
  //       ),
  //     ); // mocked data for tests
  //
  //     // 2. initialize the payment sheet
  //     await Stripe.instance.initPaymentSheet(
  //       paymentSheetParameters: SetupPaymentSheetParameters(
  //         // Main params
  //         paymentIntentClientSecret: data['paymentIntent'],
  //         merchantDisplayName: 'Kchra Booking',
  //         // Customer params
  //         customerId: data['customer'],
  //         customerEphemeralKeySecret: data['ephemeralKey'],
  //         // Extra params
  //         applePay: true,
  //         googlePay: true,
  //         style: ThemeMode.dark,
  //         primaryButtonColor: Colors.redAccent,
  //         billingDetails: billingDetails,
  //         testEnv: true,
  //         merchantCountryCode: 'DE',
  //       ),
  //     );
  //     setState(() {
  //       step = 1;
  //     });
  //   } catch (e) {
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       SnackBar(content: Text('Error: $e')),
  //     );
  //     rethrow;
  //   }
  // }
  //
  // Future<void> confirmPayment() async {
  //   try {
  //     // 3. display the payment sheet.
  //     await Stripe.instance.presentPaymentSheet();
  //
  //     setState(() {
  //       step = 0;
  //     });
  //
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       const SnackBar(
  //         content: Text('Payment succesfully completed'),
  //       ),
  //     );
  //   } on Exception catch (e) {
  //     if (e is StripeException) {
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         SnackBar(
  //           content: Text('Error from Stripe: ${e.error.localizedMessage}'),
  //         ),
  //       );
  //     } else {
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         SnackBar(
  //           content: Text('Unforeseen error: ${e}'),
  //         ),
  //       );
  //     }
  //   }
  // }

  Future<void> acceptDeclineBooking(AcceptDeclineBookingParams params) async {
    final acceptDeclineBookingBloc = BlocProvider.of<AcceptDeclineBookingBloc>(context);
    acceptDeclineBookingBloc.add(AcceptDeclineEvent(params: params, bookingModel: widget.booking));

    ///fetch new bookings
    final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
    bookingBloc.add(const GetBookingsEvent());
  }

  Future<void> loadingAcceptDeclineBooking() async {
    final acceptDeclineBookingBloc = BlocProvider.of<AcceptDeclineBookingBloc>(context);
    acceptDeclineBookingBloc.add(const LoadingEvent());
  }

  Future<void> errorAcceptDeclineBooking() async {
    final acceptDeclineBookingBloc = BlocProvider.of<AcceptDeclineBookingBloc>(context);
    acceptDeclineBookingBloc.add(const ErrorEvent());
  }

  Future<void> _handlePayPress() async {
    var logger = Logger();

    var request = MFExecutePaymentRequest.constructor(double.parse(widget.amount.toString()));
    mfPaymentCardView.pay(request,
        MFAPILanguage.EN, (String invoiceId, MFResult<MFPaymentStatusResponse> result) => {
          if (result.isSuccess())
            {
              setState(() {
                logger.i(result.response);
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content:Text('Success!: The payment was confirmed successfully!')));
                AcceptDeclineBookingParams params = AcceptDeclineBookingParams(
                  context: context,
                  bookingId: widget.booking.id,
                  bookingQuoteStatus: 'Paid',
                  amount: widget.amount);
                acceptDeclineBooking(params);
              })
            }
          else
            {
              setState(() {
                logger.i(result.response);
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content:Text('Error!: Error making payment')));
              })
            }
        });
  }

  ///TODO: UNCOMMENT IF STRIPE SHOULD BE IMPLEMENTED
  ///no webhook payment
  // Future<void> _handlePayPress() async {
  //   try {
  //     loadingAcceptDeclineBooking();
  //
  //     // 1. Gather customer billing information (ex. email)
  //     final billingDetails = BillingDetails(
  //       email: emailController.text,
  //       phone: '+48888000888',
  //       address: const Address(
  //         city: 'Houston',
  //         country: 'US',
  //         line1: '1459  Circle Drive',
  //         line2: '',
  //         state: 'Texas',
  //         postalCode: '77063',
  //       ),
  //     ); // mocked data for tests
  //
  //     // 2. Create payment method
  //     final paymentMethod = await Stripe.instance.createPaymentMethod(PaymentMethodParams.card(billingDetails: billingDetails));
  //
  //     // 3. call API to create PaymentIntent
  //     final paymentIntentResult = await callNoWebhookPayEndpointMethodId(
  //         useStripeSdk: true,
  //         paymentMethodId: paymentMethod.id,
  //         currency: 'qar',
  //         // mocked data
  //         items: [
  //           {'id': 'id'}
  //         ],
  //         amount: widget.amount * 100);
  //
  //     if (paymentIntentResult['error'] != null) {
  //       // Error during creating or confirming Intent
  //       ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //           content: Text(
  //               '${LocaleKeys.error.tr()}: ${paymentIntentResult['error']}')));
  //       return;
  //     }
  //
  //     //Successful Payment
  //     if (paymentIntentResult['clientSecret'] != null &&
  //         paymentIntentResult['requiresAction'] == null) {
  //       ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
  //           content:
  //               Text('Success!: The payment was confirmed successfully!')));
  //       AcceptDeclineBookingParams params = AcceptDeclineBookingParams(
  //           context: context,
  //           bookingId: widget.booking.id,
  //           bookingQuoteStatus: 'Paid',
  //           amount: widget.amount);
  //       acceptDeclineBooking(params);
  //       return;
  //     }
  //
  //     if (paymentIntentResult['clientSecret'] != null &&
  //         paymentIntentResult['requiresAction'] == true) {
  //       // 4. if payment requires action calling handleCardAction
  //       final paymentIntent = await Stripe.instance
  //           .handleCardAction(paymentIntentResult['clientSecret']);
  //
  //       // todo handle error
  //       /*if (cardActionError) {
  //       Alert.alert(
  //       `Error code: ${cardActionError.code}`,
  //       cardActionError.message
  //       );
  //     } else*/
  //
  //       if (paymentIntent.status == PaymentIntentsStatus.RequiresConfirmation) {
  //         // 5. Call API to confirm intent
  //         await confirmIntent(paymentIntent.id);
  //       } else {
  //         // Payment succedeed
  //         ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error: ${paymentIntentResult['error']}')));
  //         errorAcceptDeclineBooking();
  //       }
  //     }
  //   } catch (e) {
  //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Error: $e')));
  //     errorAcceptDeclineBooking();
  //     rethrow;
  //   }
  // }
  //
  // Future<void> confirmIntent(String paymentIntentId) async {
  //   final result = await callNoWebhookPayEndpointIntentId(
  //       paymentIntentId: paymentIntentId);
  //   if (result['error'] != null) {
  //     ScaffoldMessenger.of(context)
  //         .showSnackBar(SnackBar(content: Text('Error: ${result['error']}')));
  //   } else {
  //     // ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
  //     //     content: Text('Success!: The payment was confirmed successfully!')));
  //   }
  // }
  //
  // Future<Map<String, dynamic>> callNoWebhookPayEndpointIntentId({
  //   required String paymentIntentId,
  // }) async {
  //   final url = Uri.parse('$kApiUrl/charge-card-off-session');
  //   final response = await http.post(
  //     url,
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: json.encode({'paymentIntentId': paymentIntentId}),
  //   );
  //   return json.decode(response.body);
  // }
  //
  // Future<Map<String, dynamic>> callNoWebhookPayEndpointMethodId({
  //   required bool useStripeSdk,
  //   required String paymentMethodId,
  //   required String currency,
  //   required int amount,
  //   List<Map<String, dynamic>>? items,
  // }) async {
  //   final url = Uri.parse('$kApiUrl/pay-without-webhooks');
  //   final response = await http.post(
  //     url,
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: json.encode({
  //       'useStripeSdk': useStripeSdk,
  //       'paymentMethodId': paymentMethodId,
  //       'currency': currency,
  //       'items': items,
  //       'amount': amount
  //     }),
  //   );
  //   return json.decode(response.body);
  // }


}
