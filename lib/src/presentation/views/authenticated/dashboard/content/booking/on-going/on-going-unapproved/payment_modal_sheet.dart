import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/temp_value_holder.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/accept_decline_booking/accept_decline_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_payment_method/set_payment_method_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/payment/cash_on_delivery/cash_on_delivery.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/payment/stripe_payment/screen/payment_sheet_screen.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:easy_localization/easy_localization.dart';

class PaymentModalSheet extends StatefulWidget {
  final BookingModel booking;
  final int amount;
  const PaymentModalSheet({Key? key, required this.amount, required this.booking}) : super(key: key);

  @override
  State<PaymentModalSheet> createState() => _PaymentModalSheetState();
}

class _PaymentModalSheetState extends State<PaymentModalSheet> {

  Future<void> acceptDeclineBooking(AcceptDeclineBookingParams params) async {
    final acceptDeclineBookingBloc = BlocProvider.of<AcceptDeclineBookingBloc>(context);
    acceptDeclineBookingBloc.add(AcceptDeclineEvent(params: params));
  }

  @override
  Widget build(BuildContext context) {
    bool isPaymentEnabled = widget.booking.quoteStatus == 'Accepted' ? true : false;

    int? additionalAmount = widget.booking.additionalAmount;
    bool isAdditionalAmount = widget.booking.additionalAmount != null ? true : false;

    return Material(
        child: SafeArea(
          top: false,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const ListTile(
                title: Center(child: Text('Payment methods:')),
              ),
              ListTile(
                enabled: isPaymentEnabled,
                title: const Text('Pay with Card'),
                leading: const Icon(Icons.credit_card),
                onTap: () async {
                  SetPaymentBookingParams setPaymentBookingParams = SetPaymentBookingParams(
                      bookingId: widget.booking.id!,
                      paymentMethod: 'Card'
                  );

                  final setPaymentMethodBloc = BlocProvider.of<SetPaymentMethodBloc>(context);
                  setPaymentMethodBloc.add(SetPaymentEvent(setPaymentBookingParams: setPaymentBookingParams, bookingModel: widget.booking));

                  ProfileHive profile = await GetHive().getCurrentUserProfile();
                  Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => PaymentSheetScreen(booking: widget.booking ,amount: isAdditionalAmount ? additionalAmount! : widget.amount, profile: profile)));
                },
              ),

              ///Other payment methods
              // ListTile(
              //   enabled: false,
              //   title: const Text('Pay with PayPal'),
              //   leading: const ImageIcon(AssetImage('assets/img/paypal.png')),
              //   onTap: () async {
              //
              //   },
              // ),
              ListTile(
                enabled: isPaymentEnabled,
                title: const Text('Cash On Delivery'),
                leading: const ImageIcon(AssetImage('assets/img/cod.png')),
                onTap: () async {
                  final BuildContext dialogContext = context;
                  final BuildContext thisContext = context;

                  showDialog(
                    context: dialogContext,
                    builder: (context) => AlertDialog(
                      title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
                      content: Text(LocaleKeys.are_you_sure_you_want_to_choose_cash_on_delivery_as_a_payment_method.tr()),
                      actions: [
                        BlocListener<SetPaymentMethodBloc, SetPaymentMethodState>(
                          listener: (context, state){
                            if(state is SuccessSetPaymentMethodState){
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CashOnDelivery(booking: widget.booking)));
                              // pushNewScreen(context, screen: CashOnDelivery(booking: widget.booking));
                            }
                          },
                          child: BlocBuilder<SetPaymentMethodBloc, SetPaymentMethodState>(
                            builder: (context, state) {
                              if(state is LoadingSetPaymentMethodState){
                                return const CircularProgressIndicator();
                              } else {
                                return TextButton(
                                    onPressed: (){
                                      SetPaymentBookingParams setPaymentBookingParams = SetPaymentBookingParams(
                                          bookingId: widget.booking.id!,
                                          paymentMethod: 'Cash-On-Delivery'
                                      );

                                      final setPaymentMethodBloc = BlocProvider.of<SetPaymentMethodBloc>(thisContext);
                                      setPaymentMethodBloc.add(SetPaymentEvent(setPaymentBookingParams:setPaymentBookingParams, bookingModel: widget.booking));

                                      final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                                      findBookingBloc.add(FindBooking(TempValueHolderHelper.tempValue.value.bookingId));

                                      // Navigator.pop(context);
                                      // Navigator.pop(context);
                                    },
                                    child: Text('${LocaleKeys.yes.tr()} $yesEmoji')
                                );
                              }
                            }
                          ),
                        ),
                        BlocBuilder<SetPaymentMethodBloc, SetPaymentMethodState>(
                          builder: (context, state) {
                            if(state is LoadingSetPaymentMethodState){
                              return const SizedBox(height: 0);
                            } else {
                              return TextButton(
                                  onPressed: () => Navigator.of(context,rootNavigator: true).pop(),
                                  child: Text('${LocaleKeys.cancel.tr()} $noEmoji'),
                              );
                            }
                          }
                        ),
                      ],
                    )
                  );
                },
              ),
            ],
          ),
        ));
  }
}
