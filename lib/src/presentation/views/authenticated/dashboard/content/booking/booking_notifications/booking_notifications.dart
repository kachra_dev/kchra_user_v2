import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/notifications/notifications_model.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_notifications/get_notifications_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on_going_booking_preview.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';

class BookingNotifications extends StatefulWidget {
  const BookingNotifications({Key? key}) : super(key: key);

  @override
  _BookingNotificationsState createState() => _BookingNotificationsState();
}

class _BookingNotificationsState extends State<BookingNotifications> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notifications', style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: BlocBuilder<GetNotificationsBloc, GetNotificationsState>(
        builder: (context, state) {
          if(state is LoadingGetNotificationsState){
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if(state is SuccessGetNotificationsState){
            List<NotificationsModel> notificationsList = state.notificationsList!;

            return notificationsList.isNotEmpty ? ListView.builder(
              itemCount: notificationsList.length,
              itemBuilder: (context, index){
                String updatedAt = notificationsList[index].updatedAt!;
                String message = notificationsList[index].message!;

                Map<String,dynamic> jsonMessage = jsonDecode(message);
                jsonMessage['estimated_amount'] = jsonMessage['estimated_amount'] != null ? int.parse(jsonMessage['estimated_amount'].toString()) : 0;

                BookingModel booking = BookingModel.fromJson(jsonMessage);

                var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
                DateTime resDateTime = DateTime.parse(updatedAt);

                String dateTime = outputFormat.format(resDateTime).toString();

                return jsonMessage['estimated_amount'] == 0 ? const SizedBox(height: 0) : ListTile(
                  onTap: (){
                    pushNewScreen(context, screen: ViewBookingMessage(booking: booking));
                  },
                  title: Text(dateTime),
                  subtitle: const Text('View details'),
                  trailing:  IconButton(
                      onPressed: (){
                        final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                        findBookingBloc.add(FindBooking(booking.id));

                        pushNewScreen(context, screen: const OnGoingBookingPreview(), withNavBar: false);
                      },
                      icon: const Icon(Icons.my_library_books, color: Colors.black)
                  ),
                );
              }
            ) : const Center(
              child: Text('No notifications yet.'),
            );
          } else {
            return const Center(
              child: Text('Failed to load notifications'),
            );
          }
        }
      ),
    );
  }
}

class ViewBookingMessage extends StatelessWidget {
  final BookingModel booking;
  const ViewBookingMessage({Key? key, required this.booking}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
    DateTime resDateTime = DateTime.parse(booking.updatedAt!);

    String dateTime = outputFormat.format(resDateTime).toString();

    return Scaffold(
      appBar: AppBar(
        title: const Text('Details', style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Center(child: Image.asset('assets/img/success.png', scale: 3.0)),
              const SizedBox(height: 20.0),
              Text('Successfully set Estimated Amount for \nBooking Id: ${booking.id}', style: const TextStyle(fontWeight: FontWeight.w700)),
              SizedBox(height: 3.0.h),
              const Center(child: Text('Booking Details:', style: TextStyle(fontWeight: FontWeight.w800),)),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: RichText(
                    text: TextSpan(
                        children: [
                          const TextSpan(
                              text: 'Booking Id: ',
                              style: TextStyle(color: Colors.black)
                          ),
                          TextSpan(
                              text: '${booking.id}',
                              style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w800, fontSize: 11.0)
                          )
                        ]
                    )
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: RichText(
                  text: TextSpan(
                    children: [
                      const TextSpan(
                        text: 'Estimated Amount: ',
                        style: TextStyle(color: Colors.black)
                      ),
                      TextSpan(
                          text: '${booking.estimatedAmount} QAR',
                          style: const TextStyle(color: Colors.blue, fontWeight: FontWeight.w800)
                      )
                    ]
                  )
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: RichText(
                    text: TextSpan(
                        children: [
                          const TextSpan(
                              text: 'Updated At: ',
                              style: TextStyle(color: Colors.black)
                          ),
                          TextSpan(
                              text: dateTime,
                              style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w600)
                          )
                        ]
                    )
                ),
              ),
              const SizedBox(height: 20.0),
              Center(
                child: OutlinedButton(
                  onPressed: (){
                    final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                    findBookingBloc.add(FindBooking(booking.id));

                    pushNewScreen(context, screen: const OnGoingBookingPreview(), withNavBar: false);
                  },
                  child: const Text('Go To Booking')
                ),
              )
            ],
          ),
        )
      ),
    );
  }
}


