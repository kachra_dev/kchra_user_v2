import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kachra_user_app/src/data/models/booking/list_booking_model.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class BodyCompletedBooking extends StatefulWidget {
  const BodyCompletedBooking({Key? key}) : super(key: key);

  @override
  _BodyCompletedBookingState createState() => _BodyCompletedBookingState();
}

class _BodyCompletedBookingState extends State<BodyCompletedBooking> {
  Future<void> getBookings() async {
    final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
    bookingBloc.add(const GetBookingsEvent());
  }

  List<Color> colors = [
    const Color(0xFFFFBD35),
    const Color(0xFF3FA796),
    const Color(0xFFA6CF98),
    const Color(0xFFF05454)
  ];

  List<String> image = [
    'assets/img/booking1.png',
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetBookingBloc, GetBookingState>(
        builder: (context, state) {
          if (state is SuccessGetBookingState) {
            List<ListBookingModel>? resBookingList = state.bookingList;
            List<ListBookingModel> bookingList = [];

            for (var booking in resBookingList!) {
              if (booking.bookingStatus == 'Completed') {
                bookingList.add(booking);
              }
            }

            return RefreshIndicator(
              onRefresh: () => getBookings(),
              child: WillPopScope(
                onWillPop: () async {
                  return false;
                },
                child: bookingList.isNotEmpty
                    ? ListView.builder(
                    itemCount: bookingList.length,
                    itemBuilder: (context, index) {
                      ListBookingModel booking = bookingList[index];

                      var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
                      DateTime resDateTime = DateTime.parse(booking.updatedAt!);

                      String dateTime =
                      outputFormat.format(resDateTime).toString();

                      String amount = booking.estimatedAmount != null
                          ? booking.estimatedAmount!.toString()
                          : 'TBA';

                      return GestureDetector(
                        onTap: () {
                          final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                          findBookingBloc.add(FindBooking(booking.id));

                          // if (booking.assetType == 'Image') {
                          //   pushNewScreen(context,
                          //       screen: CompletedBookingPreview(
                          //           isVideo: false, booking: booking),
                          //       withNavBar: false);
                          // } else {
                          //   pushNewScreen(context,
                          //       screen: CompletedBookingPreview(
                          //           isVideo: true, booking: booking),
                          //       withNavBar: false);
                          // }
                        },
                        child: Column(
                          children: [
                            Container(
                              height: 220,
                              width: 90.0.w,
                              decoration: BoxDecoration(
                                color: colors[2],
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Card(
                                child: Column(
                                  children: [
                                    Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  image[index % image.length]),
                                              fit: BoxFit.cover)),
                                    ),
                                    ListTile(
                                      title: Text(
                                        dateTime,
                                        style: TextStyle(
                                            color: Colors.yellow[900]),
                                      ),
                                      trailing: IconButton(
                                          onPressed: () async {},
                                          icon:
                                          const Icon(Icons.chevron_right)),
                                    ),
                                    const Divider(thickness: 1.0),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          30.0, 0, 30.0, 0),
                                      child: Row(
                                        children: [
                                          const Text(
                                            'Junk',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 15.0),
                                          ),
                                          const Spacer(),
                                          Text('$amount ر.ق '),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 10)
                          ],
                        ),
                      );
                    })
                    : RefreshIndicator(
                  onRefresh: () => getBookings(),
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/img/pending.png', scale: 2.0),
                        Text(LocaleKeys.no_completed_bookings_yet.tr()),
                      ],
                    ),
                  ),
                ),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
