import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/messages/success_message.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/pin_address_google_map.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/dashboard.dart';
import 'package:kachra_user_app/src/presentation/widgets/add_location_page.dart';
import 'package:kachra_user_app/src/presentation/widgets/current_location_map_widget.dart';
import 'package:kachra_user_app/src/presentation/widgets/show_address_details.dart';
import 'package:kachra_user_app/src/presentation/widgets/show_location_map_widget.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../../../auth_checker.dart';

class AddressAndDetails extends StatefulWidget {
  final bool isVideo;
  final List<XFile> file;
  final String assetTypeSelected;

  final bool isForRemoval;

  const AddressAndDetails(
      {Key? key,
      required this.isVideo,
      required this.file,
      required this.assetTypeSelected,
      required this.isForRemoval})
      : super(key: key);

  @override
  _AddressAndDetailsState createState() => _AddressAndDetailsState();
}

class _AddressAndDetailsState extends State<AddressAndDetails> {
  late Position location;
  late Placemark place;
  // late CameraPosition _initLocation;
  late String address;

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  StreamController<List<UserSavedAddressHive>> streamController = BehaviorSubject();

  bool isCurrentLocation = true;
  UserSavedAddressHive selectedAddress = UserSavedAddressHive();

  Future<void> sendAsset(BuildContext context, AddressModel address, String bookingDetails) async {
    //TODO: APPLICABLE FOR ONE ASSET ONLY. UPDATE CODE IF IT REQUIRES MORE THAN ONE ASSET (IMAGE/VIDEO)

    ///get base64 encode of asset selected
    final Uint8List bytes;
    String base64Asset;

    if (widget.assetTypeSelected == 'Image') {
      bytes = File(widget.file[0].path).readAsBytesSync();
      base64Asset = 'data:image/png;base64,' + base64Encode(bytes);
    } else {
      bytes = File(widget.file[0].path).readAsBytesSync();
      base64Asset = 'data:video/mpeg;base64,' + base64Encode(bytes);
    }

    ///get user id of current user
    ProfileHive profile = await GetHive().getCurrentUserProfile();
    String userId = profile.id!;

    ///generate booking params to pass booking bloc
    BookingParams bookingParams = BookingParams(
        userId: userId,
        asset: base64Asset,
        bookingStatus: 'Unapproved',
        address: address,
        assetType: widget.assetTypeSelected,
        bookingDetails: bookingDetails,
        bookingPurpose: widget.isForRemoval ? "Removal" : "Sale"
    );

    ///call booking bloc
    final bookingBloc = BlocProvider.of<BookingBloc>(context);
    bookingBloc.add(CreateBookingEvent(bookingParams, base64Encode(bytes)));

    showDialog(
      barrierDismissible: false,
        context: context,
        builder: (context) => SuccessMessage(title: LocaleKeys.sent.tr(), content: LocaleKeys.successfully_created_booking.tr(),
          // onPressedFunction: (){
          //   // Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
          //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const Dashboard(bottomNavbarIndex: 0)));
          // }
        )
    );

    await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

    });

    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const Dashboard(bottomNavbarIndex: 0)));

  }

  Future<void> sendBooking(AddressModel address, String bookingDetails) async {
    final bookingBloc = BlocProvider.of<BookingBloc>(context);
    bookingBloc.add(const InitialBookingEvent());

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => AlertDialog(
              title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
              content: Text(LocaleKeys.do_you_want_to_send_the_junk_now.tr()),
              actions: [
                TextButton(
                    onPressed: () =>
                        sendAsset(context, address, bookingDetails),
                    child: Text('${LocaleKeys.yes.tr()} $yesEmoji')
                ),
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('${LocaleKeys.no.tr()} $noEmoji'),
                )
              ],
            ));
  }

  Future<CameraPosition> getLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    location = await Geolocator.getCurrentPosition();
    List<Placemark> p =
        await placemarkFromCoordinates(location.latitude, location.longitude);
    place = p[0];

    address = place.name! + ", " + place.locality! + ", " + place.country!;

    CameraPosition initLocation = CameraPosition(
      target: LatLng(location.latitude, location.longitude),
      zoom: 16,
    );

    final marker = Marker(
      markerId: MarkerId(place.name.toString()),
      position: LatLng(location.latitude, location.longitude),
    );

    markers[MarkerId(place.name.toString())] = marker;

    return initLocation;
  }

  askLocation() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => AlertDialog(
              title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
              content: Text(
                  LocaleKeys.do_you_want_to_choose_the_current_location_for_booking.tr()),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop();
                    },
                    child: Text('${LocaleKeys.yes.tr()} $yesEmoji')),
                TextButton(
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop();
                      chooseLocation();
                    },
                    child: Text('${LocaleKeys.no_choose_another.tr()} $noEmoji')
                ),
              ],
            ));
  }

  chooseLocation() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => AlertDialog(
          insetPadding: EdgeInsets.zero,
          title: Text(LocaleKeys.choose_from_saved_locations.tr()),
          content: listLocation(),
          actions: [
            TextButton(
                onPressed: () async {
                  Navigator.of(context, rootNavigator: true).pop();
                  final List<UserSavedAddressHive> newSavedAddresses = await pushNewScreen(context, screen: const AddLocationPage(), withNavBar: false);
                  streamController.add(newSavedAddresses);
                },
                child: Text(LocaleKeys.add_location.tr())
            ),
            TextButton(
                onPressed: (){
                  Navigator.of(context, rootNavigator: true).pop();
                  streamController.close();
                },
                child: Text(LocaleKeys.close.tr())
            )
          ],
        ));
  }

  showDetails(UserSavedAddressHive savedAddress) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              insetPadding: EdgeInsets.zero,
              contentPadding: EdgeInsets.zero,
              content: ShowAddressDetails(savedAddress: savedAddress),
              actions: [
                TextButton(
                    onPressed: () =>
                        Navigator.of(context, rootNavigator: true).pop(),
                    child: Text(LocaleKeys.close.tr()))
              ],
            ));
  }

  selectLocation(UserSavedAddressHive savedAddress) {
    setState(() {
      isCurrentLocation = false;
      selectedAddress = savedAddress;
    });

    Navigator.of(context, rootNavigator: true).pop();
  }

  Widget listLocation() {
    // return FutureBuilder(
    //     future: GetHive().getSavedAddressesOfCurrentUser(),
    //     builder: (context, snapshot) {
    //       if (snapshot.connectionState == ConnectionState.done) {
    //         List<UserSavedAddressHive> savedAddresses =
    //             snapshot.data as List<UserSavedAddressHive>;
    //
    //         return savedAddresses.isNotEmpty
    //             ? SizedBox(
    //                 height: 60.0.h,
    //                 width: 100.0.w,
    //                 child: SingleChildScrollView(
    //                   child: ListView.builder(
    //                       shrinkWrap: true,
    //                       itemCount: savedAddresses.length,
    //                       itemBuilder: (context, index) {
    //                         UserSavedAddressHive savedAddress =
    //                             savedAddresses[index];
    //
    //                         return Padding(
    //                           padding: const EdgeInsets.all(10.0),
    //                           child: Card(
    //                             child: ListTile(
    //                               leading: const Icon(CupertinoIcons.location,
    //                                   color: Colors.red),
    //                               title: GestureDetector(
    //                                 onTap: () => selectLocation(savedAddress),
    //                                 child: Text(
    //                                     savedAddress.addressName ?? 'N/A'),
    //                               ),
    //                               trailing: IconButton(
    //                                   onPressed: () =>
    //                                       showDetails(savedAddress),
    //                                   icon: const Icon(Icons.info_outline)),
    //                             ),
    //                           ),
    //                         );
    //                       }),
    //                 ),
    //               )
    //             : SizedBox(
    //                 height: 20.0.h,
    //                 child: Center(
    //                   child: Text(LocaleKeys.no_saved_addresses.tr()),
    //                 ),
    //               );
    //       } else {
    //         return const Center(
    //           child: CircularProgressIndicator(),
    //         );
    //       }
    //     });
    return FutureBuilder(
      future: getSavedAddresses(),
      builder: (context, snapshotFuture) {
        if(snapshotFuture.connectionState == ConnectionState.done){
          return StreamBuilder(
              stream: streamController.stream,
              builder: (context, snapshot){
                List<UserSavedAddressHive>? savedAddresses = snapshot.data as List<UserSavedAddressHive>?;

                if(savedAddresses != null){
                  return savedAddresses.isNotEmpty
                      ? SizedBox(
                    height: 60.0.h,
                    width: 100.0.w,
                    child: SingleChildScrollView(
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: savedAddresses.length,
                          itemBuilder: (context, index) {
                            UserSavedAddressHive savedAddress =
                            savedAddresses[index];

                            return Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Card(
                                child: ListTile(
                                  leading: const Icon(CupertinoIcons.location,
                                      color: Colors.red),
                                  title: GestureDetector(
                                    onTap: () => selectLocation(savedAddress),
                                    child: Text(
                                        savedAddress.addressName ?? 'N/A'),
                                  ),
                                  trailing: IconButton(
                                      onPressed: () =>
                                          showDetails(savedAddress),
                                      icon: const Icon(Icons.info_outline)),
                                ),
                              ),
                            );
                          }),
                    ),
                  )
                      : SizedBox(
                    height: 20.0.h,
                    child: Center(
                      child: Text(LocaleKeys.no_saved_addresses.tr()),
                    ),
                  );
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }
    );
  }

  Future<CameraPosition> initialDataLocation() async {
    Future.delayed(Duration.zero, (){});
    return await getLocation();
  }

  final bookingDetails =  TextEditingController();
  var focusNode = FocusNode();
  final _formKey = GlobalKey<FormState>();

  Future<void> getSavedAddresses() async {
    List<UserSavedAddressHive> savedAddressesHive = await GetHive().getSavedAddressesOfCurrentUser();
    streamController.add(savedAddressesHive);
  }

  @override
  void initState() {
    super.initState();
    getSavedAddresses();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(LocaleKeys.check_booking_details.tr(),
            style: const TextStyle(color: Colors.black),
          ),
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        body: FutureBuilder(
            future: initialDataLocation(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
                return Stack(
                  children: [
                    Image.asset('assets/img/booking_detail.png'),
                    Center(
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 70.0.h,
                              width: 95.0.w,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20.0),
                                  border: Border.all(
                                      color: Colors.black, width: 2.0)),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 20.0),
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          const Icon(Icons.location_on_outlined,
                                              color: Colors.green),
                                          Text(LocaleKeys.booking_details.tr(),
                                              style: const TextStyle(
                                                  color: Colors.green,
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.w600)),
                                          const Spacer(),
                                          TextButton(
                                              onPressed: () => chooseLocation(),
                                              child: const Text('Saved Location')
                                          )
                                        ],
                                      ),
                                      const SizedBox(height: 10.0),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          isCurrentLocation
                                              ? const CurrentLocationMapWidget(
                                                  isHideAddress: false)
                                              : ShowLocationMapWidget(
                                                  latitude: double.parse(
                                                      selectedAddress
                                                          .latitude!),
                                                  longitude: double.parse(
                                                      selectedAddress
                                                          .longitude!),
                                                  name: selectedAddress.name!),
                                          const SizedBox(height: 20),
                                          isCurrentLocation ? const SizedBox(height: 0) : Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                  LocaleKeys.booking_address.tr(),
                                                  style: const TextStyle(fontWeight: FontWeight.w600)),
                                              const SizedBox(height: 5.0),
                                              Text(selectedAddress.name! + ", " + selectedAddress.country!),
                                            ],
                                          ),
                                          // GestureDetector(
                                          //   onTap: () => chooseLocation(),
                                          //   child: RichText(
                                          //     text: TextSpan(
                                          //       children: [
                                          //         TextSpan(
                                          //           text: 'Not the right location? You can ',
                                          //           style: GoogleFonts.poppins(color: Colors.black, fontSize: 12.0)
                                          //         ),
                                          //         TextSpan(
                                          //           text: '${LocaleKeys.add_location.tr()}.',
                                          //           style: GoogleFonts.poppins(color: Colors.blue, fontSize: 12.0)
                                          //         )
                                          //       ]
                                          //     )
                                          //   ),
                                          // ),
                                          const SizedBox(height: 20),
                                          RawKeyboardListener(
                                            focusNode: focusNode,
                                            onKey: (event) {
                                              if (event.isKeyPressed(LogicalKeyboardKey.enter)) {
                                                focusNode.unfocus();
                                              }
                                            },
                                            child: Form(
                                              key: _formKey,
                                              child: TextFormField(
                                                controller: bookingDetails,
                                                decoration: InputDecoration(
                                                  border: const OutlineInputBorder(),
                                                  hintText: LocaleKeys.enter_booking_details.tr(),
                                                ),
                                                // maxLines: 2,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(height: 5.0),
                                          Center(
                                            child: ElevatedButton(
                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green)),
                                              onPressed: () async {
                                                final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => PinAddressGoogleMap(isFromBooking: true, latitude: location.latitude, longitude: location.longitude, name: place.name!)));

                                                setState(() {
                                                  isCurrentLocation = false;
                                                  selectedAddress = result;
                                                });
                                              },
                                              child: const Text('Pin Location')
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 30.0),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: SizedBox(
                                height: 80,
                                width: 95.0.w,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  Colors.green)),
                                      onPressed: () async {
                                        AddressModel sendAddress =
                                        isCurrentLocation
                                            ? AddressModel(
                                            name: place.name,
                                            street: place.street,
                                            isoCountryCode: place
                                                .isoCountryCode,
                                            country: place.country,
                                            administrativeArea:
                                            place.administrativeArea,
                                            locality: place.locality,
                                            latitude: location.latitude
                                                .toString(),
                                            longitude: location.longitude
                                                .toString())
                                            : AddressModel(
                                            name: selectedAddress.name,
                                            street:
                                            selectedAddress.street,
                                            isoCountryCode:
                                            selectedAddress.iSOCountryCode,
                                            country:
                                            selectedAddress.country,
                                            administrativeArea:
                                            selectedAddress.administrativeArea,
                                            locality:
                                            selectedAddress.locality,
                                            latitude: selectedAddress
                                                .latitude
                                                .toString(),
                                            longitude: selectedAddress
                                                .longitude
                                                .toString());

                                        await sendBooking(sendAddress, bookingDetails.text);
                                      },
                                      child: Directionality(
                                        textDirection: ltrTextDirection,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(LocaleKeys.place_booking.tr()),
                                            const SizedBox(width: 10.0),
                                            const Icon(Icons.send),
                                          ],
                                        ),
                                      )),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              } else {
                return const Center(child: CircularProgressIndicator());
              }
            }));
  }
}
