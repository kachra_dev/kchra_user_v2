import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/utils/helper/token_checker.dart';
import 'package:kachra_user_app/src/core/utils/messages/registration_required_message.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

import 'body_on_going_booking.dart';

class OnGoingBooking extends StatefulWidget {
  const OnGoingBooking({Key? key}) : super(key: key);

  @override
  _OnGoingBookingState createState() => _OnGoingBookingState();
}

class _OnGoingBookingState extends State<OnGoingBooking> with AutomaticKeepAliveClientMixin{

  @override
  void initState() {
    super.initState();
    getBookings();
  }

  Future<void> getBookings() async {
    final bookingBloc = BlocProvider.of<GetBookingBloc>(context);

    if(bookingBloc.state is SuccessGetBookingState){
    } else {
      bookingBloc.add(const GetBookingsEvent());
    }
  }

  Future<void> syncBookings() async {
    final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
    bookingBloc.add(const GetBookingsEvent());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.your_bookings.tr(), style: const TextStyle(color: Colors.black)),
      ),
      body: const Padding(
        padding: EdgeInsets.only(top: 30.0),
        child: BodyOnGoingBooking(),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: 'on-going',
        onPressed: () => syncBookings(),
        icon: const Icon(CupertinoIcons.arrow_2_circlepath),
        label: Text(LocaleKeys.sync.tr()),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
