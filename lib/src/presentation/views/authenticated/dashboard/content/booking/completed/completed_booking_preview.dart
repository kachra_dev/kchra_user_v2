import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:video_player/video_player.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class CompletedBookingPreview extends StatefulWidget {
  final BookingModel booking;
  final bool isVideo;
  const CompletedBookingPreview({Key? key, required this.booking, required this.isVideo}) : super(key: key);

  @override
  _CompletedBookingPreviewState createState() => _CompletedBookingPreviewState();
}

class _CompletedBookingPreviewState extends State<CompletedBookingPreview> {
  Map<String, MaterialColor> chipColor = {
    LocaleKeys.pending.tr(): Colors.grey,
    LocaleKeys.accepted.tr(): Colors.green,
    LocaleKeys.declined.tr(): Colors.red
  };

  showEnlargedPicture(){
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: CachedNetworkImage(imageUrl: widget.booking.asset!),
        )
    );
  }

  showEnlargedVideo(){
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          insetPadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          content: SizedBox(
              width: 100.0.w,
              child: VideoCreator(url: widget.booking.asset)
          ),
        )
    );
  }

  showBookingDetails(){
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(LocaleKeys.booking_details.tr()),
          content: Text(
              widget.booking.bookingDetails!
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    String estimatedAmount = widget.booking.estimatedAmount != null ? widget.booking.estimatedAmount.toString() : 'TBA';
    String address = widget.booking.address!.locality! + ", " + widget.booking.address!.country!;
    String driverAssigned = widget.booking.driverAssigned != null ? widget.booking.driverAssigned!.user!.name! : 'TBA';

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(LocaleKeys.booking_information.tr(), style: const TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Center(
        child: Stack(
          children: [
            widget.isVideo ?
            GestureDetector(
              onTap: () => showEnlargedVideo(),
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 350,
                  width: 100.0.w,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    border: Border.all(color: Colors.black, width: 2.0),
                    image: DecorationImage(image: const AssetImage('assets/img/front.png'), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)),
                  ),
                  child: const Center(
                    child: Icon(Icons.video_collection_outlined, color: Colors.white, size: 50.0),
                  ),
                ),
              ),
            ) : GestureDetector(
              onTap: () => showEnlargedPicture(),
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 350,
                  width: 100.0.w,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    border: Border.all(color: Colors.black, width: 2.0),
                    image: DecorationImage(image: CachedNetworkImageProvider(widget.booking.asset!), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)),
                  ),
                  child: const Center(
                    child: Icon(Icons.image, color: Colors.white, size: 50.0),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 400,
                width: 100.0.w,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                  border: Border.all(color: Colors.black, width: 2.0),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0),
                      child: SizedBox(
                        height: 260,
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        LocaleKeys.booking_id.tr(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 15.0.sp
                                        ),
                                      ),
                                      const SizedBox(height: 5.0),
                                      Text(
                                        TextChecker().checkText(widget.booking.id!),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 10.0.sp
                                        ),
                                      )
                                    ],
                                  ),
                                  const Spacer(),
                                  Text(
                                    '$estimatedAmount ر.ق ',
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.w900,
                                        fontSize: 16.0.sp
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(height: 20.0),
                              Row(
                                children: [
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Chip(
                                        label: widget.booking.quoteStatus != null ? Text(widget.booking.quoteStatus!, style: const TextStyle(color: Colors.white)) : Text(LocaleKeys.pending.tr(), style: const TextStyle(color: Colors.white)),
                                        backgroundColor: widget.booking.quoteStatus != null ? chipColor[widget.booking.quoteStatus!] : Colors.grey
                                    ),
                                  ),
                                  const Spacer(),
                                  widget.booking.bookingDetails != null ? TextButton(
                                      onPressed: (){
                                        showBookingDetails();
                                      },
                                      child: Text(LocaleKeys.view_details.tr())
                                  ) : const SizedBox(height: 0)
                                ],
                              ),
                              const SizedBox(height: 20.0),
                              Row(
                                children: [
                                  const Icon(Icons.location_on_outlined),
                                  const SizedBox(width: 5.0),
                                  Text(
                                      address
                                  )
                                ],
                              ),
                              const SizedBox(height: 20.0),
                              Card(
                                color: Colors.green[300],
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: SizedBox(
                                    width: 80.0.w,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(0,0,35.0,0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            LocaleKeys.driver_assigned.tr(),
                                            style: TextStyle(
                                                fontSize: 8.0.sp,
                                                fontWeight: FontWeight.w400,
                                                color: Colors.white
                                            ),
                                          ),
                                          const SizedBox(width: 5),
                                          Text(
                                            driverAssigned,
                                            style: TextStyle(
                                                fontSize: 12.0.sp,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.green[50]
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                    Container(
                      height: 90,
                      width: 100.0.w,
                      decoration: BoxDecoration(
                        color: Colors.green[50],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(LocaleKeys.estimate.tr()),
                                Text(
                                  '$estimatedAmount ر.ق ',
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w900
                                  ),
                                )
                              ],
                            ),
                            const Spacer(),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: ElevatedButton(
                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green[900])),
                                  onPressed:  () {},
                                  child: const Padding(
                                    padding: EdgeInsets.all(12.0),
                                    child: Text(
                                      'Completed',
                                      style: TextStyle(
                                          color: Colors.white
                                      ),
                                    ),
                                  )
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class VideoCreator extends StatefulWidget {
  final String? url;
  const VideoCreator({Key? key, this.url}) : super(key: key);

  @override
  _VideoCreatorState createState() => _VideoCreatorState();
}

class _VideoCreatorState extends State<VideoCreator> {
  VideoPlayerController? _controller;
  ChewieController? chewieController;

  @override
  Widget build(BuildContext context) {
    _controller = VideoPlayerController.network(widget.url!);

    chewieController = ChewieController(
        videoPlayerController: _controller!,
        looping: true,
        showControls: true,
        allowFullScreen: true,
        deviceOrientationsOnEnterFullScreen: [
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);

    return AspectRatio(
        aspectRatio: _controller!.value.aspectRatio,
        child: Chewie(
          controller: chewieController!,
        )
    );
  }

  @override
  void dispose() {
    _controller!.dispose();
    chewieController!.dispose();
    super.dispose();
  }
}