import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/track_location_google_map.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class PaidBookingModalSheet extends StatefulWidget {
  final BookingModel booking;
  const PaidBookingModalSheet({Key? key, required this.booking}) : super(key: key);

  @override
  _PaidBookingModalSheetState createState() => _PaidBookingModalSheetState();
}

class _PaidBookingModalSheetState extends State<PaidBookingModalSheet> {
  @override
  Widget build(BuildContext context) {
    return Material(
        child: SafeArea(
          top: false,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                title: Center(child: Text(LocaleKeys.actions.tr())),
              ),
              ListTile(
                title: Text(LocaleKeys.track_location_of_driver.tr()),
                leading: const Icon(Icons.location_on_outlined),
                onTap: () async {
                  if(widget.booking.driverAssigned != null){
                    pushNewScreen(context, screen: TrackLocationGoogleMap(driverId: widget.booking.driverAssigned!.id!));
                  } else {
                    showDialog(
                      barrierDismissible: false,
                        context: context,
                        builder: (context) => ErrorMessage(title: LocaleKeys.error.tr(), content: LocaleKeys.no_driver_assigned_yet.tr(),
                          // onPressedFunction: () => Navigator.pop(context)
                        )
                    );

                    await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

                    });

                    Navigator.of(context, rootNavigator: true).pop();
                  }
                },
              ),
            ],
          ),
        ));
  }
}
