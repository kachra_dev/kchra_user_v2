import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:logger/logger.dart';
import 'package:sizer/sizer.dart';

class TrackLocationGoogleMap extends StatefulWidget {
  final String driverId;
  const TrackLocationGoogleMap({Key? key, required this.driverId}) : super(key: key);

  @override
  _TrackLocationGoogleMapState createState() => _TrackLocationGoogleMapState();
}

class _TrackLocationGoogleMapState extends State<TrackLocationGoogleMap> {
  late BitmapDescriptor customMarker;
  late GoogleMapController _controller;
  bool _added = false;

  bool _isPolylineCreated = false;

  List<Placemark>? p;
  Position? location;

  late PolylinePoints polylinePoints;
  List<LatLng> polylineCoordinates = [];
  Map<PolylineId, Polyline> polylines = {};

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  double? destinationLat;
  double? destinationLong;

  Future<CameraPosition> getLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    MarkerId markerId = const MarkerId('RANDOM_ID');

    location = await Geolocator.getCurrentPosition();

    CameraPosition currentLocation = CameraPosition(
      target: LatLng(location!.latitude, location!.longitude),
      zoom: 14,
    );

    if(Platform.isIOS){
      customMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(),
        "assets/img/marker-3x.png",
      );
    } else {
      customMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(),
        "assets/img/marker.png",
      );
    }

    p = await placemarkFromCoordinates(location!.latitude, location!.longitude);

    final marker = Marker(
      markerId: MarkerId(p![0].name!),
      position: LatLng(location!.latitude, location!.longitude),
    );

    markers[MarkerId(p![0].name!)] = marker;

    return currentLocation;
  }


  _createPolyLines(double startLatitude, double startLongitude, double destinationLatitude, double destinationLongitude) async {
    polylinePoints = PolylinePoints();

    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      kGoogleApiKey,
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.driving,
    );

    if (result.points.isNotEmpty) {
      for (var point in result.points) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      }
    }

    // Defining an ID
    PolylineId id = const PolylineId('poly');

    // Initializing Polyline
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.red,
      points: polylineCoordinates,
      width: 5,
    );

    // Adding the polyline to the map
    polylines[id] = polyline;
  }

  @override
  void initState() {
    super.initState();
    getLocation();

    var logger = Logger();
    logger.i(widget.driverId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Track Driver', style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('location').snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (_added) {
            myMap(snapshot);
          }
          if (!snapshot.hasData) {
            return const Center(child: CircularProgressIndicator());
          }

          return Stack(
            children: [
              GoogleMap(
                mapType: MapType.normal,
                markers: Set<Marker>.of(markers.values),
                polylines: Set<Polyline>.of(polylines.values),
                initialCameraPosition: CameraPosition(
                    target: LatLng(
                      snapshot.data!.docs.singleWhere((element) => element.id == widget.driverId)['latitude'],
                      snapshot.data!.docs.singleWhere((element) => element.id == widget.driverId)['longitude'],
                    ),
                    zoom: 17),
                onMapCreated: (GoogleMapController controller) async {
                  Position currentLocate = await Geolocator.getCurrentPosition();

                  setState((){
                    location = currentLocate;

                    _controller = controller;
                    _added = true;
                  });
                },
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Container(
                    height: 70,
                    width: 100.0.w,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.black)
                    ),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: ListTile(
                          title: p != null ? Text('${p![0].name}, ${p![0].locality}', style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0)) : const Text('Loading Location...'),
                          subtitle: const Text('Your Location'),
                          leading: const Icon(CupertinoIcons.location),
                          dense: true,
                        )
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ));
  }

  Future<void> myMap(AsyncSnapshot<QuerySnapshot> snapshot) async {
    destinationLat = snapshot.data!.docs.singleWhere((element) => element.id == widget.driverId)['latitude'];
    destinationLong = snapshot.data!.docs.singleWhere((element) => element.id == widget.driverId)['longitude'];

    if(!_isPolylineCreated){
      _createPolyLines(location!.latitude, location!.longitude, destinationLat!, destinationLong!);
      _isPolylineCreated = true;
    }

    await _controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(destinationLat!, destinationLong!),
        zoom: 14)));

    if(Platform.isIOS){
      customMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(),
        "assets/img/marker-3x.png",
      );
    } else {
      customMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(),
        "assets/img/marker.png",
      );
    }

    markers[const MarkerId('id')] = Marker(
        position: LatLng(
          snapshot.data!.docs.singleWhere((element) => element.id == widget.driverId)['latitude'],
          snapshot.data!.docs.singleWhere((element) => element.id == widget.driverId)['longitude'],
        ),
        markerId: const MarkerId('id'),
        icon: customMarker
    );
  }
}
