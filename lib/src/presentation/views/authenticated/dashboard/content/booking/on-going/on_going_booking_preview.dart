import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/add_hive.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/hive/locally_saved_booking_assets_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/accept_decline_booking/accept_decline_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/change_booking_purpose/change_booking_purpose_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_payment_method/set_payment_method_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/paid_booking_modal_sheet.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on-going-unapproved/payment/set_preferred_pickup_date.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:path_provider/path_provider.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';
import 'package:video_player/video_player.dart';

import 'on-going-unapproved/payment/stripe_payment/screen/payment_sheet_screen.dart';
import 'on-going-unapproved/payment_modal_sheet.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:http/http.dart' as http;

class OnGoingBookingPreview extends StatefulWidget {
  const OnGoingBookingPreview({Key? key}) : super(key: key);

  @override
  _OnGoingBookingPreviewState createState() => _OnGoingBookingPreviewState();
}

class _OnGoingBookingPreviewState extends State<OnGoingBookingPreview> {
  showEnlargedPicture(String asset, bool isBase64){
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        insetPadding: EdgeInsets.zero,
        contentPadding: EdgeInsets.zero,
        content: isBase64 ? Image.memory(base64Decode(asset)) : CachedNetworkImage(imageUrl: asset),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
            child: Text(LocaleKeys.close.tr())
          )
        ],
      )
    );
  }

  showEnlargedVideo(String asset, bool isBase64){
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          insetPadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          content: SizedBox(
            width: 100.0.w,
            child: isBase64 ? VideoCreator(base64: asset) : VideoCreator(url: asset)
          ),
        )
    );
  }

  showBookingDetails(String bookingDetails){
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(LocaleKeys.booking_details.tr()),
          content: Text(
            bookingDetails
          ),
        )
    );
  }

  Future<void> paymentOptions(BookingModel booking, bool isAdditionalAmount) async {
    SetPaymentBookingParams setPaymentBookingParams = SetPaymentBookingParams(
        bookingId: booking.id!,
        paymentMethod: 'Card'
    );

    final setPaymentMethodBloc = BlocProvider.of<SetPaymentMethodBloc>(context);
    setPaymentMethodBloc.add(SetPaymentEvent(setPaymentBookingParams: setPaymentBookingParams, bookingModel: booking));

    final acceptDeclineBookingBloc = BlocProvider.of<AcceptDeclineBookingBloc>(context);
    acceptDeclineBookingBloc.add(const InitialEvent());

    ProfileHive profile = await GetHive().getCurrentUserProfile();
    pushNewScreen(context, screen: PaymentSheetScreen(booking: booking ,amount: isAdditionalAmount ? booking.additionalAmount! : booking.estimatedAmount!, profile: profile), withNavBar: false);
  }

  Future<void> addAssetToHive(BookingModel booking) async {
    ///Get the video URL, parse it and get the Response object
    http.Response response = await http.get(Uri.parse(booking.asset!));
    Uint8List bytes = response.bodyBytes;

    ///Convert to base64
    String base64Asset = base64Encode(bytes);

    LocallySavedBookingAssetsHive locallySavedBookingAssetsHive = LocallySavedBookingAssetsHive(
        bookingId: booking.id,
        asset: base64Asset,
        assetType: booking.assetType
    );

    ///Add base64 to hive
    AddHive().addLocallySavedBookingAsset(locallySavedBookingAssetsHive);
  }

  setPreferredPickupSchedule(BuildContext context, BookingModel booking) => pushNewScreen(context, screen: SetPreferredPickupDate(booking: booking), withNavBar: false);

  @override
  Widget build(BuildContext context) {
    Map<String, Color> chipColor = {
      'Pending': Colors.grey,
      'Accepted': Colors.green,
      'Declined': Colors.red,
      'Paid': const Color(0xFFFFBD35),
    };

    Map<String, String> chipQuoteStatus = {
      'Paid': LocaleKeys.paid.tr(),
      'Accepted' : LocaleKeys.accepted.tr(),
      'Pending' : LocaleKeys.pending.tr(),
      'Declined' : LocaleKeys.declined.tr()
    };

    Map<String, String> chipPaymentMethod = {
      'Card': LocaleKeys.card.tr(),
      'Cash-On-Delivery' : LocaleKeys.cash_on_delivery.tr(),
    };

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(LocaleKeys.booking_information.tr(), style: const TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: BlocBuilder<FindBookingBloc, FindBookingState>(
        builder: (context, state) {
          if(state is LoadingFindBookingState){
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if(state is SuccessFindBookingState) {
            BookingModel booking = state.bookingModel!;

            bool isVideo = booking.assetType == "Image" ? false : true;

            String estimatedAmount = booking.estimatedAmount != null ? booking.estimatedAmount.toString() : 'TBA';
            String additionalAmount = booking.additionalAmount != null ? booking.additionalAmount.toString() : 'TBA';

            String bookingPurpose = 'For ${booking.bookingPurpose!}';
            bool isForRemoval = booking.bookingPurpose == 'Removal' ? true: false;

            bool isAdditionalAmount = booking.additionalAmount != null ? true : false;

            String address = booking.address!.country!;
            String driverAssigned = booking.driverAssigned != null ? booking.driverAssigned!.user!.name! : 'TBA';

            var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
            DateTime? resDateTime = booking.preferredPickupDate;

            String dateTime = resDateTime != null ? outputFormat.format(resDateTime).toString() : 'TBA';

            bool isBookingEstimateAmount = booking.estimatedAmount != null ? true : false;
            bool isBookingAccepted = booking.quoteStatus == 'Accepted' ? true : false;
            bool isBookingPaid = booking.quoteStatus == 'Paid' ? true : false;

            return FutureBuilder(
              future: GetHive().getLocallySavedBookingAsset(booking.id!),
              builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.done) {
                  LocallySavedBookingAssetsHive? locallySavedBookingAssetsHive = snapshot.data as LocallySavedBookingAssetsHive?;

                  if(locallySavedBookingAssetsHive == null){
                    addAssetToHive(booking);
                  }

                  return Center(
                    child: Stack(
                      children: [
                        if(locallySavedBookingAssetsHive != null) ... [
                          locallySavedBookingAssetsHive.assetType == 'Video' ? GestureDetector(
                            onTap: () => showEnlargedVideo(locallySavedBookingAssetsHive.asset!, true),
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                height: 410,
                                width: 100.0.w,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  border: Border.all(color: Colors.black, width: 2.0),
                                  image: DecorationImage(image: const AssetImage('assets/img/front.png'), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)),
                                ),
                                child: const Center(
                                  child: Icon(Icons.video_collection_outlined, color: Colors.white, size: 50.0),
                                ),
                              ),
                            ),
                          ) : GestureDetector(
                            onTap: () => showEnlargedPicture(locallySavedBookingAssetsHive.asset!, true),
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                height: 410,
                                width: 100.0.w,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  border: Border.all(color: Colors.black, width: 2.0),
                                  image: DecorationImage(image: CachedNetworkImageProvider(booking.asset!), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)),
                                ),
                                child: const Center(
                                  child: Icon(Icons.image, color: Colors.white, size: 50.0),
                                ),
                              ),
                            ),
                          ),
                        ] else ... [
                          isVideo ? GestureDetector(
                            onTap: () => showEnlargedVideo(booking.asset!, false),
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                height: 410,
                                width: 100.0.w,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  border: Border.all(color: Colors.black, width: 2.0),
                                  image: DecorationImage(image: const AssetImage('assets/img/front.png'), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)),
                                ),
                                child: const Center(
                                  child: Icon(Icons.video_collection_outlined, color: Colors.white, size: 50.0),
                                ),
                              ),
                            ),
                          ) : GestureDetector(
                            onTap: () => showEnlargedPicture(booking.asset!, false),
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                height: 410,
                                width: 100.0.w,
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  border: Border.all(color: Colors.black, width: 2.0),
                                  image: DecorationImage(image: CachedNetworkImageProvider(booking.asset!), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)),
                                ),
                                child: const Center(
                                  child: Icon(Icons.image, color: Colors.white, size: 50.0),
                                ),
                              ),
                            ),
                          ),
                        ],
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            height: 440,
                            width: 100.0.w,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                              border: Border.all(color: Colors.black, width: 2.0),
                            ),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0),
                                  child: SizedBox(
                                    height: 310,
                                    child: SingleChildScrollView(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    LocaleKeys.booking_id.tr(),
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.w600,
                                                        fontSize: 15.0.sp
                                                    ),
                                                  ),
                                                  const SizedBox(height: 5.0),
                                                  Text(
                                                    TextChecker().checkText(booking.id!),
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 10.0.sp
                                                    ),
                                                  )
                                                ],
                                              ),
                                              const Spacer(),
                                              Column(
                                                children: [
                                                  Text(
                                                    '$estimatedAmount ر.ق ',
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontWeight: FontWeight.w900,
                                                        fontSize: 16.0.sp,
                                                        decoration: isAdditionalAmount ? TextDecoration.lineThrough : null,
                                                        decorationThickness: 3.0,
                                                        decorationColor: Colors.black
                                                    ),
                                                  ),
                                                  const SizedBox(height: 5.0),
                                                  Text(
                                                    '$additionalAmount ر.ق ',
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontWeight: FontWeight.w900,
                                                        fontSize: 12.0.sp
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                          const SizedBox(height: 20.0),
                                          Row(
                                            children: [
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: Chip(
                                                    label: booking.quoteStatus != null ? booking.quoteStatus == 'Pending' ? Text(LocaleKeys.pending.tr(), style: const TextStyle(color: Colors.white)) :  Text(chipQuoteStatus[booking.quoteStatus]!, style: const TextStyle(color: Colors.white)) : Text(LocaleKeys.pending_payment.tr(), style: const TextStyle(color: Colors.white, fontSize: 8.0)),
                                                    backgroundColor: booking.quoteStatus != null ? booking.quoteStatus == 'Pending' ? Colors.grey : chipColor[booking.quoteStatus!] : Colors.grey
                                                ),
                                              ),
                                              const Spacer(),
                                              booking.bookingDetails != null ? TextButton(
                                                  onPressed: (){
                                                    showBookingDetails(booking.bookingDetails!);
                                                  },
                                                  child: Text(LocaleKeys.view_details.tr())
                                              ) : const SizedBox(height: 0)
                                            ],
                                          ),
                                          SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Chip(
                                                      label: booking.paymentMethod != null ? Text(chipPaymentMethod[booking.paymentMethod]!, style: const TextStyle(color: Colors.white, fontSize: 10.0)) : Text(LocaleKeys.pending_payment_method.tr(), style: const TextStyle(color: Colors.white, fontSize: 8.0)),
                                                      backgroundColor: booking.paymentMethod != null ? Colors.green : Colors.grey
                                                  ),
                                                ),
                                                const SizedBox(width: 10.0),
                                                Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Chip(
                                                    label: Text(bookingPurpose, style: const TextStyle(color: Colors.white, fontSize: 10.0)),
                                                    backgroundColor: isForRemoval ? Colors.green : Colors.red,
                                                  ),
                                                ),
                                                const SizedBox(width: 10.0),
                                                booking.quoteStatus == null || booking.quoteStatus == 'Pending' ? TextButton(
                                                  onPressed: (){
                                                    showDialog(
                                                      context: context,
                                                      builder: (context) => AlertDialog(
                                                        title: Text(LocaleKeys.confirm.tr()),
                                                        content: Text('Do you want to change the booking purpose to ${isForRemoval ? 'Sale' : 'Removal'}?'),
                                                        actions: [
                                                          BlocBuilder<ChangeBookingPurposeBloc, ChangeBookingPurposeState>(
                                                            builder: (context, state) {
                                                              if(state is LoadingChangeBookingPurposeState){
                                                                return const CircularProgressIndicator();
                                                              } else {
                                                                return TextButton(
                                                                    onPressed: (){
                                                                      ChangeBookingPurposeParams changeBookingPurposeParams = ChangeBookingPurposeParams(
                                                                          bookingPurpose: isForRemoval ? 'Sale' : 'Removal',
                                                                          bookingId: booking.id!
                                                                      );

                                                                      final changeBookingPurposeBloc = BlocProvider.of<ChangeBookingPurposeBloc>(context);
                                                                      changeBookingPurposeBloc.add(ChangePurposeEvent(changeBookingPurposeParams));
                                                                    },
                                                                    child: Text('${LocaleKeys.yes.tr()} $yesEmoji')
                                                                );
                                                              }
                                                            }
                                                          ),
                                                          BlocBuilder<ChangeBookingPurposeBloc, ChangeBookingPurposeState>(
                                                            builder: (context, state) {
                                                              if(state is LoadingChangeBookingPurposeState){
                                                                return const SizedBox(height: 0);
                                                              } else {
                                                                return TextButton(
                                                                    onPressed: () => Navigator.pop(context),
                                                                    child: Text('${LocaleKeys.no.tr()} $noEmoji')
                                                                );
                                                              }
                                                            }
                                                          )
                                                        ],
                                                      )
                                                    );
                                                  },
                                                  child: const Text('Change', style: TextStyle(fontSize: 10.0))
                                                ) : const SizedBox(height: 0)
                                              ],
                                            ),
                                          ),
                                          const SizedBox(height: 20.0),
                                          Row(
                                            children: [
                                              const Icon(Icons.location_on_outlined),
                                              const SizedBox(width: 5.0),
                                              const Text('Location: '),
                                              const SizedBox(width: 5.0),
                                              Text(address, style: const TextStyle(fontWeight: FontWeight.w600))
                                            ],
                                          ),
                                          const SizedBox(height: 20.0),
                                          Row(
                                            children: [
                                              const Icon(CupertinoIcons.car_detailed),
                                              const SizedBox(width: 5.0),
                                              Text(LocaleKeys.driver_assigned.tr()),
                                              const SizedBox(width: 5.0),
                                              Text(
                                                driverAssigned,
                                                style: const TextStyle(fontWeight: FontWeight.w600),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 20.0),
                                          Row(
                                            children: [
                                              const Icon(CupertinoIcons.time),
                                              const SizedBox(width: 5.0),
                                              Text(LocaleKeys.schedule.tr()),
                                              const SizedBox(width: 5.0),
                                              GestureDetector(
                                                onTap: (){
                                                  if(isBookingPaid && resDateTime != null){
                                                    setPreferredPickupSchedule(context, booking);
                                                  }
                                                },
                                                child: Text(
                                                  dateTime,
                                                  style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.blue, decoration: TextDecoration.underline, decorationStyle: TextDecorationStyle.dotted, decorationThickness: 3.0),
                                                ),
                                              ),
                                              const Spacer(),
                                              isBookingPaid ? resDateTime != null ? const SizedBox(height: 0) : TextButton(
                                                onPressed: () => setPreferredPickupSchedule(context, booking),
                                                child: const Text('Set Schedule')
                                              ) : const SizedBox(height: 0)
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                const Spacer(),
                                Container(
                                  height: 90,
                                  width: 100.0.w,
                                  decoration: BoxDecoration(
                                    color: Colors.green[50],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 0),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(LocaleKeys.estimate.tr()),
                                            Text(
                                              '$estimatedAmount ر.ق ',
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.w900
                                              ),
                                            )
                                          ],
                                        ),
                                        const Spacer(),
                                        isBookingAccepted || isBookingPaid?
                                        ClipRRect(
                                          borderRadius: BorderRadius.circular(10.0),
                                          child: ElevatedButton(
                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green[900])),
                                              onPressed: () {
                                                isBookingPaid ? showCupertinoModalBottomSheet(
                                                    context: context,
                                                    backgroundColor: Colors.transparent,
                                                    builder: (context) =>
                                                    isBookingPaid ?
                                                    PaidBookingModalSheet(booking: booking) :
                                                    PaymentModalSheet(booking: booking ,amount: booking.estimatedAmount!)
                                                ) : paymentOptions(booking, isAdditionalAmount);

                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.all(12.0),
                                                child: isBookingPaid ?
                                                Text(
                                                  LocaleKeys.actions.tr(),
                                                  style: const TextStyle(
                                                      color: Colors.white
                                                  ),
                                                ) :
                                                Text(
                                                  LocaleKeys.payment_options.tr(),
                                                  style: const TextStyle(
                                                      color: Colors.white
                                                  ),
                                                ),
                                              )
                                          ),
                                        ) :
                                        isBookingEstimateAmount ?
                                        Row(
                                          children: [
                                            ClipRRect(
                                              borderRadius: BorderRadius.circular(10.0),
                                              child: ElevatedButton(
                                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green[900])),
                                                  onPressed: () {
                                                    showDialog(
                                                        context: context,
                                                        builder: (context) => AlertDialog(
                                                          title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
                                                          content: BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                                              builder: (context, state){
                                                                if(state is LoadingBookingAcceptDeclineState){
                                                                  return Text(LocaleKeys.please_wait.tr());
                                                                } else {
                                                                  return Text(LocaleKeys.do_you_want_to_accept_the_proposed_booking.tr());
                                                                }
                                                              }
                                                          ),
                                                          actions: [
                                                            BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                                                builder: (context, state){
                                                                  if(state is LoadingBookingAcceptDeclineState){
                                                                    return const CircularProgressIndicator();
                                                                  } else {
                                                                    return TextButton(
                                                                        onPressed: (){
                                                                          AcceptDeclineBookingParams params = AcceptDeclineBookingParams(
                                                                            context: context,
                                                                            bookingId: booking.id,
                                                                            bookingQuoteStatus: 'Accepted',
                                                                          );

                                                                          final acceptDeclineBookingBloc = BlocProvider.of<AcceptDeclineBookingBloc>(context);
                                                                          acceptDeclineBookingBloc.add(AcceptDeclineEvent(params: params));

                                                                          final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                                                                          findBookingBloc.add(FindBooking(booking.id));
                                                                        },
                                                                        child: Text(LocaleKeys.accept.tr(), style: const TextStyle(color: Colors.green),)
                                                                    );
                                                                  }
                                                                }
                                                            ),
                                                            BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                                                builder: (context, state){
                                                                  if(state is LoadingBookingAcceptDeclineState){
                                                                    return const SizedBox(height: 0);
                                                                  } else {
                                                                    return TextButton(
                                                                      onPressed: (){
                                                                        Navigator.pop(context);
                                                                      },
                                                                      child: Text('${LocaleKeys.cancel.tr()} $noEmoji'),
                                                                    );
                                                                  }
                                                                }
                                                            ),
                                                          ],
                                                        )
                                                    );
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(12.0),
                                                    child: Text(
                                                      LocaleKeys.accept.tr(),
                                                      style: const TextStyle(
                                                          color: Colors.white
                                                      ),
                                                    ),
                                                  )
                                              ),
                                            ),
                                            const SizedBox(width: 10.0),
                                            ClipRRect(
                                              borderRadius: BorderRadius.circular(10.0),
                                              child: ElevatedButton(
                                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red[900])),
                                                  onPressed: () {
                                                    showDialog(
                                                        context: context,
                                                        builder: (context) => AlertDialog(
                                                          title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
                                                          content: BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                                              builder: (context, state){
                                                                if(state is LoadingBookingAcceptDeclineState){
                                                                  return const Text('Please wait.');
                                                                } else {
                                                                  return Text(LocaleKeys.do_you_want_to_decline_the_proposed_booking.tr());
                                                                }
                                                              }
                                                          ),
                                                          actions: [
                                                            BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                                                builder: (context, state){
                                                                  if(state is LoadingBookingAcceptDeclineState){
                                                                    return const CircularProgressIndicator();
                                                                  } else {
                                                                    return TextButton(
                                                                        onPressed: (){
                                                                          AcceptDeclineBookingParams params = AcceptDeclineBookingParams(
                                                                            context: context,
                                                                            bookingId: booking.id,
                                                                            bookingQuoteStatus: 'Declined',
                                                                          );

                                                                          final acceptDeclineBookingBloc = BlocProvider.of<AcceptDeclineBookingBloc>(context);
                                                                          acceptDeclineBookingBloc.add(AcceptDeclineEvent(params: params));

                                                                          final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                                                                          findBookingBloc.add(FindBooking(booking.id));
                                                                        },
                                                                        child: Text(LocaleKeys.decline.tr(), style: const TextStyle(color: Colors.red),)
                                                                    );
                                                                  }
                                                                }
                                                            ),
                                                            BlocBuilder<AcceptDeclineBookingBloc, AcceptDeclineBookingState>(
                                                                builder: (context, state){
                                                                  if(state is LoadingBookingAcceptDeclineState){
                                                                    return const SizedBox(height: 0);
                                                                  } else {
                                                                    return TextButton(
                                                                      onPressed: (){
                                                                        Navigator.pop(context);
                                                                      },
                                                                      child: Text('${LocaleKeys.cancel.tr()} $noEmoji'),
                                                                    );
                                                                  }
                                                                }
                                                            ),
                                                          ],
                                                        )
                                                    );
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(12.0),
                                                    child: isBookingPaid ?
                                                    const Text(
                                                      'Actions',
                                                      style: TextStyle(
                                                          color: Colors.white
                                                      ),
                                                    ) :
                                                    Text(
                                                      LocaleKeys.decline.tr(),
                                                      style: const TextStyle(
                                                          color: Colors.white
                                                      ),
                                                    ),
                                                  )
                                              ),
                                            ),
                                          ],
                                        ) :
                                        ClipRRect(
                                          borderRadius: BorderRadius.circular(10.0),
                                          child: ElevatedButton(
                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green[900])),
                                              onPressed: (){
                                                showDialog(
                                                    context: context,
                                                    builder: (context) => AlertDialog(
                                                      title: Text(LocaleKeys.please_wait.tr()),
                                                      content: Text(LocaleKeys.there_is_no_proposed_estimated_amount_from_the_admin_yet.tr()),
                                                    )
                                                );
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.all(12.0),
                                                child: Text(
                                                  LocaleKeys.wait_for_estimate.tr(),
                                                  style: const TextStyle(
                                                      color: Colors.white
                                                  ),
                                                ),
                                              )
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }
            );
          } else {
            return const Center(
              child: Text('Failed to load booking.'),
            );
          }
        }
      ),
    );
  }
}

class VideoCreator extends StatefulWidget {
  final String? url;
  final String? base64;
  const VideoCreator({Key? key, this.url, this.base64}) : super(key: key);

  @override
  _VideoCreatorState createState() => _VideoCreatorState();
}

class _VideoCreatorState extends State<VideoCreator> {
  VideoPlayerController? _controller;
  ChewieController? chewieController;

  @override
  void initState() {
    super.initState();
    createVideoController();
  }

  Future<VideoPlayerController?> createVideoController() async {
    if(widget.base64 != null){
      var rng = Random();
      Directory tempDir = await getTemporaryDirectory();
      String tempPath = tempDir.path;
      File file = File(tempPath+ (rng.nextInt(100)).toString() +'.mp4');

      file.writeAsBytes(base64.decode(widget.base64!));
      _controller = VideoPlayerController.file(file);
    } else {
      _controller = VideoPlayerController.network(widget.url!);
    }

    chewieController = ChewieController(
      videoPlayerController: _controller!,
      allowFullScreen: false,
      showControls: true,
      looping: false,
      autoPlay: false,

      deviceOrientationsOnEnterFullScreen: [
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight
    ]);

    return _controller;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: createVideoController(),
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
          return AspectRatio(
              aspectRatio: _controller!.value.aspectRatio,
              child: Chewie(
                controller: chewieController!,
              )
          );
        } else {
          return const CircularProgressIndicator();
        }
      }
    );
  }

  @override
  void dispose() {
    _controller!.dispose();
    chewieController!.dispose();
    super.dispose();
  }
}
