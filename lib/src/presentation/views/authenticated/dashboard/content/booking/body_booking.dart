import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/utils/helper/token_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/messages/ask_phone_number_message.dart';
import 'package:kachra_user_app/src/core/utils/messages/registration_required_message.dart';
import 'package:kachra_user_app/src/core/utils/shared_preferences/get_preference.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/booking_preview.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/site_visit_schedule.dart';
import 'package:kachra_user_app/src/presentation/widgets/video_player_on_front_page.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:video_player/video_player.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:visibility_detector/visibility_detector.dart';

class BodyBooking extends StatefulWidget {
  const BodyBooking({Key? key}) : super(key: key);

  @override
  _BodyBookingState createState() => _BodyBookingState();
}

class _BodyBookingState extends State<BodyBooking> {
  List<XFile>? _imageFileList;
  List<XFile>? _videoFileList;

  String assetTypeSelected = '';

  set _imageFile(XFile? value) {
    _imageFileList = value == null ? null : [value];
  }

  set _videoFile(XFile? value) {
    _videoFileList = value == null ? null : [value];
  }

  bool isVideo = false;

  VideoPlayerController? _controller;
  VideoPlayerController? _toBeDisposed;

  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  Future<void> _playVideo(XFile? file) async {
    if (file != null && mounted) {
      await _disposeVideoController();
      late VideoPlayerController controller;
      if (kIsWeb) {
        controller = VideoPlayerController.network(file.path);
      } else {
        controller = VideoPlayerController.file(File(file.path));
      }
      _controller = controller;
      final double volume = kIsWeb ? 0.0 : 1.0;
      await controller.setVolume(volume);
      await controller.initialize();
      await controller.setLooping(true);
      await controller.play();
      setState(() {});
    }
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext? context, bool isMultiImage = false}) async {
    ProfileHive profile = await GetHive().getCurrentUserProfile();
    if(profile.phoneNumber != null){
      bool? saveToPhoneSetting = await GetPreference().getSaveToPhoneSetting();

      if (_imageFileList != null) {
        _imageFileList!.clear();
      }

      if (_videoFileList != null) {
        _videoFileList!.clear();
      }

      if (_controller != null) {
        await _controller!.setVolume(0.0);
      }

      if (isVideo) {
        final XFile? file = await _picker.pickVideo(source: source, maxDuration: const Duration(seconds: 10));
        await _playVideo(file);
        setState(() {
          _videoFile = file;

          if(_videoFileList != null){
            if(saveToPhoneSetting != null && saveToPhoneSetting){
              GallerySaver.saveVideo(File(file!.path).path, albumName: 'Kchra');
            }

            // Navigator.of(context!, rootNavigator: true).push(MaterialPageRoute(builder: (context) => BookingPreview(isVideo: true, file: _videoFileList!, assetTypeSelected: 'Video', controller: _controller)));
            pushNewScreen(context!, screen: BookingPreview(isVideo: true, file: _videoFileList!, assetTypeSelected: 'Video', controller: _controller), withNavBar: false);
          }
        });
      } else {
        try {
          final pickedFile = await _picker.pickImage(source: source);

          await Future.delayed(const Duration(milliseconds: 500), (){});

          setState(() async {
            _imageFile = pickedFile;

            if(_imageFileList != null){
              if(saveToPhoneSetting != null && saveToPhoneSetting){
                GallerySaver.saveImage(pickedFile!.path, albumName: 'Kchra');
              }

              // Navigator.of(context!, rootNavigator: true).push(MaterialPageRoute(builder: (context) => BookingPreview(isVideo: isVideo, file: _imageFileList!, assetTypeSelected: assetTypeSelected)));
              pushNewScreen(context!, screen: BookingPreview(isVideo: isVideo, file: _imageFileList!, assetTypeSelected: assetTypeSelected), withNavBar: false);
            }
          });
        } catch (e) {
          setState(() {});
        }
      }
    } else {
      showDialog(
          context: context!,
          builder: (context) => const AskPhoneNumberMessage()
      );
    }
  }

  @override
  void deactivate() {
    if (_controller != null) {
      _controller!.setVolume(0.0);
      _controller!.pause();
    }
    super.deactivate();
  }

  @override
  void dispose() {
    _disposeVideoController();
    maxWidthController.dispose();
    maxHeightController.dispose();
    qualityController.dispose();
    super.dispose();
  }

  Future<void> _disposeVideoController() async {
    if (_toBeDisposed != null) {
      await _toBeDisposed!.dispose();
    }
    _toBeDisposed = _controller;
    _controller = null;
  }


  Widget _handlePreview() {
    return SingleChildScrollView(
      child: Column(
        children: [
          FutureBuilder(
            future: GetHive().getAppAssets(),
            builder: (context, snapshot) {
              if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
                AppAssetHive appAsset = snapshot.data as AppAssetHive;

                return GestureDetector(
                  onTap:() => Navigator.push(context, MaterialPageRoute(builder: (context) => VideoPlayerOnFrontPage(base64: MyApp.navigatorKey.currentContext!.locale == const Locale('ar') ? appAsset.frontPageAssetArabic : appAsset.frontPageAssetEnglish, newKey: UniqueKey()))),
                  child: Container(
                    height: 30.0.h,
                    width: 100.0.w,
                    decoration: const BoxDecoration(
                      color: Color(0xFFF1F409),
                      image: DecorationImage(
                          image: AssetImage('assets/img/front_3.png'),
                          fit: BoxFit.cover,
                          // colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)
                      ),
                    ),
                  ),
                );
              } else {
                return Container(
                  height: 30.0.h,
                  decoration: const BoxDecoration(
                    color: Colors.green,
                    image: DecorationImage(
                        image: AssetImage('assets/img/front_2.png'),
                        fit: BoxFit.cover),
                  ),
                );
              }
            }
          ),
          SizedBox(
            height: 61.0.h,
            child: Column(
              children: [
                const SizedBox(height: 20.0),
                SizedBox(
                  width: 80.0.w,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: FutureBuilder(
                      future: GetPreference().getIsForRemoval(),
                      builder: (context, snapshot) {
                        if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
                          bool? isForRemoval = snapshot.data as bool?;

                          return RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: '${LocaleKeys.start_a_booking.tr()} ',
                                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 12.0.sp)
                                ),
                                TextSpan(
                                    text: '[${isForRemoval != null  && isForRemoval ? LocaleKeys.for_removal.tr() : LocaleKeys.for_sale.tr()}]',
                                    style: GoogleFonts.poppins(color:isForRemoval != null  && isForRemoval ? Colors.green : Colors.red, fontWeight: FontWeight.w700, fontSize: 8.0.sp)
                                ),
                              ]
                            )
                          );
                        } else {
                          return const CircularProgressIndicator();
                        }
                      }
                    ),
                  ),
                ),
                const SizedBox(height: 20.0),
                Container(
                  height: 50.0.h,
                  width: 95.0.w,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                    // image: DecorationImage(image: const AssetImage('assets/img/front2.png'), fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop)),
                    // border: Border.all(color: Colors.black, width: 1.0)
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ///Open Video
                        GestureDetector(
                          onTap: (){
                            isVideo = true;
                            assetTypeSelected = 'Video';
                            _onImageButtonPressed(ImageSource.camera, context: context);
                          },
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  isVideo = true;
                                  assetTypeSelected = 'Video';
                                  _onImageButtonPressed(ImageSource.camera, context: context);
                                },
                                child: SizedBox(
                                  height: 60.0,
                                  width: 80.0,
                                  // decoration: BoxDecoration(
                                  //   color: Colors.white,
                                  //   borderRadius: BorderRadius.circular(10.0),
                                  //   image: const DecorationImage(image: AssetImage('assets/img/video.png'), fit: BoxFit.cover),
                                  // ),
                                  child: Image.asset('assets/img/video.png'),
                                ),
                              ),
                              SizedBox(
                                height: 70.0,
                                width: 95.0.w-80.0,
                                child: Card(
                                  elevation: 0,
                                  borderOnForeground: true,
                                  child: ListTile(
                                    title: Text(
                                      LocaleKeys.take_a_video.tr()
                                    ),
                                    subtitle: Text(LocaleKeys.record_video_from_your_phone.tr(), style: const TextStyle(fontSize: 10.0),),
                                    trailing: const Icon(Icons.chevron_right),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(height: 5.0),

                        ///Open Camera
                        GestureDetector(
                          onTap: (){
                            isVideo = false;
                            assetTypeSelected = 'Image';
                            _onImageButtonPressed(ImageSource.camera, context: context);
                          },
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  isVideo = false;
                                  assetTypeSelected = 'Image';
                                  _onImageButtonPressed(ImageSource.camera, context: context);
                                },
                                child: SizedBox(
                                  height: 60.0,
                                  width: 80.0,
                                  // decoration: BoxDecoration(
                                  //   color: Colors.white,
                                  //   borderRadius: BorderRadius.circular(10.0),
                                  //   image: const DecorationImage(image: AssetImage('assets/img/camera.png'), fit: BoxFit.contain),
                                  // ),
                                  child: Image.asset('assets/img/camera.png'),
                                ),
                              ),
                              SizedBox(
                                height: 70.0,
                                width: 95.0.w-80.0,
                                child: Card(
                                  elevation: 0,
                                  borderOnForeground: true,
                                  child: ListTile(
                                    title: Text(LocaleKeys.take_a_picture.tr()),
                                    subtitle: Text(LocaleKeys.open_the_phone_camera.tr(), style: const TextStyle(fontSize: 10.0),),
                                    trailing: const Icon(Icons.chevron_right),
                                  ),
                                ),
                              )
                              // ListTile(
                              //   title: Text('Select from Gallery'),
                              // )
                            ],
                          ),
                        ),
                        const SizedBox(height: 5.0),

                        ///Select from image gallery
                        GestureDetector(
                          onTap: (){
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                title: Text(LocaleKeys.select_from_options.tr()),
                                content: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: ElevatedButton(
                                                onPressed: (){
                                                  isVideo = false;
                                                  assetTypeSelected = 'Image';
                                                  _onImageButtonPressed(ImageSource.gallery, context: context);
                                                },
                                                child: Text(
                                                  LocaleKeys.image.tr(),
                                                  style: const TextStyle(
                                                      color: Colors.white
                                                  ),
                                                ),
                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green)),
                                            ),
                                          ),
                                          const SizedBox(width: 10.0),
                                          Expanded(
                                            child: ElevatedButton(
                                                onPressed: (){
                                                  isVideo = true;
                                                  assetTypeSelected = 'Video';
                                                  _onImageButtonPressed(ImageSource.gallery, context: context);
                                                },
                                                child: Text(
                                                  LocaleKeys.video.tr(),
                                                  style: const TextStyle(
                                                      color: Colors.white
                                                  ),
                                                ),
                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              )
                            );
                          },
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: Text(LocaleKeys.select_from_options.tr()),
                                        content: SingleChildScrollView(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: ElevatedButton(
                                                      onPressed: (){
                                                        isVideo = false;
                                                        assetTypeSelected = 'Image';
                                                        _onImageButtonPressed(ImageSource.gallery, context: context);
                                                      },
                                                      child: Text(
                                                        LocaleKeys.image.tr(),
                                                        style: const TextStyle(
                                                            color: Colors.white
                                                        ),
                                                      ),
                                                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green)),
                                                    ),
                                                  ),
                                                  const SizedBox(width: 10.0),
                                                  Expanded(
                                                    child: ElevatedButton(
                                                      onPressed: (){
                                                        isVideo = true;
                                                        assetTypeSelected = 'Video';
                                                        _onImageButtonPressed(ImageSource.gallery, context: context);
                                                      },
                                                      child: Text(
                                                        LocaleKeys.video.tr(),
                                                        style: const TextStyle(
                                                            color: Colors.white
                                                        ),
                                                      ),
                                                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
                                                    ),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                  );
                                },
                                child: SizedBox(
                                  height: 60.0,
                                  width: 80.0,
                                  // decoration: BoxDecoration(
                                  //     color: Colors.green,
                                  //     borderRadius: BorderRadius.circular(10.0),
                                  //     image: const DecorationImage(image: AssetImage('assets/img/imagelib.png'), fit: BoxFit.cover),
                                  // ),
                                  child: Image.asset('assets/img/imagelib.png')
                                ),
                              ),
                              SizedBox(
                                height: 50.0,
                                width: 95.0.w-80.0,
                                child: Card(
                                  elevation: 0,
                                  borderOnForeground: true,
                                  child: ListTile(
                                    title: Text(LocaleKeys.select_from_gallery.tr()),
                                    subtitle: Text(LocaleKeys.select_saved_assets_from_your_gallery.tr(), style: const TextStyle(fontSize: 10.0),),
                                    trailing: const Icon(Icons.chevron_right),
                                  ),
                                ),
                              )
                              // ListTile(
                              //   title: Text('Select from Gallery'),
                              // )
                            ],
                          ),
                        ),
                        const SizedBox(height: 5.0),

                        ///Request a site visit
                        GestureDetector(
                          onTap: () => pushNewScreen(context, screen: const SiteVisitSchedule(), withNavBar: false),
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () => pushNewScreen(context, screen: const SiteVisitSchedule(), withNavBar: false),
                                child: SizedBox(
                                  height: 60.0,
                                  width: 80.0,
                                  // decoration: BoxDecoration(
                                  //   color: Colors.white,
                                  //   borderRadius: BorderRadius.circular(10.0),
                                  //   image: const DecorationImage(image: AssetImage('assets/img/request-site.png'), fit: BoxFit.contain),
                                  // ),
                                  child: Image.asset('assets/img/request-site.png')
                                ),
                              ),
                              SizedBox(
                                height: 70.0,
                                width: 95.0.w-80.0,
                                child: Card(
                                  elevation: 0,
                                  borderOnForeground: true,
                                  child: ListTile(
                                    title: Text(LocaleKeys.request_a_site_visit.tr()),
                                    subtitle: Text(LocaleKeys.schedule_a_visit_to_your_area.tr(), style: const TextStyle(fontSize: 10.0),),
                                    trailing: const Icon(Icons.chevron_right),
                                  ),
                                ),
                              )
                              // ListTile(
                              //   title: Text('Select from Gallery'),
                              // )
                            ],
                          ),
                        ),
                        const SizedBox(height: 5.0),
                        const Divider(thickness: 1.0, color: Colors.black,),
                        GestureDetector(
                          onTap: () => Navigator.popUntil(MyApp.navigatorKey.currentContext!, (Route<dynamic> route) => route.isFirst),
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () => pushNewScreen(context, screen: const SiteVisitSchedule(), withNavBar: false),
                                child: const SizedBox(
                                    height: 50.0,
                                    width: 80.0,
                                    child: Center(child: Icon(CupertinoIcons.home, color: Colors.blue,))
                                ),
                              ),
                              SizedBox(
                                height: 60.0,
                                width: 95.0.w-80.0,
                                child: const Card(
                                  elevation: 0,
                                  borderOnForeground: true,
                                  child: ListTile(
                                    title: Text('Back to Homepage', style: TextStyle(color: Colors.blue),),
                                  ),
                                ),
                              )
                              // ListTile(
                              //   title: Text('Select from Gallery'),
                              // )
                            ],
                          ),
                        ),
                        const SizedBox(height: 26.0),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await _picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      if (response.type == RetrieveType.video) {
        isVideo = true;
        await _playVideo(response.file);
      } else {
        isVideo = false;
        setState(() {
          _imageFile = response.file;
          _imageFileList = response.files;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: !kIsWeb && defaultTargetPlatform == TargetPlatform.android
            ? FutureBuilder<void>(
                future: retrieveLostData(),
                builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return Center(
                        child: Image.asset('assets/img/logo_real.png', scale: 2.0),
                      );
                    case ConnectionState.done:
                      return _handlePreview();
                    default:
                      if (snapshot.hasError) {
                        return Text(
                          'Pick image/video error: ${snapshot.error}}',
                          textAlign: TextAlign.center,
                        );
                      }
                      else {
                        return Center(
                          child: Image.asset('assets/img/logo_real.png', scale: 2.0),
                        );
                      }
                  }
                },
              )
            : _handlePreview(),
      ),
    );
  }
}

typedef OnPickImageCallback = void Function(double? maxWidth, double? maxHeight, int? quality);

class AspectRatioVideo extends StatefulWidget {
  const AspectRatioVideo(this.controller, {Key? key}) : super(key: key);

  final VideoPlayerController? controller;

  @override
  AspectRatioVideoState createState() => AspectRatioVideoState();
}

class AspectRatioVideoState extends State<AspectRatioVideo> {
  VideoPlayerController? get controller => widget.controller;
  bool initialized = false;

  void _onVideoControllerUpdate() {
    if (!mounted) {
      return;
    }
    if (initialized != controller!.value.isInitialized) {
      initialized = controller!.value.isInitialized;
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    controller!.addListener(_onVideoControllerUpdate);
  }

  @override
  void dispose() {
    controller!.removeListener(_onVideoControllerUpdate);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (initialized) {
      return Center(
        child: AspectRatio(
          aspectRatio: controller!.value.aspectRatio,
          child: VideoPlayer(controller!),
        ),
      );
    } else {
      return Container();
    }
  }
}