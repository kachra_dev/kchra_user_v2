import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/utils/helper/token_checker.dart';
import 'package:kachra_user_app/src/core/utils/messages/registration_required_message.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/completed/body_completed_booking.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

class CompletedBooking extends StatefulWidget {
  const CompletedBooking({Key? key}) : super(key: key);

  @override
  _CompletedBookingState createState() => _CompletedBookingState();
}

class _CompletedBookingState extends State<CompletedBooking> {

  @override
  void initState() {
    super.initState();
    getBookings();
  }

  Future<void> getBookings() async {
    final bookingBloc = BlocProvider.of<GetBookingBloc>(context);

    if(bookingBloc.state is SuccessGetBookingState){
    } else {
      bookingBloc.add(const GetBookingsEvent());
    }
  }

  Future<void> syncBookings() async {
    final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
    bookingBloc.add(const GetBookingsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.completed_bookings.tr(), style: const TextStyle(color: Colors.black),),
      ),
      body: const BodyCompletedBooking(),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: 'completed',
        onPressed: () => syncBookings(),
        icon: const Icon(CupertinoIcons.arrow_2_circlepath),
        label: Text(LocaleKeys.sync.tr()),
      )
    );
  }
}
