import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/body_booking.dart';
import 'package:kachra_user_app/src/presentation/widgets/add_location_page.dart';
import 'package:kachra_user_app/src/presentation/widgets/video_player_on_front_page.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../../../../../../../main.dart';
import '../../../../auth_checker.dart';
import '../../../booking_drawer.dart';
import 'package:easy_localization/easy_localization.dart';

class Booking extends StatefulWidget {
  const Booking({Key? key}) : super(key: key);

  @override
  State<Booking> createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  Future<ProfileHive?> getProfile() async {
    await Future.delayed(const Duration(seconds: 1), (){});
    return GetHive().getCurrentUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            iconTheme: const IconThemeData(color: Colors.black),
            //TODO: UNCOMMENT IF NOTIFICATION IS ACTIVE
            actions: [
              FutureBuilder(
                  future: GetHive().getAppAssets(),
                  builder: (context, snapshot) {
                    if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
                      AppAssetHive appAsset = snapshot.data as AppAssetHive;

                      return Padding(
                        padding: const EdgeInsets.only(top: 20.0, right: 20.0),
                        child: ElevatedButton(
                          onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => VideoPlayerOnFrontPage(base64: MyApp.navigatorKey.currentContext!.locale == const Locale('ar') ? appAsset.frontPageAssetArabic : appAsset.frontPageAssetEnglish, newKey: UniqueKey()))),
                          child: Center(child: Text(LocaleKeys.watch_video.tr(), style: const TextStyle(color: Colors.white,fontSize: 10.0, fontWeight: FontWeight.w700))),
                        ),
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.only(top: 20.0, right: 20.0),
                        child: ElevatedButton(
                          onPressed: (){},
                          child: const Center(child: Text('Loading...', style: TextStyle(color: Colors.white,fontSize: 10.0, fontWeight: FontWeight.w700))),
                        ),
                      );
                    }
                  }
              ),
            ],
          ),
          body: FutureBuilder(
            future: getProfile(),
            builder: (context, snapshot) {
              if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
                return const BodyBooking();
              } else {
                return Center(
                  child: Image.asset('assets/img/logo_real.png', scale: 2.0),
                );
              }
            }
          ),
          drawer: const BookingDrawer(),
          // floatingActionButton: FloatingActionButton.extended(
          //   heroTag: 'booking',
          //   onPressed: () {
          //     pushNewScreen(context, screen: const AddLocationPage(), withNavBar: false);
          //   },
          //   icon: const Icon(CupertinoIcons.location),
          //   label: Text(LocaleKeys.add_location.tr()),
          // ),
        ),
      ),
    );
  }
}
