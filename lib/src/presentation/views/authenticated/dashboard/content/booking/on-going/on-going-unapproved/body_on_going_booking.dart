import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/data/models/booking/list_booking_model.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/booking/on-going/on_going_booking_preview.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class BodyOnGoingBooking extends StatefulWidget {
  const BodyOnGoingBooking({Key? key}) : super(key: key);

  @override
  _BodyOnGoingBookingState createState() => _BodyOnGoingBookingState();
}

class _BodyOnGoingBookingState extends State<BodyOnGoingBooking> {
  Future<void> getBookings() async {
    final bookingBloc = BlocProvider.of<GetBookingBloc>(context);
    bookingBloc.add(const GetBookingsEvent());
  }

  List<Color> colors = [
    const Color(0xFFFFBD35),
    const Color(0xFF3FA796),
    const Color(0xFFA6CF98),
    const Color(0xFFF05454)
  ];

  List<String> image = [
    'assets/img/booking1.png',
  ];

  @override
  Widget build(BuildContext context) {
    Map<String, Color> chipColors = {
      LocaleKeys.paid.tr() : const Color(0xFFFFBD35),
      LocaleKeys.accepted.tr(): Colors.green,
      LocaleKeys.pending.tr(): Colors.grey,
      LocaleKeys.declined.tr(): Colors.red
    };

    Map<String, String> chipQuoteStatus = {
      'Paid': LocaleKeys.paid.tr(),
      'Accepted' : LocaleKeys.accepted.tr(),
      'Pending' : LocaleKeys.pending.tr(),
      'Declined' : LocaleKeys.declined.tr()
    };

    return BlocBuilder<GetBookingBloc, GetBookingState>(
        builder: (context, state) {
      if (state is SuccessGetBookingState) {
        List<ListBookingModel>? bookingList = state.bookingList;
        // List<BookingModel> bookingList = [];

        // for (var booking in resBookingList!) {
        //   if (booking.quoteStatus != 'Paid') {
        //     bookingList.add(booking);
        //   }
        // }

        return RefreshIndicator(
          onRefresh: () => getBookings(),
          child: WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: bookingList!.isNotEmpty
                ? ListView.builder(
                    itemCount: bookingList.length,
                    itemBuilder: (context, index) {
                      ListBookingModel booking = bookingList[index];
                      String quoteStatus = booking.quoteStatus == null || booking.quoteStatus == 'Pending'? LocaleKeys.pending.tr() : chipQuoteStatus[booking.quoteStatus]!;
                      String bookingPurpose = booking.bookingPurpose == 'Removal' ? LocaleKeys.for_removal.tr() : LocaleKeys.for_sale.tr();

                      bool isBookingPaid = booking.quoteStatus == 'Paid' ? true : false;
                      bool isForRemoval = booking.bookingPurpose == 'Removal' ? true: false;

                      var outputFormat = DateFormat('MMMM dd, yyyy | hh:mm a');
                      var outputFormatPreferredPickup = DateFormat('MMM dd,yyyy hh:mm a');

                      DateTime resDateTime = DateTime.parse(booking.updatedAt!);
                      resDateTime = resDateTime.add(const Duration(hours: 3));

                      String dateTime = outputFormat.format(resDateTime);
                      String? preferredPickupDAte;
                      if (booking.preferredPickupDate != null) {
                        DateTime tempDateTime = DateTime.parse(booking.preferredPickupDate.toString());
                        preferredPickupDAte = outputFormatPreferredPickup.format(tempDateTime).toString();
                      } else {
                        preferredPickupDAte = null;
                      }

                      String amount = booking.estimatedAmount != null
                          ? booking.estimatedAmount!.toString()
                          : 'TBA';

                      return GestureDetector(
                        onTap: () {
                          final findBookingBloc = BlocProvider.of<FindBookingBloc>(context);
                          findBookingBloc.add(FindBooking(booking.id));

                          pushNewScreen(context, screen: const OnGoingBookingPreview(), withNavBar: false);

                        },
                        child: Column(
                          children: [
                            Container(
                              height: preferredPickupDAte != null ? 250 : 240,
                              width: 90.0.w,
                              decoration: BoxDecoration(
                                color: isBookingPaid ? colors[2] : Colors.grey,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Card(
                                child: Column(
                                  children: [
                                    Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  image[index % image.length]),
                                              fit: BoxFit.cover)),
                                    ),
                                    ListTile(
                                      title: Text(
                                        dateTime,
                                        style: TextStyle(
                                            color: Colors.yellow[900]),
                                      ),
                                      subtitle: preferredPickupDAte != null ?
                                      Directionality(
                                        textDirection: ltrTextDirection,
                                        child: Text(
                                          '${LocaleKeys.pickup_by.tr()}: $preferredPickupDAte',
                                          style: const TextStyle(color: Colors.blue),
                                        ),
                                      ) : null,
                                      trailing: IconButton(
                                          onPressed: () async {},
                                          icon:
                                              const Icon(Icons.chevron_right)),
                                    ),
                                    const Divider(thickness: 1.0),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          30.0, 0, 30.0, 0),
                                      child: Row(
                                        children: [
                                          Chip(
                                            label: Text(TextChecker().checkText(quoteStatus), style: const TextStyle(color: Colors.white, fontSize: 10.0)),
                                            backgroundColor: chipColors[quoteStatus]
                                          ),
                                          const SizedBox(width: 10.0),
                                          Chip(
                                              label: Text(bookingPurpose, style: const TextStyle(color: Colors.white, fontSize: 10.0)),
                                              backgroundColor: isForRemoval ? Colors.green : Colors.red
                                          ),
                                          const Spacer(),
                                          Text('$amount ر.ق '),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 10)
                          ],
                        ),
                      );
                    })
                : RefreshIndicator(
                    onRefresh: () => getBookings(),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/img/pending.png', scale: 2.0),
                          Text(LocaleKeys.no_unapproved_on_going_bookings_yet.tr()),
                        ],
                      ),
                    ),
                  ),
          ),
        );
      } else {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
    });
  }
}
