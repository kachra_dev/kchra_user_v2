import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/core/utils/helper/get_other_location.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/pin_address_google_map.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class EnterManualAddress extends StatefulWidget {
  final double latitude;
  final double longitude;
  final String name;
  const EnterManualAddress({Key? key, required this.latitude, required this.longitude, required this.name}) : super(key: key);

  @override
  _EnterManualAddressState createState() => _EnterManualAddressState();
}

class _EnterManualAddressState extends State<EnterManualAddress> {
  GetOtherLocation? getOtherLocation;
  final _formKey = GlobalKey<FormState>();

  TextEditingController apartmentController = TextEditingController();
  TextEditingController buildingController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController zoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Enter Manual Address', style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Stack(
        children: [
          Container(
            height: 100.0.h,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0)),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding:
                        const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 20.0),
                        child: TextFormField(
                          controller: apartmentController,
                          decoration: InputDecoration(
                            labelText: 'Apartment Number',
                            labelStyle: TextStyle(
                                color: Colors.black, fontSize: 10.0.sp),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                        child: TextFormField(
                          controller: buildingController,
                          decoration: InputDecoration(
                              labelText: 'Building Number',
                              labelStyle: TextStyle(
                                  color: Colors.black, fontSize: 10.0.sp),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                        child: TextFormField(
                          controller: streetController,
                          decoration: InputDecoration(
                              labelText: 'Street Number',
                              labelStyle: TextStyle(
                                  color: Colors.black, fontSize: 10.0.sp),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                        child: TextFormField(
                          controller: zoneController,
                          decoration: InputDecoration(
                              labelText: 'Zone Number',
                              labelStyle: TextStyle(
                                  color: Colors.black, fontSize: 10.0.sp),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                        child: TextFormField(
                          controller: cityController,
                          decoration: InputDecoration(
                              labelText: 'City',
                              labelStyle: TextStyle(
                                  color: Colors.black, fontSize: 10.0.sp),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                      GestureDetector(
                        onTap: () async {
                          final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => PinAddressGoogleMap(latitude: widget.latitude, longitude: widget.longitude, name: widget.name)));

                          setState(() {
                            getOtherLocation = result;
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                          child: TextFormField(
                            enabled: false,
                            decoration: InputDecoration(
                                suffixIcon: const Icon(CupertinoIcons.location),
                                labelText: getOtherLocation != null ? getOtherLocation!.name : 'Enter Blue Plate details',
                                labelStyle: TextStyle(
                                    color: Colors.black, fontSize: 10.0.sp),
                                filled: true,
                                fillColor: Colors.white,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: const BorderSide(
                                    color: Colors.grey,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: const BorderSide(
                                    color: Colors.grey,
                                  ),
                                )),
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.fromLTRB(30.0, 0, 30.0, 10.0),
                        child: SizedBox(
                            width: 100.0.w,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                    MaterialStateProperty.all(
                                        Colors.green[800])),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    GetOtherLocation submitOtherLocation = GetOtherLocation(
                                      name: getOtherLocation!.name.toString(),
                                      isCurrentLocation: false,
                                      isBluePlateNumber: true,
                                      latitude: getOtherLocation!.latitude,
                                      longitude: getOtherLocation!.longitude,
                                      apartmentNumber: apartmentController.text,
                                      buildingNumber: buildingController.text,
                                      streetNumber: streetController.text,
                                      city: cityController.text,
                                      zoneNumber: zoneController.text
                                    );

                                    // Navigator.of(context, rootNavigator: true).pop();
                                    Navigator.pop(context, submitOtherLocation);

                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Text(LocaleKeys.add_location.tr()),
                                ))
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      )
    );
  }
}
