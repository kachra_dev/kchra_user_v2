import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/core/utils/helper/get_other_location.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/enter_manual_address.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/pin_address_google_map.dart';
import 'package:kachra_user_app/src/presentation/widgets/search_location.dart';
import 'package:kachra_user_app/src/presentation/widgets/show_address_details.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class SiteVisitScheduleModalSheet extends StatefulWidget {
  final double? latitude;
  final double? longitude;
  final String? name;

  const SiteVisitScheduleModalSheet({Key? key, this.latitude, this.longitude, this.name}) : super(key: key);

  @override
  _SiteVisitScheduleModalSheetState createState() => _SiteVisitScheduleModalSheetState();
}

class _SiteVisitScheduleModalSheetState extends State<SiteVisitScheduleModalSheet> {
  GetOtherLocation? getOtherLocation;

  chooseLocation() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => AlertDialog(
          insetPadding: EdgeInsets.zero,
          title: Text(LocaleKeys.choose_from_saved_locations.tr()),
          content: listLocation(),
          actions: [
            TextButton(
                onPressed: () =>
                    Navigator.of(context, rootNavigator: true).pop(),
                child: Text(LocaleKeys.close.tr()))
          ],
        ));
  }

  showDetails(UserSavedAddressHive savedAddress) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          insetPadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          content: ShowAddressDetails(savedAddress: savedAddress),
          actions: [
            TextButton(
                onPressed: () =>
                    Navigator.of(context, rootNavigator: true).pop(),
                child: Text(LocaleKeys.close.tr()))
          ],
        ));
  }

  selectLocation(UserSavedAddressHive savedAddress) {
    GetOtherLocation getOtherLocation = GetOtherLocation(
      name: savedAddress.name,
      latitude: double.parse(savedAddress.latitude!),
      longitude: double.parse(savedAddress.longitude!),
      isCurrentLocation: false
    );

    Navigator.of(context, rootNavigator: true).pop();
    Navigator.pop(context, getOtherLocation);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, getOtherLocation);
        return true;
      },
      child: Material(
          child: SafeArea(
            top: false,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                  title: Center(child: Text(LocaleKeys.get_address_methods.tr())),
                ),
                ListTile(
                  title: Text(LocaleKeys.get_current_location.tr()),
                  leading: const Icon(CupertinoIcons.location_circle),
                  onTap: () async {
                    GetOtherLocation getOtherLocation = GetOtherLocation(
                        name: widget.name,
                        latitude: widget.latitude,
                        longitude: widget.longitude,
                        isCurrentLocation: true
                    );
                    Navigator.pop(context, getOtherLocation);
                  },
                ),
                ListTile(
                  title: Text(LocaleKeys.pin_address_in_google_map.tr()),
                  leading: const Icon(CupertinoIcons.location),
                  onTap: () async {
                    final result = await Navigator.push(context, MaterialPageRoute(builder: (context) =>
                      PinAddressGoogleMap(latitude: widget.latitude!, longitude: widget.longitude!, name: widget.name!)));

                    setState(() {
                      getOtherLocation = result;
                    });
                  },
                ),
                ListTile(
                  title: Text(LocaleKeys.enter_address_manually.tr()),
                  leading: const Icon(Icons.location_searching),
                  onTap: () async {
                    final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => EnterManualAddress(latitude: widget.latitude!, longitude: widget.longitude!, name: widget.name!)));

                    setState(() {
                      getOtherLocation = result;
                    });
                  },
                ),
                ListTile(
                  title: Text(LocaleKeys.search_through_saved_addresses.tr()),
                  leading: const Icon(CupertinoIcons.map),
                  onTap: () async => chooseLocation(),
                ),
              ],
            ),
          )),
    );
  }

  Widget listLocation() {
    return FutureBuilder(
        future: GetHive().getSavedAddressesOfCurrentUser(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            List<UserSavedAddressHive> savedAddresses =
            snapshot.data as List<UserSavedAddressHive>;

            return savedAddresses.isNotEmpty
                ? SizedBox(
              height: 60.0.h,
              width: 100.0.w,
              child: SingleChildScrollView(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: savedAddresses.length,
                    itemBuilder: (context, index) {
                      UserSavedAddressHive savedAddress =
                      savedAddresses[index];

                      return Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Card(
                          child: ListTile(
                            leading: const Icon(CupertinoIcons.location,
                                color: Colors.red),
                            title: GestureDetector(
                              onTap: () => selectLocation(savedAddress),
                              child: Text(
                                  savedAddress.addressName ?? 'N/A'),
                            ),
                            trailing: IconButton(
                                onPressed: () =>
                                    showDetails(savedAddress),
                                icon: const Icon(Icons.info_outline)),
                          ),
                        ),
                      );
                    }),
              ),
            )
                : SizedBox(
              height: 20.0.h,
              child: Center(
                child: Text(LocaleKeys.no_saved_addresses.tr()),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
