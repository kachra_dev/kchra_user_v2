import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:kachra_user_app/src/core/params/site_visit_schedule_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/get_other_location.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/messages/ask_phone_number_message.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/presentation/bloc/site_visit_schedule/site_visit_schedule_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/site_visit_schedule_modal_sheet.dart';
import 'package:kachra_user_app/src/presentation/widgets/current_location_map_widget.dart';
import 'package:kachra_user_app/src/presentation/widgets/show_location_map_widget.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:logger/logger.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class SiteVisitSchedule extends StatefulWidget {
  const SiteVisitSchedule({Key? key}) : super(key: key);

  @override
  _SiteVisitScheduleState createState() => _SiteVisitScheduleState();
}

class _SiteVisitScheduleState extends State<SiteVisitSchedule> {
  final format = DateFormat("MMMM-dd-yyy HH:mm a");

  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController noteEstimatorController = TextEditingController();

  GetOtherLocation? getOtherLocation;

  DateTime currentValue = DateTime.now();
  late DateTime dateController;
  late Position location;
  late Placemark place;

  bool isCurrentLocationLoading = false;

  Future<void> getCurrentLocation() async {
    setState(() {
      getOtherLocation = null;
      isCurrentLocationLoading = true;
    });

    addressController.text = 'Finding address...';
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    location = await Geolocator.getCurrentPosition();
    List<Placemark> p = await placemarkFromCoordinates(location.latitude, location.longitude);
    place = p[0];

    addressController.text = place.street.toString() + ', ' + place.administrativeArea.toString();

    setState(() {
      isCurrentLocationLoading = false;
    });
  }

  Future<void> initializeData() async {
    Future.delayed(Duration.zero, (){});
    getCurrentLocation();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initializeData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LocaleKeys.schedule_a_visit_to_your_area.tr(), style: const TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: FutureBuilder(
        future: GetHive().getCurrentUserProfile(),
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
            ProfileHive currentProfile = snapshot.data as ProfileHive;
            nameController.text = currentProfile.name!;

            return Center(
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      height: 60.0.h,
                      width: 100.0.w,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        image: DecorationImage(
                            image: const AssetImage('assets/img/front.png'),
                            fit: BoxFit.cover,
                            colorFilter: ColorFilter.mode(Colors.grey.withOpacity(0.5), BlendMode.dstATop)
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: getOtherLocation != null &&
                            getOtherLocation!.isCurrentLocation == false
                            ? ShowLocationMapWidget(
                            latitude: getOtherLocation!.latitude!,
                            longitude: getOtherLocation!.longitude!,
                            name: getOtherLocation!.name!
                        )
                            : const CurrentLocationMapWidget(isHideAddress: true),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: SingleChildScrollView(
                      child: Container(
                        height: 400,
                        width: 100.0.w,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: const BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                          border: Border.all(color: Colors.black, width: 2.0),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0),
                              child: SizedBox(
                                height: 280,
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(LocaleKeys.your_name.tr(), style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.green)),
                                      TextField(
                                        controller: nameController,
                                      ),
                                      const SizedBox(height: 10.0),
                                      Text(LocaleKeys.your_address.tr(), style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.green)),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: TextField(
                                              enabled: false,
                                              controller: addressController,
                                            ),
                                          ),
                                          isCurrentLocationLoading ? const CircularProgressIndicator() : SizedBox(
                                            width: 60.0,
                                            child: IconButton(
                                                onPressed: () async {
                                                  final result = await showCupertinoModalBottomSheet(
                                                      context: context,
                                                      backgroundColor: Colors.transparent,
                                                      builder: (context) =>
                                                          SiteVisitScheduleModalSheet(latitude: location.latitude, longitude: location.longitude, name: place.name)
                                                  );

                                                  setState(() {
                                                    var logger = Logger();
                                                    logger.i(result);

                                                    getOtherLocation = result;
                                                    addressController.text = getOtherLocation != null ? getOtherLocation!.name! : addressController.text;
                                                  });
                                                },
                                                icon: const Icon(Icons.add, color: Colors.blue,)
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(height: 10.0),
                                      Text(LocaleKeys.date_and_time_of_schedule.tr(), style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.green)),
                                      const SizedBox(height: 10.0),
                                      Container(
                                        height: 90,
                                        width: 90.0.w,
                                        decoration: BoxDecoration(
                                            color: Colors.green[100],
                                            borderRadius: BorderRadius.circular(15.0)
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Container(
                                                height: 70,
                                                width: 60.0.w,
                                                decoration: BoxDecoration(
                                                    color: Colors.green[900],
                                                    borderRadius: BorderRadius.circular(15.0)
                                                ),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(15.0),
                                                  child: Row(
                                                    children: [
                                                      SizedBox(
                                                        width: 50.0,
                                                        child: Column(
                                                          children: [
                                                            Text(
                                                              DateFormat('MMM').format(DateTime(0, currentValue.month)).toString(),
                                                              style: const TextStyle(
                                                                  color: Colors.white,
                                                                  fontWeight: FontWeight.w500
                                                              ),
                                                            ),
                                                            Text(
                                                              currentValue.day.toString(),
                                                              style: TextStyle(
                                                                  color: Colors.orange[200],
                                                                  fontWeight: FontWeight.w700,
                                                                  fontSize: 18.0
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      const VerticalDivider(color: Colors.white,thickness: 1.0),
                                                      Expanded(
                                                        child: Column(
                                                          children: [
                                                            Text(
                                                              DateFormat('EEEE').format(currentValue),
                                                              style: const TextStyle(
                                                                  color: Colors.white,
                                                                  fontWeight: FontWeight.w600,
                                                                  fontSize: 15.0
                                                              ),
                                                            ),
                                                            Text(
                                                              DateFormat.jm().format(currentValue),
                                                              style: TextStyle(
                                                                  color: Colors.orange[200],
                                                                  fontWeight: FontWeight.w500
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              const Spacer(),
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  IconButton(
                                                      onPressed: () async {
                                                        final date = await showDatePicker(
                                                            context: context,
                                                            firstDate: DateTime(DateTime.now().year),
                                                            initialDate: currentValue,
                                                            lastDate: DateTime(DateTime.now().year + 1));
                                                        if (date != null) {
                                                          final time = await showTimePicker(
                                                            context: context,
                                                            initialTime: TimeOfDay.fromDateTime(currentValue),
                                                          );

                                                          DateTime dateSchedule = DateTimeField.combine(date, time);
                                                          setState(() {
                                                            currentValue = dateSchedule;
                                                            // dateController = dateSchedule;
                                                          });
                                                        }
                                                      },
                                                      icon: const Icon(Icons.calendar_today_outlined, color: Colors.blue),
                                                    tooltip: 'Change Schedule',
                                                  ),
                                                  const Text('Change \nSchedule', style: TextStyle(fontSize: 8.0, color: Colors.blue), textAlign: TextAlign.center,)
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const Spacer(),
                            GestureDetector(
                              onTap: (){
                                AddressModel address =
                                getOtherLocation != null ?
                                getOtherLocation!.isBluePlateNumber != null && getOtherLocation!.isBluePlateNumber! ?  AddressModel(
                                  latitude: getOtherLocation!.latitude.toString(),
                                  longitude: getOtherLocation!.longitude.toString(),
                                  name: getOtherLocation!.name.toString(),
                                  apartmentNumber: getOtherLocation!.apartmentNumber.toString(),
                                  buildingNumber: getOtherLocation!.buildingNumber.toString(),
                                  streetNumber: getOtherLocation!.streetNumber.toString(),
                                  city: getOtherLocation!.city.toString(),
                                  zone: getOtherLocation!.zoneNumber.toString(),
                                  hasBluePlateNumber: 1
                                ) : AddressModel(
                                  latitude: getOtherLocation!.latitude.toString(),
                                  longitude: getOtherLocation!.longitude.toString(),
                                  name: getOtherLocation!.name.toString(),
                                ) : AddressModel(
                                    name: place.name,
                                    country: place.country,
                                    administrativeArea: place.administrativeArea,
                                    isoCountryCode: place.isoCountryCode,
                                    street: place.street,
                                    locality: place.locality,
                                    latitude: location.latitude.toString(),
                                    longitude: location.longitude.toString()
                                );

                                SiteVisitScheduleParams siteVisitScheduleParams = SiteVisitScheduleParams(
                                  scheduledDate: currentValue.toString(),
                                  userId: currentProfile.id,
                                  address: address
                                );

                                if(currentProfile.phoneNumber != null){
                                  showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: Text(LocaleKeys.confirm.tr()),
                                        content: Text('${LocaleKeys.are_you_sure_you_want_to_request_a_site_visit.tr()} $confirmEmoji'),
                                        actions: [
                                          BlocBuilder<SiteVisitScheduleBloc, SiteVisitScheduleState>(
                                              builder: (context, state){
                                                if(state is LoadingSiteVisitScheduleState){
                                                  return const CircularProgressIndicator();
                                                } else {
                                                  return TextButton(
                                                      onPressed: (){
                                                        final siteVisitScheduleBloc = BlocProvider.of<SiteVisitScheduleBloc>(context);
                                                        siteVisitScheduleBloc.add(CreateSiteVisitScheduleEvent(siteVisitScheduleParams));
                                                      },
                                                      child: Text('${LocaleKeys.request_now.tr()} $yesEmoji', style: const TextStyle(color: Colors.green))
                                                  );
                                                }
                                              }
                                          ),
                                          BlocBuilder<SiteVisitScheduleBloc, SiteVisitScheduleState>(
                                              builder: (context, state){
                                                if(state is LoadingSiteVisitScheduleState){
                                                  return const SizedBox(height: 0);
                                                } else {
                                                  return TextButton(
                                                      onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
                                                      child: Text('${LocaleKeys.cancel.tr()} $noEmoji')
                                                  );
                                                }
                                              }
                                          )
                                        ],
                                      )
                                  );
                                } else {
                                  showDialog(
                                      context: context,
                                      builder: (context) => const AskPhoneNumberMessage()
                                  );
                                }
                              },
                              child: Container(
                                height: 70,
                                width: 100.0.w,
                                decoration: const BoxDecoration(
                                  color: Colors.green,
                                ),
                                child: Padding(
                                    padding: const EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 0),
                                    child: Center(
                                      child: Text(LocaleKeys.request_a_site_visit.tr(), style: const TextStyle(color: Colors.white, fontSize: 12.0, fontWeight: FontWeight.w600),),
                                    )
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      )
    );
  }
}
