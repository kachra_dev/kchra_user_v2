import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kachra_user_app/src/core/utils/helper/future_delay_time_const.dart';
import 'package:kachra_user_app/src/core/utils/helper/get_other_location.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';
import 'package:kachra_user_app/src/presentation/widgets/search_location.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:sizer/sizer.dart';

class PinAddressGoogleMap extends StatefulWidget {
  final bool? isFromBooking;
  final double latitude;
  final double longitude;
  final String name;
  const PinAddressGoogleMap({Key? key, this.isFromBooking, required this.latitude, required this.longitude, required this.name}) : super(key: key);

  @override
  _PinAddressGoogleMapState createState() => _PinAddressGoogleMapState();
}

class _PinAddressGoogleMapState extends State<PinAddressGoogleMap> {
  late BitmapDescriptor customMarker;

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  final Completer<GoogleMapController> _controller = Completer();
  late CameraPosition _initLocation;

  StreamController<Set<Marker>> streamController = StreamController();
  late Stream<Set<Marker>> markerStream;
  late List<Placemark> p;

  Future<Uint8List> getBytesFromAsset({required String path,required int width})async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(
        data.buffer.asUint8List(),
        targetWidth: width
    );
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(
        format: ui.ImageByteFormat.png))!.buffer.asUint8List();
  }

  Future _addMarkerLongPressed(LatLng location) async {
    const MarkerId markerId = MarkerId("RANDOM_ID");
    Marker marker = Marker(
      markerId: markerId,
      draggable: true,
      position: location, //With this parameter you automatically obtain latitude and longitude
      infoWindow: const InfoWindow(
        title: "Marker here",
        snippet: 'This looks good',
      ),
      icon: BitmapDescriptor.defaultMarker
    );

    markers[markerId] = marker;

    streamController.add(Set<Marker>.of(markers.values));

    //This is optional, it will zoom when the marker has been created
    GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLngZoom(location, 17.0));
  }

  Future<CameraPosition> getLocation() async{
    markerStream = streamController.stream.asBroadcastStream();

    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    MarkerId markerId = const MarkerId('RANDOM_ID');

    _initLocation = CameraPosition(
      target: markers.isEmpty ? LatLng(widget.latitude, widget.longitude) : LatLng(markers[markerId]!.position.latitude, markers[markerId]!.position.longitude),
      zoom: 14,
    );

    p = await placemarkFromCoordinates(widget.latitude, widget.longitude);

    if(Platform.isIOS){
      customMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(),
        "assets/img/marker-3x.png",
      );
    } else {
      customMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(),
        "assets/img/marker.png",
      );
    }

    final marker = Marker(
      markerId: MarkerId(widget.name),
      position: LatLng(widget.latitude, widget.longitude),
        icon: customMarker
    );

    markers[MarkerId(widget.name)] = marker;

    return _initLocation;
  }

  Future<void> pinLocation() async {
    if (markers[const MarkerId('RANDOM_ID')] != null) {
      p = await placemarkFromCoordinates(
          markers[const MarkerId('RANDOM_ID')]!.position.latitude,
          markers[const MarkerId('RANDOM_ID')]!.position.longitude);

      if(widget.isFromBooking != null && widget.isFromBooking!){
        UserSavedAddressHive pinLocationUserSavedAddressHiveFormat = UserSavedAddressHive(
          name: '${p[0].name.toString()}, ${p[0].street.toString()}',
          street: p[0].street.toString(),
          administrativeArea: p[0].administrativeArea.toString(),
          locality: p[0].locality.toString(),
          country: p[0].country.toString(),
          iSOCountryCode: p[0].isoCountryCode.toString(),
          latitude: markers[const MarkerId('RANDOM_ID')]!.position.latitude.toString(),
          longitude: markers[const MarkerId('RANDOM_ID')]!.position.longitude.toString(),
        );

        Navigator.pop(context, pinLocationUserSavedAddressHiveFormat);
      } else {
        GetOtherLocation getOtherLocation = GetOtherLocation(
            name: '${p[0].name.toString()}, ${p[0].street.toString()}',
            latitude: markers[const MarkerId('RANDOM_ID')]!.position.latitude,
            longitude: markers[const MarkerId('RANDOM_ID')]!.position.longitude,
            isCurrentLocation: false
        );

        Navigator.pop(context, getOtherLocation);
      }
    } else {
      showDialog(
        barrierDismissible: false,
          context: context,
          builder: (context) =>
              ErrorMessage(title: LocaleKeys.error.tr(),
                content: 'No selected Location.',
                // onPressedFunction: () => Navigator.of(context, rootNavigator: true).pop()
              )
      );

      await Future.delayed(const Duration(seconds: futureDelayTimeConst), (){

      });

      Navigator.of(context, rootNavigator: true).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pin Location', style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: FutureBuilder(
        future: getLocation(),
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.done){
            return StreamBuilder<Set<Marker>>(
              initialData: Set<Marker>.of(markers.values),
              stream: markerStream,
              builder: (context, snapshot) {
                return Stack(
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: GoogleMap(
                            mapType: MapType.normal,
                            myLocationEnabled: true,
                            myLocationButtonEnabled: true,
                            initialCameraPosition: _initLocation,
                            onMapCreated: (GoogleMapController controller) {
                              _controller.complete(controller);
                            },
                            compassEnabled: true,
                            tiltGesturesEnabled: false,
                            // onTap: (location){
                            //   _addMarkerLongPressed(location);
                            // },
                            markers: Set<Marker>.of(markers.values),
                            onCameraMove: (CameraPosition cameraPosition) async {
                              if(markers.isNotEmpty) {
                                MarkerId markerId = const MarkerId('RANDOM_ID');
                                Marker marker = Marker(
                                    markerId: markerId,
                                    draggable: true,
                                    position: cameraPosition.target,
                                    infoWindow: const InfoWindow(
                                      title: "Marker here",
                                      snippet: 'This looks good',
                                    ),
                                    icon: customMarker
                                );

                                Marker updatedMarker = marker.copyWith(positionParam: cameraPosition.target);

                                markers[markerId] = updatedMarker;
                                streamController.add(Set<Marker>.of(markers.values));
                              }
                            },
                            onCameraIdle: () async {
                              if(markers[const MarkerId('RANDOM_ID')] != null){
                                p = await placemarkFromCoordinates(
                                    markers[const MarkerId('RANDOM_ID')]!.position.latitude,
                                    markers[const MarkerId('RANDOM_ID')]!.position.longitude);
                              }
                            },
                          )
                      ),
                      GestureDetector(
                        onTap: () async {
                          GetOtherLocation result = await Navigator.push(context, MaterialPageRoute(builder: (context) => const SearchLocation()));

                          MarkerId markerId = const MarkerId('RANDOM_ID');
                          Marker marker = Marker(
                              markerId: markerId,
                              draggable: true,
                              position: LatLng(result.latitude!, result.longitude!),
                              infoWindow: const InfoWindow(
                                title: "Marker here",
                                snippet: 'This looks good',
                              ),
                              icon: customMarker
                          );

                          Marker updatedMarker = marker.copyWith(positionParam: LatLng(result.latitude!, result.longitude!));

                          markers[markerId] = updatedMarker;
                          p = await placemarkFromCoordinates(result.latitude!, result.longitude!);

                          final GoogleMapController controller = await _controller.future;
                          controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
                            target: LatLng(result.latitude!, result.longitude!),
                            zoom: 15
                          )));

                          streamController.add(Set<Marker>.of(markers.values));
                        },
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              height: 60,
                              width: 100.0.w,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0)
                              ),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: ListTile(
                                    title: Text('${p[0].name}, ${p[0].locality}', style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0)),
                                    leading: const Icon(CupertinoIcons.location),
                                  )
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                            padding: const EdgeInsets.only(bottom: 30.0),
                            child: GestureDetector(
                              onTap: () async => pinLocation(),
                              child: Container(
                                height: 60,
                                width: 90.0.w,
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.circular(10.0)
                                ),
                                child: const Center(
                                  child: Text('Pin Location', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 15.0),),
                                ),
                              ),
                            )
                        ),
                      )
                    ]
                );
              }
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        }
      ),
    );
  }
}
