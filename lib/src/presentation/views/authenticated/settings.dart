import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          onPressed: () async {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
                content: Text(LocaleKeys.are_you_sure_you_want_to_log_out.tr()),
                actions: [
                  TextButton(
                    onPressed: (){
                      final authBloc = BlocProvider.of<AuthBloc>(context);
                      authBloc.add(const LogOutEvent());

                      Navigator.pop(context);
                    },
                    child: Text(LocaleKeys.log_out.tr())
                  ),
                  TextButton(
                      onPressed: (){},
                      child: Text('${LocaleKeys.cancel.tr()} $noEmoji'),
                  )
                ],
              )
            );
          },
          child: const Text('Log Out')
        ),
      ),
    );
  }
}
