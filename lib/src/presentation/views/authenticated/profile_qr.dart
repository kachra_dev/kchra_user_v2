import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:qr_flutter/qr_flutter.dart';

class ProfileQR extends StatelessWidget {
  const ProfileQR({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Profile', style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: FutureBuilder(
          future: GetHive().getCurrentUserProfile(),
          builder: (context, snapshot) {
            if(snapshot.connectionState == ConnectionState.done){
              ProfileHive profile = snapshot.data as ProfileHive;

              Map<String, dynamic> qrDataJson = {
                '"id"': profile.id!,
                '"name"': profile.name!
              };


              return Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    QrImage(
                      data: qrDataJson.toString(),
                      size: 200.0,
                    ),
                    const SizedBox(height: 10.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Name:'),
                        const SizedBox(width: 5.0),
                        Text(
                            profile.name!
                        )
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    profile.email != null ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Email:'),
                        const SizedBox(width: 5.0),
                        Text(
                            profile.email!
                        )
                      ],
                    ) : const SizedBox(height: 0),
                    const SizedBox(height: 10.0),
                    profile.phoneNumber != null ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Phone Number:'),
                        const SizedBox(width: 5.0),
                        Text(
                            profile.phoneNumber!
                        )
                      ],
                    ) : const SizedBox(height: 0),
                    const SizedBox(height: 15.0),
                    const Text('Address:'),
                    const SizedBox(height: 5.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Name:'),
                        const SizedBox(width: 5.0),
                        Text(
                            profile.address!.name ?? 'N/A'
                        )
                      ],
                    ),
                    const SizedBox(height: 2.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Street:'),
                        const SizedBox(width: 5.0),
                        Text(
                            profile.address!.street ?? 'N/A'
                        )
                      ],
                    ),
                    const SizedBox(height: 2.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Locality:'),
                        const SizedBox(width: 5.0),
                        Text(
                            profile.address!.locality ?? 'N/A'
                        )
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    profile.country != null ? Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Country:'),
                        const SizedBox(width: 5.0),
                        Text(
                            profile.country!
                        )
                      ],
                    ) : const SizedBox(height: 0),
                    profile.isGuest == 1 ? const Text('Guest', style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600, color: Colors.blue)) : const SizedBox(height: 0)
                  ],
                ),
              );
            } else {
              return const Center(
                  child: CircularProgressIndicator()
              );
            }
          }
      ),
    );
  }
}
