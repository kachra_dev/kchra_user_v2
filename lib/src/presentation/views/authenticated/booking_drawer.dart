import 'dart:async';

import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/helper/token_checker.dart';
import 'package:kachra_user_app/src/core/utils/messages/registration_required_message.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_notifications/get_notifications_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/settings/global_settings.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/menu_page.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/profile_qr.dart';
import 'package:kachra_user_app/src/presentation/widgets/saved_addresses_page.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../main.dart';
import '../auth_checker.dart';
import 'dashboard/content/booking/booking_notifications/booking_notifications.dart';

class BookingDrawer extends StatefulWidget {
  const BookingDrawer({Key? key}) : super(key: key);

  @override
  State<BookingDrawer> createState() => _BookingDrawerState();
}

class _BookingDrawerState extends State<BookingDrawer> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  Future<void> initData() async {
    PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
              child: Image.asset('assets/img/logo_real.png', scale: 2.0)),
          ListTile(
            onTap: () async => Navigator.popUntil(MyApp.navigatorKey.currentContext!, (Route<dynamic> route) => route.isFirst),
            leading: const Icon(
              CupertinoIcons.home,
              color: Colors.blue,
            ),
            title: Text(
              'Back to Home Page',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          ListTile(
            onTap: () {
              final notificationsBloc = BlocProvider.of<GetNotificationsBloc>(context);
              notificationsBloc.add(const GetBookingNotificationsEvent());

              pushNewScreen(context, screen: const BookingNotifications(), withNavBar: false);
            },
            leading: const Icon(
              CupertinoIcons.bell,
              color: Colors.black,
            ),
            title: Text(
              'Notifications',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          ListTile(
            onTap: () {
              pushNewScreen(context, screen: const ProfileQR(), withNavBar: false);
            },
            leading: const Icon(
              CupertinoIcons.qrcode,
              color: Colors.black,
            ),
            title: Text(
              'Profile QR',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          ListTile(
            onTap: () {
              pushNewScreen(context, screen: const SavedAddresses(), withNavBar: false);
            },
            leading: const Icon(
              CupertinoIcons.location,
              color: Colors.black,
            ),
            title: Text(
              LocaleKeys.saved_addresses.tr(),
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          Column(
            children: [
              ExpansionTile(
                title: const Text('Legal Documents'),
                leading: const Icon(Icons.book),
                initiallyExpanded: false,
                children: [
                  ListTile(
                    onTap: () async {
                      String url = 'https://drive.google.com/file/d/17-sdnWrHvaqw-LEywEQ35B03jk5a3YVk/view?usp=sharing';
                      if (!await launch(url)) throw 'Could not launch $url';
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Terms and Conditions',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      String url = 'https://drive.google.com/file/d/1v0j00iwmtOeL5DLiT_83skjK4mCMzrfo/view?usp=sharing';
                      if (!await launch(url)) throw 'Could not launch $url';
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Privacy Policy',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      String url = 'https://drive.google.com/file/d/1H1wbHAK_1jOKOXSy56t8yjaSuAq1n-IG/view?usp=sharing';
                      if (!await launch(url)) throw 'Could not launch $url';
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Disclaimer',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      String url = 'https://drive.google.com/file/d/14nx2kaLkluL9F5AsikEsRSiEGbZI-0rE/view?usp=sharing';
                      if (!await launch(url)) throw 'Could not launch $url';
                    },
                    leading: const Icon(
                      CupertinoIcons.doc_text_fill,
                      color: Colors.black,
                    ),
                    title: Text(
                      'Copyright Policy',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                ],
              )
            ],
          ),
          Column(
            children: [
              ExpansionTile(
                title: const Text('Languages'),
                leading: const Icon(Icons.language),
                initiallyExpanded: false,
                children: [
                  ListTile(
                    onTap: () async {
                      await MyApp.navigatorKey.currentContext!.setLocale(const Locale('en'));
                      Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);

                      MyApp.restartApp(context);
                    },
                    leading: const Icon(
                      CupertinoIcons.bookmark_solid,
                      color: Colors.black,
                    ),
                    title: Text(
                      'English',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      await MyApp.navigatorKey.currentContext!.setLocale(const Locale('ar'));
                      Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);

                      MyApp.restartApp(context);
                    },
                    leading: const Icon(
                      CupertinoIcons.bookmark_solid,
                      color: Colors.black,
                    ),
                    title: Text(
                      'عربي',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                ],
              )
            ],
          ),
          ListTile(
            onTap: () async {
              pushNewScreen(context, screen: const GlobalSettings(), withNavBar: false);
            },
            leading: const Icon(
              CupertinoIcons.gear,
              color: Colors.black,
            ),
            title: Text(
              'Settings',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red)),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: Text('${LocaleKeys.confirm.tr()} $confirmEmoji'),
                        content:
                        Text(LocaleKeys.are_you_sure_you_want_to_log_out.tr()),
                        actions: [
                          TextButton(
                              onPressed: () {
                                final authBloc =
                                BlocProvider.of<AuthBloc>(context);
                                authBloc.add(const LogOutEvent());

                                Navigator.pop(context);
                              },
                              child: Text('${LocaleKeys.log_out.tr()} $yesEmoji')),
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('${LocaleKeys.cancel.tr()} $noEmoji')),
                        ],
                      ));
                },
                child: Text(LocaleKeys.log_out.tr())),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("Version: ${_packageInfo.version}"),
          ),
        ],
      ),
    );
  }
}
