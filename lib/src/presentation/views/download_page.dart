import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/core/utils/hive/add_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

import 'auth_checker.dart';

class DownloadPage extends StatefulWidget {
  final String? latestUrlLink;
  final String description;
  final bool isAppAsset;
  const DownloadPage({Key? key, this.latestUrlLink, required this.description, required this.isAppAsset}) : super(key: key);

  @override
  State<DownloadPage> createState() => _DownloadPageState();
}

class _DownloadPageState extends State<DownloadPage> {
  late http.StreamedResponse _responseArabic;
  late http.StreamedResponse _responseEnglish;

  late String base64Arabic;
  late String base64English;

  int _total = 0, _received = 0, count = 0;
  final List<int> _bytesArabic = [];
  final List<int> _bytesEnglish = [];

  List<_TaskInfo>? _tasks;
  String? task;
  final ReceivePort _port = ReceivePort();
  int? progressDownload;

  Map<int, dynamic> progressDownloadAssets = {};
  Map<int, dynamic> downloaded= {};

  String downloadDescription = 'Loading Assets...';

  bool isLoading = true;

  Future<String?> _findLocalPath() async {
    String? externalStorageDirPath;
    if (Platform.isAndroid) {
      try {
        Directory dir = Directory('/storage/emulated/0/Download');
        externalStorageDirPath = dir.path;
      } catch (e) {
        final directory = await getExternalStorageDirectory();
        externalStorageDirPath = directory?.path;
      }
    } else if (Platform.isIOS) {
      externalStorageDirPath = (await getApplicationDocumentsDirectory()).absolute.path;
    }
    return externalStorageDirPath;
  }

  Future<void> updateApp() async {
    String _localPath = (await _findLocalPath())!;
    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }

    String? taskQueue = await FlutterDownloader.enqueue(
      url: widget.latestUrlLink!,
      savedDir: _localPath,
      showNotification: true, // show download progress in status bar (for Android)
      openFileFromNotification: true,
      saveInPublicStorage: true
    );

    final tasks = await FlutterDownloader.loadTasks();

    _tasks = [];
    _tasks!.add(_TaskInfo(name: 'user app', link: widget.latestUrlLink));

    for (var task in tasks!) {
      for (_TaskInfo info in _tasks!) {
        if (info.link == task.url) {
          info.taskId = taskQueue;
          info.status = task.status;
          info.progress = task.progress;
        }
      }
    }
  }

  Future<void> downloadAppAsset() async {
    await _downloadArabicYoutubeLink().then((value) => _downloadEnglishYoutubeLink());
  }

  Future<void> _downloadArabicYoutubeLink() async {
    _responseArabic = await http.Client().send(http.Request('GET', Uri.parse(arabicYoutubeLink)));

    _total = _responseArabic.contentLength ?? 0;

    _responseArabic.stream.listen((value) {
      setState(() {
        _bytesArabic.addAll(value);
        _received += value.length;

        progressDownload = ((_received/_total) * 100).toInt();
      });
    }).onDone(() {
      setState(() {
        count++;

        ///Get the video URL, parse it and get the Response object
        Uint8List bytes = Uint8List.fromList(_bytesArabic);

        ///Convert to base64
        String base64Asset = base64Encode(bytes);

        base64Arabic = base64Asset;

        if(count == 1){
          downloadDescription = 'We are halfway there. Please wait...';
        } else if(count == 2){
          isLoading = false;
          downloadDescription = 'All Done!';

          AppAssetHive appAssetHive = AppAssetHive(
            frontPageAssetEnglish: base64English,
            frontPageAssetArabic: base64Arabic
          );

          AddHive().addAppAsset(appAssetHive);
        }
      });
    });
  }

  Future<void> _downloadEnglishYoutubeLink() async {
    _responseEnglish = await http.Client().send(http.Request('GET', Uri.parse(englishYoutubeLink)));

    _total = _responseEnglish.contentLength ?? 0;

    _responseEnglish.stream.listen((value) {
      setState(() {
        _bytesEnglish.addAll(value);
        _received += value.length;

        progressDownload = ((_received/_total) * 100).toInt();
      });
    }).onDone(() {
      setState(() {
        count++;

        ///Get the video URL, parse it and get the Response object
        Uint8List bytes = Uint8List.fromList(_bytesEnglish);

        ///Convert to base64
        String base64Asset = base64Encode(bytes);

        base64English = base64Asset;

        if(count == 1){
          downloadDescription = 'We are halfway there. Please wait...';
        } else if(count == 2){
          isLoading = false;
          downloadDescription = 'All Done!';

          AppAssetHive appAssetHive = AppAssetHive(
              frontPageAssetEnglish: base64English,
              frontPageAssetArabic: base64Arabic
          );

          AddHive().addAppAsset(appAssetHive);
        }
      });
    });
  }

  static void downloadCallback(String id, DownloadTaskStatus status, int progress) {
    final SendPort send = IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  void _bindBackgroundIsolate() {
    bool isSuccess = IsolateNameServer.registerPortWithName(_port.sendPort, 'downloader_send_port');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      String? id = data[0];
      DownloadTaskStatus? status = data[1];
      int? progress = data[2];

      if (_tasks != null && _tasks!.isNotEmpty) {
        final task = _tasks!.firstWhere((task) => task.taskId == id);

        setState(() {
          task.status = status;
          task.progress = progress;
          progressDownload = task.progress;

          if(task.status == DownloadTaskStatus.complete){
            FlutterDownloader.open(taskId: id!);
            isLoading = false;
          }
        });
      }
    });
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.isAppAsset){
      downloadAppAsset();
    } else {
      _bindBackgroundIsolate();
      FlutterDownloader.registerCallback(downloadCallback);

      updateApp();
    }
  }

  @override
  void dispose() {
    _unbindBackgroundIsolate();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/img/logo_real.png', scale: 2.0),
            const SizedBox(height: 20.0),
            Center(child: Text(widget.description, textAlign: TextAlign.center)),
            const SizedBox(height: 20.0),
            !widget.isAppAsset ? Text('${progressDownload ?? '0'}% downloaded') : Text(downloadDescription),
            const SizedBox(height: 20.0),
            isLoading ? const CircularProgressIndicator() : const SizedBox(height: 0),
            !isLoading && widget.isAppAsset ?
              ElevatedButton(
                onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker())),
                child: const Text('Go To App')
              ) : const SizedBox(height: 0),
          ],
        ),
      ),
    );
  }
}

class _TaskInfo {
  final String? name;
  final String? link;

  String? taskId;
  int? progress = 0;
  DownloadTaskStatus? status = DownloadTaskStatus.undefined;

  _TaskInfo({this.name, this.link});
}
