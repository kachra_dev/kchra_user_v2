import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';

class EnterAsNewGuestRequest extends StatelessWidget {
  const EnterAsNewGuestRequest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        leading: IconButton(
          onPressed: () => Navigator.popUntil(
              context, (Route<dynamic> route) => route.isFirst),
          icon: const Icon(
            Icons.keyboard_arrow_left_outlined,
            color: Colors.black,
          ),
        ));
    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: SizedBox(
          height: 100.0.h - (MediaQuery.of(context).padding.top + appBar.preferredSize.height),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  LocaleKeys.are_you_entering_as_a_new_guest.tr(),
                  style: GoogleFonts.lato(
                    fontSize: 25.0,
                    fontWeight: FontWeight.w600,
                    color: const Color(0xFF125C13),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              const Spacer(),
              Image.asset('assets/img/enter_guest.gif'),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.green),
                            fixedSize:
                                MaterialStateProperty.all(Size(80.0.w, 70.0))),
                        onPressed: () => pushNewScreen(context, screen: const ProvidePhoneNumberRequest(), withNavBar: false),
                        child: Text(
                          LocaleKeys.yes.tr(),
                          style: const TextStyle(fontSize: 20.0),
                        )),
                    TextButton(
                        onPressed: () => pushNewScreen(context, screen: const LoginUsingPhoneNumber()),
                        child: Text(
                          LocaleKeys.no_i_have_already_registered_as_a_new_guest.tr(),
                          style: const TextStyle(fontSize: 15.0),
                        )),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

///for providing the name of the guest
// class ProvideNameRequest extends StatelessWidget {
//   const ProvideNameRequest({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     final _formKey = GlobalKey<FormState>();
//     TextEditingController nameController = TextEditingController();
//
//     return Scaffold(
//       appBar: AppBar(
//           iconTheme: const IconThemeData(color: Colors.black),
//           leading: IconButton(
//             onPressed: () => Navigator.popUntil(
//                 context, (Route<dynamic> route) => route.isFirst),
//             icon: const Icon(
//               Icons.keyboard_arrow_left_outlined,
//               color: Colors.black,
//             ),
//           )),
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           const Text(
//             'Please provide us your name.',
//             style: TextStyle(fontSize: 30.0),
//             textAlign: TextAlign.center,
//           ),
//           const Spacer(),
//           Padding(
//             padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 10.0),
//             child: Form(
//               key: _formKey,
//               child: TextFormField(
//                 controller: nameController,
//                 decoration: InputDecoration(
//                   labelText: LocaleKeys.full_name.tr(),
//                   labelStyle: TextStyle(color: Colors.black, fontSize: 10.0.sp),
//                   filled: true,
//                   fillColor: Colors.white,
//                 ),
//                 validator: (value) {
//                   if (value == null || value.isEmpty) {
//                     return 'Please enter your name';
//                   }
//                 },
//               ),
//             ),
//           ),
//           const Spacer(),
//           Padding(
//             padding: const EdgeInsets.all(30.0),
//             child: ElevatedButton(
//                 style: ButtonStyle(
//                     backgroundColor: MaterialStateProperty.all(Colors.green),
//                     fixedSize: MaterialStateProperty.all(Size(80.0.w, 70.0))),
//                 onPressed: (){
//                   if (_formKey.currentState!.validate()){
//                     pushNewScreen(context, screen: ProvidePhoneNumberRequest(name: nameController.text));
//                   }
//                 },
//                 child: const Text(
//                   'Next',
//                   style: TextStyle(fontSize: 20.0),
//                 )),
//           ),
//         ],
//       ),
//     );
//   }
// }

class ProvidePhoneNumberRequest extends StatefulWidget {
  const ProvidePhoneNumberRequest({Key? key})
      : super(key: key);

  @override
  State<ProvidePhoneNumberRequest> createState() =>
      _ProvidePhoneNumberRequestState();
}

class _ProvidePhoneNumberRequestState extends State<ProvidePhoneNumberRequest> {
  String countryPhoneCode = '';
  TextEditingController phoneNumberController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  late Position location;
  late Placemark place;

  Future<CameraPosition> getLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    location = await Geolocator.getCurrentPosition();
    List<Placemark> p =
        await placemarkFromCoordinates(location.latitude, location.longitude);
    place = p[0];

    return CameraPosition(
      target: LatLng(location.latitude, location.longitude),
      zoom: 16,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: const IconThemeData(color: Colors.black),
          leading: IconButton(
            onPressed: () => Navigator.popUntil(
                context, (Route<dynamic> route) => route.isFirst),
            icon: const Icon(
              Icons.keyboard_arrow_left_outlined,
              color: Colors.black,
            ),
          )),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              LocaleKeys.please_enter_your_phone_number.tr(),
              style: GoogleFonts.lato(
                fontSize: 25.0,
                fontWeight: FontWeight.w600,
                color: const Color(0xFF125C13),
              ),
              textAlign: TextAlign.center,
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
            child: Directionality(
              textDirection: ltrTextDirection,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 24.0),
                    child: Container(
                      height: 60.0,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0)),
                          border: Border.all(color: Colors.grey)),
                      child: CountryCodePicker(
                        onChanged: (CountryCode value) {
                          setState(() {
                            countryPhoneCode = value.dialCode.toString();
                          });
                        },
                        initialSelection: 'QA',
                        showCountryOnly: false,
                        showOnlyCountryWhenClosed: false,
                        alignLeft: false,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Form(
                      key: _formKey,
                      child: TextFormField(
                        controller: phoneNumberController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: LocaleKeys.phone_number.tr(),
                            labelStyle:
                                TextStyle(color: Colors.black, fontSize: 10.0.sp),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: const OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            border: const OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            )),
                        maxLength: 8,
                        validator: (value) {
                          if(value != null && value.length != 8){
                            return LocaleKeys.enter_8_digits.tr();
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const Spacer(),
          FutureBuilder(
              future: getLocation(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  return BlocBuilder<AuthBloc, AuthState>(
                      builder: (context, state) {
                    if (state is AuthLoadingState) {
                      return const Padding(
                        padding: EdgeInsets.all(30.0),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.green),
                                fixedSize: MaterialStateProperty.all(
                                    Size(80.0.w, 70.0))),
                            onPressed: () {
                              if (_formKey.currentState!.validate()){
                                AddressModel currentAddress = AddressModel(
                                    name: place.name,
                                    street: place.street,
                                    isoCountryCode: place.isoCountryCode,
                                    country: place.country,
                                    administrativeArea: place.administrativeArea,
                                    locality: place.locality,
                                    latitude: location.latitude.toString(),
                                    longitude: location.longitude.toString());

                                RegisterAsGuestAuthParams registerAsGuestParams =
                                RegisterAsGuestAuthParams(
                                    'Guest',
                                    currentAddress,
                                    countryPhoneCode != ''
                                        ? countryPhoneCode +
                                        phoneNumberController.text
                                        : phoneNumberController.text != ''
                                        ? '+974' +
                                        phoneNumberController.text
                                        : null);

                                final registerAsGuestBloc =
                                BlocProvider.of<AuthBloc>(context);
                                registerAsGuestBloc.add(
                                    EnterAsGuestEvent(registerAsGuestParams));

                                Navigator.popUntil(context,
                                        (Route<dynamic> route) => route.isFirst);
                              }
                            },
                            child: Text(
                              LocaleKeys.go_to_dashboard.tr(),
                              style: const TextStyle(fontSize: 15.0),
                            )),
                      );
                    }
                  });
                } else {
                  return const Padding(
                    padding: EdgeInsets.all(30.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
              }),
        ],
      ),
    );
  }
}

class LoginUsingPhoneNumber extends StatefulWidget {
  const LoginUsingPhoneNumber({Key? key}) : super(key: key);

  @override
  State<LoginUsingPhoneNumber> createState() => _LoginUsingPhoneNumberState();
}

class _LoginUsingPhoneNumberState extends State<LoginUsingPhoneNumber> {
  String countryPhoneCode = '';
  TextEditingController phoneNumberController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              LocaleKeys.enter_your_phone_number.tr(),
              style: GoogleFonts.lato(
                fontSize: 25.0,
                fontWeight: FontWeight.w600,
                color: const Color(0xFF125C13),
              ),
              textAlign: TextAlign.center,
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
            child: Directionality(
              textDirection: ltrTextDirection,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 24.0),
                    child: Container(
                      height: 60.0,
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0)),
                          border: Border.all(color: Colors.grey)),
                      child: CountryCodePicker(
                        onChanged: (CountryCode value) {
                          setState(() {
                            countryPhoneCode = value.dialCode.toString();
                          });
                        },
                        initialSelection: 'QA',
                        showCountryOnly: false,
                        showOnlyCountryWhenClosed: false,
                        alignLeft: false,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Form(
                      key: _formKey,
                      child: TextFormField(
                        controller: phoneNumberController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: LocaleKeys.phone_number.tr(),
                            labelStyle:
                                TextStyle(color: Colors.black, fontSize: 10.0.sp),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: const OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10.0),
                                  bottomRight: Radius.circular(10.0)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            )),
                        maxLength: 8,
                        validator: (value) {
                          if (value == null || value.length != 8) {
                            return LocaleKeys.enter_8_digits.tr();
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.green),
                    fixedSize: MaterialStateProperty.all(Size(80.0.w, 70.0))),
                onPressed: () {
                  if (_formKey.currentState!.validate()){
                    String phoneNumber = countryPhoneCode != '' ? countryPhoneCode + phoneNumberController.text : '+974' + phoneNumberController.text;

                    final loginPhoneNumberBloc = BlocProvider.of<AuthBloc>(context);
                    loginPhoneNumberBloc.add(LoginPhoneNumberEvent(phoneNumber));

                    Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
                  }
                },
                child: Text(
                  LocaleKeys.go_to_dashboard.tr(),
                  style: const TextStyle(fontSize: 15.0),
                )),
          )
        ],
      ),
    );
  }
}
