import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/login/body_login.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: BodyLogin(),
    );
  }
}
