import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/service/firebase_service.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/login/guest/enter_as_guest.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/registration/registration_modal_sheet.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class BodyLogin extends StatefulWidget {
  const BodyLogin({Key? key}) : super(key: key);

  @override
  _BodyLoginState createState() => _BodyLoginState();
}

class _BodyLoginState extends State<BodyLogin> {
  bool hidePassword = true;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 40.0.h,
            decoration: const BoxDecoration(
                color: Colors.green,
                image: DecorationImage(
                    image: AssetImage('assets/img/front_3.png'),
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50.0),
                    bottomRight: Radius.circular(50.0))),
          ),
          SizedBox(
            height: 70.0.h,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      LocaleKeys.kchra.tr(),
                      style: GoogleFonts.lato(
                          fontWeight: FontWeight.w900,
                          color: const Color(0xFF125C13),
                          fontSize: 20.0.sp),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    LocaleKeys.junk_removal_app.tr(),
                    style: GoogleFonts.lato(
                        fontWeight: FontWeight.w900,
                        color: const Color(0xFF125C13),
                        fontSize: 10.0.sp),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 5.0),
                  ElevatedButton(
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white)),
                      onPressed: () async {
                        await FirebaseService().signOutFromGoogle();
                        String? email = await FirebaseService().signInWithGoogle();

                        final authBloc = BlocProvider.of<AuthBloc>(context);
                        authBloc.add(LoginGoogleAuthEvent(email, context));
                      },
                      child: SizedBox(
                        width: 65.0.w,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset('assets/img/google.png', scale: 18.0),
                            const SizedBox(width: 10.0),
                            Text(LocaleKeys.sign_in_with_google.tr(), style: const TextStyle(color: Colors.black),)
                          ],
                        ),
                      )
                  ),
                  const SizedBox(height: 5.0),
                  // const Padding(
                  //   padding: EdgeInsets.all(10.0),
                  //   child: Text('Or'),
                  // ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 10.0),
                    child: TextFormField(
                      controller: emailController,
                      decoration: InputDecoration(
                          labelText: LocaleKeys.email.tr(),
                          labelStyle: GoogleFonts.poppins(
                              color: Colors.black, fontSize: 10.0.sp),
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: const BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: const BorderSide(
                              color: Colors.grey,
                            ),
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 10.0),
                    child: TextFormField(
                      controller: passwordController,
                      decoration: InputDecoration(
                        labelText: LocaleKeys.password.tr(),
                        labelStyle: GoogleFonts.poppins(
                            color: Colors.black, fontSize: 10.0.sp),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                        suffixIcon: IconButton(
                            onPressed: hidePass,
                            icon: const Icon(Icons.remove_red_eye))
                      ),
                      obscureText: hidePassword,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50.0, 10.0, 50.0, 0),
                    child: SizedBox(
                      width: 100.0.w,
                      child: BlocBuilder<AuthBloc, AuthState>(
                        builder: (context, state) {
                          if(state is AuthLoadingState){
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                    MaterialStateProperty.all(Colors.green[800])),
                                onPressed: () {
                                  AuthParams authParams = AuthParams(
                                      email: emailController.text,
                                      password: passwordController.text);

                                  final authBloc = BlocProvider.of<AuthBloc>(context);
                                  authBloc.add(LoginAuthEvent(authParams, context));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Text(LocaleKeys.log_in.tr()),
                                ));
                          }
                        }
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50.0, 10.0, 50.0, 0),
                    child: SizedBox(
                      width: 100.0.w,
                      child: OutlinedButton(
                          onPressed: () {
                            pushNewScreen(context, screen: const EnterAsNewGuestRequest(), withNavBar: false);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Text(LocaleKeys.enter_as_guest.tr(), style: const TextStyle(fontSize: 17.0),),
                          )),
                    ),
                  ),
                  Directionality(
                    textDirection: ltrTextDirection,
                    child: Column(
                      children: [
                        const SizedBox(height: 10.0),
                        GestureDetector(
                          onTap: () async {
                            showCupertinoModalBottomSheet(
                                context: context,
                                backgroundColor: Colors.transparent,
                                builder: (context) =>
                                const RegistrationModalSheet()
                            );
                          },
                          child: RichText(
                          text: TextSpan(
                            children: [
                             TextSpan(
                                text: 'Don\'t have an account? ',
                                style: GoogleFonts.poppins(fontSize: 13.0, color: Colors.black)
                              ),
                              TextSpan(
                                  text: LocaleKeys.register_here.tr(),
                                  style: GoogleFonts.poppins(fontSize: 13.0, color: Colors.blue)
                              )
                            ]
                          )),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Choose Language:', style: GoogleFonts.poppins(fontSize: 13.0, color: Colors.black)),
                            TextButton(
                              onPressed: () async {
                                await MyApp.navigatorKey.currentContext!.setLocale(const Locale('en'));
                                Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);

                                MyApp.restartApp(context);
                              },
                              child: Text('English', style: GoogleFonts.poppins(fontSize: 13.0)),
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero
                              ),
                            ),
                            TextButton(
                              onPressed: () async {
                                await MyApp.navigatorKey.currentContext!.setLocale(const Locale('ar'));
                                Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);

                                MyApp.restartApp(context);
                              },
                              child: Text('عربي', style: GoogleFonts.poppins(fontSize: 13.0)),
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  hidePass() {
    setState(() {
      if (hidePassword) {
        hidePassword = false;
      } else {
        hidePassword = true;
      }
    });
  }
}
