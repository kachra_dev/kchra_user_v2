import 'dart:async';
import 'dart:ui';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kachra_user_app/src/config/themes/app_theme.dart';
import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/core/utils/service/firebase_service.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class BodyRegistrationGoogle extends StatefulWidget {
  const BodyRegistrationGoogle({Key? key}) : super(key: key);

  @override
  _BodyRegistrationGoogleState createState() => _BodyRegistrationGoogleState();
}

class _BodyRegistrationGoogleState extends State<BodyRegistrationGoogle> {
  final _formKey = GlobalKey<FormState>();

  bool hidePassword = true;
  bool hidePasswordConfirm = true;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmationController = TextEditingController();
  String country = '';
  String countryPhoneCode = '';
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  late Position location;
  late Placemark place;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.light.accentColor,
      appBar: AppBar(
        backgroundColor: AppTheme.light.accentColor,
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
      ),
      body: Stack(
        children: [
          Container(
            height: 100.0.h,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0)),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 15.0, 0, 0),
                        child: Directionality(
                          textDirection: ltrTextDirection,
                          child: RichText(
                              textAlign: TextAlign.left,
                              text: TextSpan(children: [
                                TextSpan(
                                    text: LocaleKeys.register_to.tr(),
                                    style: GoogleFonts.poppins(
                                        fontSize: 15.0.sp,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black)),
                              ])),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 0.0),
                        child: ElevatedButton(
                            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white)),
                            onPressed: () async {
                              await FirebaseService().signOutFromGoogle();
                              User? user = await FirebaseService().signInWithGoogleReturnUser();

                              if(user != null){
                                nameController.text = user.displayName!;
                                emailController.text = user.email!;
                              }
                            },
                            child: SizedBox(
                              width: 65.0.w,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/img/google.png', scale: 18.0),
                                  const SizedBox(width: 10.0),
                                  Text(LocaleKeys.click_to_sign_in_with_google.tr(), style: const TextStyle(color: Colors.black, fontSize: 12.0),)
                                ],
                              ),
                            )
                        ),
                      ),
                      const Divider(),
                      const SizedBox(height: 10.0),
                      Padding(
                        padding:
                        const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 20.0),
                        child: TextFormField(
                          controller: nameController,
                          readOnly: true,
                          decoration: InputDecoration(
                            labelText: LocaleKeys.full_name.tr(),
                            labelStyle: GoogleFonts.poppins(
                                color: Colors.black, fontSize: 10.0.sp),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                        child: TextFormField(
                          controller: emailController,
                          readOnly: true,
                          decoration: InputDecoration(
                              labelText: LocaleKeys.email.tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: Colors.black, fontSize: 10.0.sp),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                      Text(LocaleKeys.please_fill_up_the_address_and_phone_number_below.tr(), style: const TextStyle(fontSize: 10.0)),
                      const SizedBox(height: 10.0),
                      GestureDetector(
                        onTap: () async {
                          bool serviceEnabled;
                          LocationPermission permission;

                          serviceEnabled = await Geolocator.isLocationServiceEnabled();
                          if (!serviceEnabled) {
                            return Future.error('Location services are disabled.');
                          }

                          permission = await Geolocator.checkPermission();
                          if (permission == LocationPermission.denied) {
                            permission = await Geolocator.requestPermission();
                            if (permission == LocationPermission.denied) {
                              return Future.error('Location permissions are denied');
                            }
                          }

                          if (permission == LocationPermission.deniedForever) {
                            // Permissions are denied forever, handle appropriately.
                            return Future.error(
                                'Location permissions are permanently denied, we cannot request permissions.');
                          }

                          location = await Geolocator.getCurrentPosition();
                          List<Placemark> p = await placemarkFromCoordinates(
                              location.latitude, location.longitude);
                          place = p[0];

                          addressController.text = place.street.toString() + ', ' + place.administrativeArea.toString();
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                          child: TextFormField(
                            enabled: false,
                            controller: addressController,
                            decoration: InputDecoration(
                                suffixIcon: const Icon(CupertinoIcons.location),
                                labelText: LocaleKeys.address.tr(),
                                labelStyle: GoogleFonts.poppins(
                                    color: Colors.black, fontSize: 10.0.sp),
                                filled: true,
                                fillColor: Colors.white,
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: const BorderSide(
                                    color: Colors.grey,
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: const BorderSide(
                                    color: Colors.grey,
                                  ),
                                )),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return LocaleKeys.please_enter_some_text.tr();
                              }
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                        child: Directionality(
                          textDirection: ltrTextDirection,
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 24.0),
                                child: Container(
                                  height: 60.0,
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(10.0),
                                          bottomLeft: Radius.circular(10.0)),
                                      border: Border.all(color: Colors.grey)),
                                  child: CountryCodePicker(
                                    onChanged: (CountryCode value) {
                                      setState(() {
                                        country = value.name.toString();
                                        countryPhoneCode =
                                            value.dialCode.toString();

                                        if (value.name.toString() == 'قطر') {
                                          country = 'Qatar';
                                        }
                                      });
                                    },
                                    initialSelection: 'QA',
                                    showCountryOnly: false,
                                    showOnlyCountryWhenClosed: false,
                                    alignLeft: false,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: phoneNumberController,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                      labelText: LocaleKeys.phone_number.tr(),
                                      labelStyle: GoogleFonts.poppins(
                                          color: Colors.black, fontSize: 10.0.sp),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: const OutlineInputBorder(
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(10.0),
                                            bottomRight: Radius.circular(10.0)),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                        ),
                                      ),
                                      border: const OutlineInputBorder(
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(10.0),
                                            bottomRight: Radius.circular(10.0)),
                                        borderSide: BorderSide(
                                          color: Colors.grey,
                                        ),
                                      )),
                                  maxLength: 8,
                                  validator: (value) {
                                    if (value == null || value.length != 8) {
                                      return LocaleKeys.enter_8_digits.tr();
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection: ltrTextDirection,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text('By registering, you agree to our ', style: TextStyle(fontSize: 10.0)),
                            TextButton(
                              onPressed: () async {
                                String url = 'https://kchraapp.com/#/terms-and-conditions';
                                if (!await launch(url)) throw 'Could not launch $url';
                              },
                              child: const Text('Terms and Conditions', style: TextStyle(fontSize: 10.0)),
                              style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.fromLTRB(30.0, 0, 30.0, 10.0),
                        child: SizedBox(
                          width: 100.0.w,
                          child: BlocBuilder<AuthBloc, AuthState>(
                              builder: (context, state) {
                                if (state is AuthLoadingState) {
                                  return const Center(
                                    child: CircularProgressIndicator(),
                                  );
                                } else {
                                  return ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                          MaterialStateProperty.all(
                                              Colors.green[800])),
                                      onPressed: () {
                                        if (_formKey.currentState!.validate()) {
                                          AddressModel address = AddressModel(
                                              name: place.name,
                                              country: place.country,
                                              administrativeArea: place.administrativeArea,
                                              isoCountryCode: place.isoCountryCode,
                                              street: place.street,
                                              locality: place.locality,
                                              latitude: location.latitude.toString(),
                                              longitude: location.longitude.toString()
                                          );

                                          RegisterUsingGoogleAuthParams registerUsingGoogleAuthParams =
                                          RegisterUsingGoogleAuthParams(
                                              nameController.text,
                                              emailController.text,
                                              address,
                                              countryPhoneCode != '' ? countryPhoneCode + phoneNumberController.text : '+974' + phoneNumberController.text,
                                              country == '' ? 'Qatar' : country,
                                              'Client'
                                          );

                                          final authBloc = BlocProvider.of<AuthBloc>(context);
                                          authBloc.add(RegisterUsingGoogleAuthEvent(registerUsingGoogleAuthParams, context));

                                        }
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Text(LocaleKeys.create_an_account.tr()),
                                      ));
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      )
    );
  }

  hidePass() {
    setState(() {
      if (hidePassword) {
        hidePassword = false;
      } else {
        hidePassword = true;
      }
    });
  }

  hidePassConfirm() {
    setState(() {
      if (hidePasswordConfirm) {
        hidePasswordConfirm = false;
      } else {
        hidePasswordConfirm = true;
      }
    });
  }
}
