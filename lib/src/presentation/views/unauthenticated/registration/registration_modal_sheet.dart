import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/registration/body_registration_google.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/registration/registration.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

class RegistrationModalSheet extends StatefulWidget {
  const RegistrationModalSheet({Key? key}) : super(key: key);

  @override
  _RegistrationModalSheetState createState() => _RegistrationModalSheetState();
}

class _RegistrationModalSheetState extends State<RegistrationModalSheet> {
  @override
  Widget build(BuildContext context) {
    return Material(
        child: SafeArea(
          top: false,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                title: Center(child: Text(LocaleKeys.register.tr())),
              ),
              ListTile(
                title: Text(LocaleKeys.register_with_google.tr()),
                leading: SizedBox(height: 25.0,child: Image.asset('assets/img/google.png', scale: 8.0,)),
                onTap: () async {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const BodyRegistrationGoogle()));
                },
              ),
              ListTile(
                title: Text(LocaleKeys.register_using_email_password.tr()),
                leading: const Icon(Icons.note_alt_sharp),
                onTap: () async {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Registration()));
                },
              ),
            ],
          ),
        ));
  }
}
