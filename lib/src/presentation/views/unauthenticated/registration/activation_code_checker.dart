import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/auth_checker.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class ActivationCodeChecker extends StatefulWidget {
  final AuthParams authParams;

  const ActivationCodeChecker({Key? key, required this.authParams})
      : super(key: key);

  @override
  _ActivationCodeCheckerState createState() => _ActivationCodeCheckerState();
}

class _ActivationCodeCheckerState extends State<ActivationCodeChecker> {
  TextEditingController text1 = TextEditingController();
  TextEditingController text2 = TextEditingController();
  TextEditingController text3 = TextEditingController();
  TextEditingController text4 = TextEditingController();
  TextEditingController text5 = TextEditingController();
  TextEditingController text6 = TextEditingController();

  FocusNode pin1FocusNode = FocusNode();
  FocusNode pin2FocusNode = FocusNode();
  FocusNode pin3FocusNode = FocusNode();
  FocusNode pin4FocusNode = FocusNode();
  FocusNode pin5FocusNode = FocusNode();
  FocusNode pin6FocusNode = FocusNode();

  final kTextColor = const Color(0xFF757575);
  final boxWidth = 17.0.w;

  @override
  void initState() {
    super.initState();
    pin1FocusNode = FocusNode();
    pin2FocusNode = FocusNode();
    pin3FocusNode = FocusNode();
    pin4FocusNode = FocusNode();
    pin5FocusNode = FocusNode();
    pin6FocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    pin1FocusNode.dispose();
    pin2FocusNode.dispose();
    pin3FocusNode.dispose();
    pin4FocusNode.dispose();
    pin5FocusNode.dispose();
    pin6FocusNode.dispose();
  }

  void nextField(String value, FocusNode? focusNode) {
    if (value.length == 1) {
      focusNode!.requestFocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder outlineInputBorder() {
      return OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
        borderSide: BorderSide(color: kTextColor),
      );
    }

    final otpInputDecoration = InputDecoration(
      contentPadding:
      const EdgeInsets.symmetric(vertical: 20.0),
      border: outlineInputBorder(),
      focusedBorder: outlineInputBorder(),
      enabledBorder: outlineInputBorder(),
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('Activate your Account',
            style: TextStyle(color: Colors.black)),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 10,
              child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text('Activation Code',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'We\'ve sent you a 4-digit code to ${widget.authParams.email}',
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Form(
                    child: Column(
                      children: [
                        const SizedBox(height: 50.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: boxWidth,
                              child: TextFormField(
                                controller: text1,
                                autofocus: true,
                                style: const TextStyle(fontSize: 24),
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                decoration: otpInputDecoration,
                                onChanged: (value) {
                                  nextField(value, pin2FocusNode);
                                },
                              ),
                            ),
                            SizedBox(
                              width: boxWidth,
                              child: TextFormField(
                                controller: text2,
                                focusNode: pin2FocusNode,
                                style: const TextStyle(fontSize: 24),
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                decoration: otpInputDecoration,
                                onChanged: (value) {
                                  if (value.isEmpty) {
                                    FocusScope.of(context).requestFocus(pin1FocusNode);
                                  } else if(value.length == 1){
                                    nextField(value, pin3FocusNode);
                                  }
                                }
                              ),
                            ),
                            SizedBox(
                              width: boxWidth,
                              child: TextFormField(
                                controller: text3,
                                focusNode: pin3FocusNode,
                                style: const TextStyle(fontSize: 24),
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                decoration: otpInputDecoration,
                                onChanged: (value) {
                                  if (value.isEmpty) {
                                    FocusScope.of(context).requestFocus(pin2FocusNode);
                                  } else {
                                    nextField(value, pin4FocusNode);
                                  }
                                }
                              ),
                            ),
                            SizedBox(
                              width: boxWidth,
                              child: TextFormField(
                                controller: text4,
                                focusNode: pin4FocusNode,
                                style: const TextStyle(fontSize: 24),
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                decoration: otpInputDecoration,
                                onChanged: (value) {
                                  if (value.isEmpty) {
                                    FocusScope.of(context).requestFocus(pin3FocusNode);
                                  } else {
                                    nextField(value, pin5FocusNode);
                                  }
                                }
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )),
            Expanded(
              flex: 2,
              child: GestureDetector(
                onTap: (){
                  String activationCode = text1.text + text2.text + text3.text + text4.text + text5.text + text6.text;

                  AuthParams authParams = AuthParams(
                      email: widget.authParams.email,
                      password: widget.authParams.password,
                    activationCode: activationCode
                  );

                  final authBloc = BlocProvider.of<AuthBloc>(context);

                  if(authBloc.state == const UnauthenticatedState()){
                    authBloc.add(LoginAuthEvent(authParams, context));
                  } else {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker()));
                  }
                },
                child: Container(
                  width: 100.0.w,
                  color: Colors.green,
                  child: BlocBuilder<AuthBloc, AuthState>(
                    builder: (context, state) {
                      if(state is AuthLoadingState){
                        return const Center(
                          child: CircularProgressIndicator(color: Colors.white),
                        );
                      } else if (state is UnauthenticatedState){
                        return const Center(
                            child: Text('Activate account', style: TextStyle(color: Colors.white, fontSize: 15.0),)
                        );
                      } else {
                        return Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Directionality(
                                textDirection: ltrTextDirection,
                                child: Text('${LocaleKeys.success.tr()} ${LocaleKeys.go_to_dashboard.tr()}', style: const TextStyle(color: Colors.white, fontSize: 15.0))
                              ),
                              const SizedBox(width: 5.0),
                              IconButton(
                                onPressed: (){
                                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const AuthChecker()));
                                },
                                icon: const Icon(Icons.chevron_right, color: Colors.white,),
                              )
                            ],
                          ),
                        );
                      }
                    }
                  ),
                ),
              )
            )
          ],
        ),
      ),
    );
  }
}
