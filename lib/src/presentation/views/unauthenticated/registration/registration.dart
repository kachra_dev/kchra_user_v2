import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/config/themes/app_theme.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/registration/body_registration.dart';

class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.light.accentColor,
      appBar: AppBar(
        backgroundColor: AppTheme.light.accentColor,
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
      ),
      body: const BodyRegistration(),
    );
  }
}
