import 'dart:async';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/messages/error_message.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class BodyRegistration extends StatefulWidget {
  const BodyRegistration({Key? key}) : super(key: key);

  @override
  _BodyRegistrationState createState() => _BodyRegistrationState();
}

class _BodyRegistrationState extends State<BodyRegistration> {
  final _formKey = GlobalKey<FormState>();

  bool hidePassword = true;
  bool hidePasswordConfirm = true;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmationController = TextEditingController();
  String country = '';
  String countryPhoneCode = '';
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  late Position location;
  late Placemark place;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 100.0.h,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0)),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15.0, 0, 0),
                      child: Directionality(
                        textDirection: ltrTextDirection,
                        child: RichText(
                            textAlign: TextAlign.left,
                            text: TextSpan(children: [
                              TextSpan(
                                  text: LocaleKeys.register_to.tr(),
                                  style: GoogleFonts.poppins(
                                      fontSize: 15.0.sp,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black)),
                            ])),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 20.0),
                      child: TextFormField(
                        controller: nameController,
                        decoration: InputDecoration(
                          labelText: LocaleKeys.full_name.tr(),
                          labelStyle: GoogleFonts.poppins(
                              color: Colors.black, fontSize: 10.0.sp),
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: const BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: const BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return LocaleKeys.please_enter_some_text.tr();
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                      child: TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                            labelText: LocaleKeys.email.tr(),
                            labelStyle: GoogleFonts.poppins(
                                color: Colors.black, fontSize: 10.0.sp),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            )),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return LocaleKeys.please_enter_some_text.tr();
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                      child: TextFormField(
                        controller: passwordController,
                        decoration: InputDecoration(
                            labelText: LocaleKeys.password.tr(),
                            labelStyle: GoogleFonts.poppins(
                                color: Colors.black, fontSize: 10.0.sp),
                            filled: true,
                            fillColor: Colors.white,
                            suffixIcon: IconButton(
                                onPressed: hidePass,
                                icon: const Icon(Icons.remove_red_eye)),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            )),
                        obscureText: hidePassword,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return LocaleKeys.please_enter_some_text.tr();
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                      child: TextFormField(
                        controller: passwordConfirmationController,
                        decoration: InputDecoration(
                            labelText: LocaleKeys.confirm_password.tr(),
                            labelStyle: GoogleFonts.poppins(
                                color: Colors.black, fontSize: 10.0.sp),
                            filled: true,
                            fillColor: Colors.white,
                            suffixIcon: IconButton(
                                onPressed: hidePassConfirm,
                                icon: const Icon(Icons.remove_red_eye)),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                              ),
                            )),
                        obscureText: hidePasswordConfirm,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return LocaleKeys.please_enter_some_text.tr();
                          }
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        bool serviceEnabled;
                        LocationPermission permission;

                        serviceEnabled = await Geolocator.isLocationServiceEnabled();
                        if (!serviceEnabled) {
                          return Future.error('Location services are disabled.');
                        }

                        permission = await Geolocator.checkPermission();
                        if (permission == LocationPermission.denied) {
                          permission = await Geolocator.requestPermission();
                          if (permission == LocationPermission.denied) {
                            return Future.error('Location permissions are denied');
                          }
                        }

                        if (permission == LocationPermission.deniedForever) {
                          // Permissions are denied forever, handle appropriately.
                          return Future.error(
                              'Location permissions are permanently denied, we cannot request permissions.');
                        }

                        location = await Geolocator.getCurrentPosition();
                        List<Placemark> p = await placemarkFromCoordinates(
                            location.latitude, location.longitude);
                        place = p[0];

                        addressController.text = place.street.toString() + ', ' + place.administrativeArea.toString();
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                        child: TextFormField(
                          enabled: false,
                          controller: addressController,
                          decoration: InputDecoration(
                              suffixIcon: const Icon(CupertinoIcons.location),
                              labelText: LocaleKeys.address.tr(),
                              labelStyle: GoogleFonts.poppins(
                                  color: Colors.black, fontSize: 10.0.sp),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                ),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return LocaleKeys.please_enter_some_text.tr();
                            }
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                      child: Directionality(
                        textDirection: ltrTextDirection,
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 24.0),
                              child: Container(
                                height: 60.0,
                                decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(10.0),
                                        bottomLeft: Radius.circular(10.0)),
                                    border: Border.all(color: Colors.grey)),
                                child: CountryCodePicker(
                                  onChanged: (CountryCode value) {
                                    setState(() {
                                      country = value.name.toString();
                                      countryPhoneCode =
                                          value.dialCode.toString();

                                      if (value.name.toString() == 'قطر') {
                                        country = 'Qatar';
                                      }
                                    });
                                  },
                                  initialSelection: 'QA',
                                  showCountryOnly: false,
                                  showOnlyCountryWhenClosed: false,
                                  alignLeft: false,
                                ),
                              ),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: phoneNumberController,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    labelText: LocaleKeys.phone_number.tr(),
                                    labelStyle: GoogleFonts.poppins(
                                        color: Colors.black, fontSize: 10.0.sp),
                                    filled: true,
                                    fillColor: Colors.white,
                                    focusedBorder: const OutlineInputBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10.0),
                                          bottomRight: Radius.circular(10.0)),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    ),
                                    enabledBorder: const OutlineInputBorder(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10.0),
                                          bottomRight: Radius.circular(10.0)),
                                      borderSide: BorderSide(
                                        color: Colors.grey,
                                      ),
                                    )),
                                maxLength: 8,
                                validator: (value) {
                                  if (value == null || value.length != 8) {
                                    return LocaleKeys.enter_8_digits.tr();
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Directionality(
                      textDirection: ltrTextDirection,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text('By registering, you agree to our ', style: TextStyle(fontSize: 10.0)),
                          TextButton(
                              onPressed: () async {
                                String url = 'https://kchraapp.com/#/terms-and-conditions';
                                if (!await launch(url)) throw 'Could not launch $url';
                              },
                              child: const Text('Terms and Conditions', style: TextStyle(fontSize: 10.0)),
                            style: TextButton.styleFrom(
                              padding: EdgeInsets.zero
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.fromLTRB(30.0, 0, 30.0, 10.0),
                      child: SizedBox(
                        width: 100.0.w,
                        child: BlocBuilder<AuthBloc, AuthState>(
                            builder: (context, state) {
                          if (state is AuthLoadingState) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          } else {
                            return ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all(
                                            Colors.green[800])),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    AddressModel address = AddressModel(
                                      name: place.name,
                                      country: place.country,
                                      administrativeArea: place.administrativeArea,
                                      isoCountryCode: place.isoCountryCode,
                                      street: place.street,
                                      locality: place.locality,
                                      latitude: location.latitude.toString(),
                                      longitude: location.longitude.toString()
                                    );

                                    RegisterAuthParams registerAuthParams =
                                        RegisterAuthParams(
                                            nameController.text,
                                            emailController.text,
                                            passwordController.text,
                                            passwordConfirmationController.text,
                                            address,
                                            countryPhoneCode != '' ? countryPhoneCode + phoneNumberController.text : '+974' + phoneNumberController.text,
                                            country == '' ? 'Qatar' : country,
                                            'Client'
                                        );

                                    final authBloc = BlocProvider.of<AuthBloc>(context);
                                    authBloc.add(RegisterAuthEvent(registerAuthParams, context));

                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Text(LocaleKeys.create_an_account.tr()),
                                ));
                          }
                        }),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  hidePass() {
    setState(() {
      if (hidePassword) {
        hidePassword = false;
      } else {
        hidePassword = true;
      }
    });
  }

  hidePassConfirm() {
    setState(() {
      if (hidePasswordConfirm) {
        hidePasswordConfirm = false;
      } else {
        hidePasswordConfirm = true;
      }
    });
  }
}
