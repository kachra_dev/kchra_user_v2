import 'package:flutter/material.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';
import 'package:kachra_user_app/src/presentation/widgets/show_location_map_widget.dart';

class ShowAddressDetails extends StatefulWidget {
  final UserSavedAddressHive savedAddress;
  final double? height;
  final double? width;

  const ShowAddressDetails({Key? key, required this.savedAddress, this.height, this.width}) : super(key: key);

  @override
  _ShowAddressDetailsState createState() => _ShowAddressDetailsState();
}

class _ShowAddressDetailsState extends State<ShowAddressDetails> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ShowLocationMapWidget(
              latitude: double.parse(widget.savedAddress.latitude!),
              longitude: double.parse(widget.savedAddress.longitude!),
              name: widget.savedAddress.name!,
              height: widget.height,
              width: widget.width,
          ),
          const SizedBox(height: 10.0),
          Text(
              widget.savedAddress.addressName!
          ),
          const SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Name:'),
              const SizedBox(width: 10.0),
              Flexible(child: Text(widget.savedAddress.name!))
            ],
          ),
          const SizedBox(height: 5.0),
          widget.savedAddress.street != null ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Street:'),
              const SizedBox(width: 10.0),
              Flexible(child: Text(widget.savedAddress.street!))
            ],
          ) : const SizedBox(height: 0),
          const SizedBox(height: 5.0),
          widget.savedAddress.administrativeArea != null ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Area:'),
              const SizedBox(width: 10.0),
              Flexible(child: Text(widget.savedAddress.administrativeArea!))
            ],
          ) : const SizedBox(height: 0),
          const SizedBox(height: 5.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Country:'),
              const SizedBox(width: 10.0),
              Flexible(child: Text(widget.savedAddress.country!))
            ],
          ),
          const SizedBox(height: 10.0),
        ],
      ),
    );
  }
}
