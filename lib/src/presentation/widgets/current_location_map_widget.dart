import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class CurrentLocationMapWidget extends StatefulWidget {
  final bool isHideAddress;
  const CurrentLocationMapWidget({Key? key, required this.isHideAddress}) : super(key: key);

  @override
  _CurrentLocationMapWidgetState createState() => _CurrentLocationMapWidgetState();
}

class _CurrentLocationMapWidgetState extends State<CurrentLocationMapWidget> {
  late Position location;
  late Placemark place;
  late String address;

  final Completer<GoogleMapController> _controller = Completer();

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};


  Future<CameraPosition> getLocation() async{
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    location = await Geolocator.getCurrentPosition();
    List<Placemark> p = await placemarkFromCoordinates(location.latitude, location.longitude);
    place = p[0];

    address = place.name! + ", " + place.locality! + ", " + place.country!;

    CameraPosition initLocation = CameraPosition(
      target: LatLng(location.latitude, location.longitude),
      zoom: 16,
    );

    final marker = Marker(
      markerId: MarkerId(place.name.toString()),
      position: LatLng(location.latitude, location.longitude),
    );

    markers[MarkerId(place.name.toString())] = marker;

    return initLocation;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getLocation(),
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
            CameraPosition initialLocation = snapshot.data as CameraPosition;

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 30.0.h,
                  width: 95.0.w,
                  child: GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition: initialLocation,
                    onMapCreated: (GoogleMapController controller) {
                      if (!_controller.isCompleted) {
                        _controller.complete(controller);
                      }else{
                      }
                    },
                    markers: markers.values.toSet(),
                  ),
                ),
                widget.isHideAddress ? const SizedBox(height: 0) : Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(LocaleKeys.booking_address.tr(), style: const TextStyle(fontWeight: FontWeight.w600)),
                      const SizedBox(height: 10.0),
                      Text(address),
                    ],
                  ),
                ),
              ],
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        }
    );
  }
}
