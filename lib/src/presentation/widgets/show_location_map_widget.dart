import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sizer/sizer.dart';

class ShowLocationMapWidget extends StatefulWidget {
  final double latitude;
  final double longitude;
  final String name;
  final double? height;
  final double? width;

  const ShowLocationMapWidget({Key? key, required this.latitude, required this.longitude, required this.name, this.height, this.width}) : super(key: key);

  @override
  _ShowLocationMapWidgetState createState() => _ShowLocationMapWidgetState();
}

class _ShowLocationMapWidgetState extends State<ShowLocationMapWidget> {
  final Completer<GoogleMapController> _controller = Completer();

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  Future<CameraPosition> getLocation() async{
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    CameraPosition initLocation = CameraPosition(
      target: LatLng(widget.latitude, widget.longitude),
      zoom: 16,
    );

    final marker = Marker(
      markerId: MarkerId(widget.name),
      position: LatLng(widget.latitude, widget.longitude),
    );

    markers[MarkerId(widget.name)] = marker;

    return initLocation;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getLocation(),
        builder: (context, snapshot) {
          if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
            CameraPosition initialLocation = snapshot.data as CameraPosition;

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: widget.height ?? 30.0.h,
                  width: widget.width ?? 95.0.w,
                  child: GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition: initialLocation,
                    onMapCreated: (GoogleMapController controller) {
                      if (!_controller.isCompleted) {
                        _controller.complete(controller);
                      }else{
                      }
                    },
                    markers: markers.values.toSet(),
                  ),
                ),
              ],
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        }
    );
  }
}
