import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:chewie/chewie.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:video_player/video_player.dart';
import 'package:visibility_detector/visibility_detector.dart';

class VideoPlayerOnFrontPage extends StatefulWidget {
  final String? base64;
  final UniqueKey newKey;

  const VideoPlayerOnFrontPage({Key? key, this.base64, required this.newKey}) : super(key: key);

  @override
  _VideoPlayerOnFrontPageState createState() => _VideoPlayerOnFrontPageState();
}

class _VideoPlayerOnFrontPageState extends State<VideoPlayerOnFrontPage> {
  VideoPlayerController? _controller;
  ChewieController? chewieController;

  bool isPlaying = false;

  StreamController<bool> streamController = BehaviorSubject();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      createVideoController();
    });
  }

  Future<VideoPlayerController?> createVideoController() async {
    streamController.add(false);

    var rng = Random();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File file = File(tempPath+ (rng.nextInt(100)).toString() +'.mp4');

    file.writeAsBytes(base64.decode(widget.base64!));

    // _controller = VideoPlayerController.file(file);
    _controller = VideoPlayerController.network(
        MyApp.navigatorKey.currentContext!.locale == const Locale('ar') ? arabicYoutubeLink : englishYoutubeLink
    );

    chewieController = ChewieController(
      videoPlayerController: _controller!,
      allowFullScreen: false,
      autoInitialize: true,
      showControls: true,
      looping: false,
      autoPlay: false,
      allowPlaybackSpeedChanging: false,
      allowMuting: true,
      startAt: const Duration(seconds: 1),
      showControlsOnInitialize: false,
      aspectRatio: 3.2/2,
    );

    // chewieController!.play();

    _controller!.addListener(() {
      if(_controller != null){
        if(_controller!.value.isPlaying){
          streamController.add(true);
        } else {
          streamController.add(false);
        }
      }
    });

    return _controller;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);

        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(LocaleKeys.watch_video.tr(), style: const TextStyle(color: Colors.black)),
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.keyboard_backspace),
            color: Colors.black,
          )
        ),
        body: Center(
          child: FutureBuilder(
              future: createVideoController(),
              builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.done && snapshot.hasData){
                  return VisibilityDetector(
                    key: const Key("frontPageAsset"),
                    onVisibilityChanged: (visibility){
                      if(visibility.visibleFraction == 0 && chewieController != null){
                        chewieController!.pause();
                      } else{
                        if(_controller != null && _controller!.value.duration != _controller!.value.position && chewieController != null){
                          chewieController!.play();
                        }
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black)
                      ),
                      child: AspectRatio(
                          aspectRatio: _controller!.value.aspectRatio,
                          child: Stack(
                            children: [
                              FutureBuilder(
                                future: Future.delayed(const Duration(seconds: 2)),
                                builder: (context, snapshot) {
                                  if(snapshot.connectionState == ConnectionState.done){
                                    chewieController!.play();
                                    return Chewie(
                                        controller: chewieController!
                                    );
                                  } else {
                                    return const Center(child: CircularProgressIndicator());
                                  }
                                }
                              ),
                              StreamBuilder(
                                  stream: streamController.stream,
                                  builder: (context, snapshot){
                                    bool? isPlay = snapshot.data as bool?;

                                    return isPlay == null || !isPlay ?
                                    const Padding(
                                      padding: EdgeInsets.only(top: 80.0),
                                      child: Align(
                                        alignment: Alignment.center,
                                        // child: CircularProgressIndicator()
                                      ),
                                    ) : const SizedBox(height: 0);
                                  }
                              )
                            ],
                          )
                      ),
                    ),
                  );
                } else {
                  return const SizedBox(
                      height: 50,
                      child: CircularProgressIndicator(color: Colors.white)
                  );
                }
              }
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller!.dispose();
    chewieController!.dispose();

    _controller = null;
    chewieController = null;
    super.dispose();
  }
}
