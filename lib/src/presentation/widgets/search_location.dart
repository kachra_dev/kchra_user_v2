import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/core/utils/helper/get_other_location.dart';
import 'package:kachra_user_app/src/core/utils/helper/uuid_generator.dart';

class SearchLocation extends StatefulWidget {
  const SearchLocation({Key? key}) : super(key: key);

  @override
  _SearchLocationState createState() => _SearchLocationState();
}

class _SearchLocationState extends State<SearchLocation> {
  @override
  Widget build(BuildContext context) {
    return CustomSearchScaffold();
  }
}

Future<void> displayPrediction(Prediction? p, BuildContext context) async {
  ScaffoldMessenger.of(context).showSnackBar(
    const SnackBar(
      content: Text("Getting Address..."),
      duration: Duration(seconds: 1),
    ),
  );

  if (p != null) {
    // get detail (lat/lng)
    GoogleMapsPlaces _places = GoogleMapsPlaces(
      apiKey: kGoogleApiKey,
      apiHeaders: await const GoogleApiHeaders().getHeaders(),
    );
    PlacesDetailsResponse detail =
    await _places.getDetailsByPlaceId(p.placeId!);
    final lat = detail.result.geometry!.location.lat;
    final lng = detail.result.geometry!.location.lng;

    // ScaffoldMessenger.of(context).showSnackBar(
    //   SnackBar(content: Text("${p.description} - $lat/$lng")),
    // );

    GetOtherLocation? getOtherLocation = GetOtherLocation(
      name: p.description,
      latitude: lat,
      longitude: lng,
      isCurrentLocation: false
    );

    Navigator.pop(context, getOtherLocation);
  }
}
final searchScaffoldKey = GlobalKey<ScaffoldState>();

class CustomSearchScaffold extends PlacesAutocompleteWidget {
  CustomSearchScaffold({Key? key})
      : super(
    key: key,
    apiKey: kGoogleApiKey,
    sessionToken: Uuid().generateV4(),
    language: "en",
    types: [],
    components: [Component(Component.country, "qa")],
    strictbounds: false,
  );

  @override
  _CustomSearchScaffoldState createState() => _CustomSearchScaffoldState();
}

class _CustomSearchScaffoldState extends PlacesAutocompleteState {
  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      title: AppBarPlacesAutoCompleteTextField(),
      iconTheme: const IconThemeData(color: Colors.black),
    );
    final body = PlacesAutocompleteResult(
      onTap: (p) {
        displayPrediction(p, context);
      },
      logo: Row(
        children: const [
          Text('Search for another location.', style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500, color: Colors.grey))
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
    return Scaffold(key: searchScaffoldKey, appBar: appBar, body: body);
  }

  @override
  void onResponseError(PlacesAutocompleteResponse response) {
    super.onResponseError(response);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(response.errorMessage!)),
    );
  }

  // @override
  // void onResponse(PlacesAutocompleteResponse? response) {
  //   super.onResponse(response);
  //   if (response != null && response.predictions.isNotEmpty) {
  //     ScaffoldMessenger.of(context).showSnackBar(
  //       const SnackBar(content: Text("Getting the address...")),
  //     );
  //   }
  // }
}