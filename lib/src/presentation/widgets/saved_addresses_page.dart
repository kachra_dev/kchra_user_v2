import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/main.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/hive/delete_hive.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';
import 'package:kachra_user_app/src/presentation/widgets/add_location_page.dart';
import 'package:kachra_user_app/src/presentation/widgets/show_address_details.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class SavedAddresses extends StatefulWidget {
  const SavedAddresses({Key? key}) : super(key: key);

  @override
  _SavedAddressesState createState() => _SavedAddressesState();
}

class _SavedAddressesState extends State<SavedAddresses> {

  showDetails(UserSavedAddressHive savedAddress){
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        insetPadding: EdgeInsets.zero,
        contentPadding: EdgeInsets.zero,
        content: ShowAddressDetails(savedAddress: savedAddress, height: 60.0.h),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
            child: Text(LocaleKeys.close.tr())
          )
        ],
      )
    );
  }

  Future<List<UserSavedAddressHive>> showSnackBarInstruction() async {
    List<UserSavedAddressHive> userSavedAddresses = await GetHive().getSavedAddressesOfCurrentUser();

    if(userSavedAddresses.isNotEmpty){
      WidgetsBinding.instance!.addPostFrameCallback((_) => ScaffoldMessenger.of(MyApp.navigatorKey.currentContext!).showSnackBar(
          SnackBar(
            content: Text(LocaleKeys.swipe_the_address_to_delete.tr()),
            duration: const Duration(seconds: 2),
          )
      ));
    }

    return userSavedAddresses;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        title: Text(LocaleKeys.saved_addresses.tr(), style: const TextStyle(color: Colors.black)),
      ),
      body: FutureBuilder(
        future: showSnackBarInstruction(),
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.done){
            List<UserSavedAddressHive> savedAddresses = snapshot.data as List<UserSavedAddressHive>;

            return savedAddresses.isNotEmpty ? ListView.builder(
              itemCount: savedAddresses.length,
              itemBuilder: (context, index){
                UserSavedAddressHive savedAddress = savedAddresses[index];

                return Dismissible(
                  key: Key(savedAddress.toString()),
                  confirmDismiss: (val) async {
                    return await showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: Text('${LocaleKeys.delete.tr()} "${savedAddress.addressName}"'),
                          content: Text(LocaleKeys.are_you_sure_you_want_to_delete.tr(args: ["${savedAddress.addressName}"])),
                          actions: [
                            TextButton(
                                onPressed: () async {
                                  await DeleteHive().deleteUserSavedAddress(index);
                                  ScaffoldMessenger.of(MyApp.navigatorKey.currentContext!).showSnackBar(SnackBar(content: Text('"${savedAddress.addressName}" is deleted.')));

                                  Navigator.pop(context, true);
                                },
                                child: Text('${LocaleKeys.delete.tr()} $yesEmoji')
                            ),
                            TextButton(
                                onPressed: () => Navigator.pop(context, false),
                                child: Text('${LocaleKeys.cancel.tr()} $noEmoji')
                            )
                          ],
                        )
                    );
                  },
                  background: Container(color: Colors.red),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Card(
                      child: ListTile(
                        onTap: () => showDetails(savedAddress),
                        leading: const Icon(CupertinoIcons.location, color: Colors.red),
                        title: Text(
                          savedAddress.addressName ?? 'N/A'
                        ),
                        trailing: IconButton(
                          onPressed: () => showDetails(savedAddress),
                          icon: const Icon(Icons.info_outline)
                        ),
                      ),
                    ),
                  ),
                );
              }
            ) : Center(
              child: Text(LocaleKeys.no_saved_addresses.tr()),
            );
          } else {
             return const Center(
               child: CircularProgressIndicator(),
             );
          }
        }
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'booking',
        onPressed: () {
          pushNewScreen(context, screen: const AddLocationPage(), withNavBar: false);
        },
        child: const Icon(CupertinoIcons.add),
      ),
    );
  }
}
