import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:kachra_user_app/src/core/utils/helper/get_current_location.dart';
import 'package:kachra_user_app/src/core/utils/helper/get_other_location.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_and_widgets.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/core/utils/helper/token_checker.dart';
import 'package:kachra_user_app/src/core/utils/hive/add_hive.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/core/utils/messages/registration_required_message.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/enter_manual_address.dart';
import 'package:kachra_user_app/src/presentation/views/authenticated/dashboard/content/site_visit_schedule/pin_address_google_map.dart';
import 'package:kachra_user_app/src/presentation/widgets/current_location_map_widget.dart';
import 'package:kachra_user_app/src/presentation/widgets/search_location.dart';
import 'package:kachra_user_app/src/presentation/widgets/show_location_map_widget.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class AddLocationPage extends StatefulWidget {
  const AddLocationPage(
      {Key? key})
      : super(key: key);

  @override
  _AddLocationPageState createState() => _AddLocationPageState();
}

class _AddLocationPageState extends State<AddLocationPage> {
  TextEditingController addressName = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  GetOtherLocation? getOtherLocation;

  bool isButtonLoading = false;

  Future<void> addLocation() async {
    ProfileHive profile = await GetHive().getCurrentUserProfile();

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(LocaleKeys.please_name_this_address.tr()),
              content: Form(
                key: _formKey,
                child: Directionality(
                  textDirection: ltrTextDirection,
                  child: TextFormField(
                    controller: addressName,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return LocaleKeys.address_name_must_not_be_empty.tr();
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      labelText: LocaleKeys.address_name.tr(),
                    ),
                  ),
                ),
              ),
              actions: [
                TextButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        GetCurrentLocation getCurrentLocation =
                            await GetLocation().getLocation();

                        UserSavedAddressHive currentLocation =
                            UserSavedAddressHive(
                                userId: profile.id,
                                name: getCurrentLocation.place.name,
                                street: getCurrentLocation.place.street,
                                iSOCountryCode: getCurrentLocation
                                    .place.isoCountryCode,
                                country: getCurrentLocation.place.country,
                                administrativeArea: getCurrentLocation
                                    .place.administrativeArea,
                                locality: getCurrentLocation.place.locality,
                                latitude: getCurrentLocation
                                    .location.latitude
                                    .toString(),
                                longitude: getCurrentLocation.location.longitude
                                    .toString(),
                                addressName: addressName.text);

                        AddHive().addUserSavedAddress(currentLocation);

                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text(LocaleKeys.saved_current_location.tr())),
                        );

                        addressName.clear();

                        Navigator.of(context, rootNavigator: true).pop();
                      }
                    },
                    child: Text('${LocaleKeys.save_address.tr()} $yesEmoji')),
                TextButton(
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop();
                    },
                    child: Text('${LocaleKeys.cancel.tr()} $noEmoji')),
              ],
            ));
  }

  Future<void> addOtherLocation(GetOtherLocation? getOtherLocation) async {
    ProfileHive profile = await GetHive().getCurrentUserProfile();

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(LocaleKeys.please_name_this_address.tr()),
          content: Form(
            key: _formKey,
            child: Directionality(
              textDirection: ltrTextDirection,
              child: TextFormField(
                controller: addressName,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return LocaleKeys.address_name_must_not_be_empty.tr();
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: LocaleKeys.address_name.tr(),
                ),
              ),
            ),
          ),
          actions: [
            TextButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {

                    UserSavedAddressHive otherLocation =
                    UserSavedAddressHive(
                      userId: profile.id,
                      name: getOtherLocation!.name,
                      country: 'Qatar',
                      latitude: getOtherLocation.latitude!.toString(),
                      longitude: getOtherLocation.longitude!.toString(),
                      addressName: addressName.text,
                      apartmentNumber: getOtherLocation.apartmentNumber,
                      buildingNumber: getOtherLocation.buildingNumber,
                      streetNumber: getOtherLocation.streetNumber,
                      city: getOtherLocation.city,
                      zoneNumber: getOtherLocation.zoneNumber,
                    );

                    AddHive().addUserSavedAddress(otherLocation);

                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(LocaleKeys.saved_current_location.tr())),
                    );

                    addressName.clear();

                    Navigator.of(context, rootNavigator: true).pop();
                  }
                },
                child: Text('${LocaleKeys.save_address.tr()} $yesEmoji')
            ),
            TextButton(
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                },
                child: Text('${LocaleKeys.cancel.tr()} $noEmoji')),
          ],
        ));
  }

  late Position location;
  late Placemark place;

  bool isCurrentLocationLoading = false;

  Future<void> getCurrentLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    location = await Geolocator.getCurrentPosition();
    List<Placemark> p = await placemarkFromCoordinates(location.latitude, location.longitude);
    place = p[0];
  }

  Future<void> initializeData() async {
    Future.delayed(Duration.zero, (){});
    getCurrentLocation();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initializeData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        List<UserSavedAddressHive> savedAddresses = await GetHive().getSavedAddressesOfCurrentUser();
        savedAddresses.isEmpty ? Navigator.pop(context) : Navigator.pop(context, savedAddresses);
        return true;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text(LocaleKeys.add_location.tr(), style: const TextStyle(color: Colors.black)),
          actions: [
            IconButton(
              onPressed: (){
                setState(() {
                  getOtherLocation = null;
                });
              },
              icon: const Icon(Icons.location_searching_rounded, color: Colors.black)
            )
          ],
        ),
        body: Column(
          children: [
            Expanded(
              flex: 11,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 80.0.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            border: Border.all(color: Colors.black, width: 1.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: getOtherLocation != null &&
                                  getOtherLocation!.isCurrentLocation == false
                              ? ShowLocationMapWidget(
                                  latitude: getOtherLocation!.latitude!,
                                  longitude: getOtherLocation!.longitude!,
                                  name: getOtherLocation!.name!
                              )
                              : const CurrentLocationMapWidget(isHideAddress: true),
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      getOtherLocation != null && getOtherLocation!.isCurrentLocation == false ? const Text('Current Location:') : Text(LocaleKeys.selected_location.tr()),
                      const SizedBox(height: 10.0),
                      getOtherLocation != null && getOtherLocation!.isCurrentLocation == false ?
                      Flexible(
                        child: Text(
                          getOtherLocation!.name!,
                          textAlign: TextAlign.center,
                        ),
                      ) :
                      FutureBuilder(
                          future: GetLocation().getLocation(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.done) {
                              GetCurrentLocation currentLocation =
                                  snapshot.data as GetCurrentLocation;

                              String address =
                                  currentLocation.place.name.toString() +
                                      " " +
                                      currentLocation.place.street.toString() +
                                      ", " +
                                      currentLocation.place.country.toString();

                              return Flexible(
                                child: Text(
                                  address,
                                  textAlign: TextAlign.center,
                                ),
                              );
                            } else {
                              return const Text('Getting location...');
                            }
                          }),
                      const SizedBox(height: 50.0),
                      Directionality(
                        textDirection: ltrTextDirection,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'What does saving location do?',
                              style: TextStyle(
                                  fontSize: 12.0.sp, fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(width: 10.0),
                            const Icon(
                              Icons.info_outline,
                              size: 15.0,
                            )
                          ],
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      const Directionality(
                        textDirection: ltrTextDirection,
                        child: Text(
                          'There’s no need to actually be at the address of your place. Save it once and make it easier for future bookings.',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      TextButton(
                          onPressed: () async {
                            final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => const SearchLocation()));

                            setState(() {
                              getOtherLocation = result;
                            });
                          },
                          child: Text(
                            LocaleKeys.not_the_location_search_here.tr(),
                            style: const TextStyle(fontSize: 12.0),
                          )),
                      FutureBuilder(
                        future: initializeData(),
                        builder: (context, snapshot) {
                          if(snapshot.connectionState == ConnectionState.done){
                            return TextButton(
                                onPressed: () async {
                                  final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => EnterManualAddress(latitude: location.latitude, longitude: location.longitude, name: place.name!)));

                                  setState(() {
                                    getOtherLocation = result;
                                  });
                                },
                                child: Text(
                                  LocaleKeys.enter_address_manually.tr(),
                                  style: const TextStyle(fontSize: 12.0),
                                ),
                                style: TextButton.styleFrom(
                                  minimumSize: Size.zero,
                                  padding: EdgeInsets.zero,
                                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,)
                            );
                          } else {
                            return const CircularProgressIndicator();
                          }
                        }
                      ),
                      const SizedBox(height: 10.0),
                      FutureBuilder(
                          future: initializeData(),
                          builder: (context, snapshot) {
                            if(snapshot.connectionState == ConnectionState.done){
                              return TextButton(
                                  onPressed: () async {
                                    final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => PinAddressGoogleMap(latitude: location.latitude, longitude: location.longitude, name: place.name!)));

                                    setState(() {
                                      getOtherLocation = result;
                                    });
                                  },
                                  child: Text(
                                    LocaleKeys.pin_address_in_google_map.tr(),
                                    style: const TextStyle(fontSize: 12.0),
                                  ),
                                  style: TextButton.styleFrom(
                                    minimumSize: Size.zero,
                                    padding: EdgeInsets.zero,
                                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,)
                              );
                            } else {
                              return const CircularProgressIndicator();
                            }
                          }
                      )
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    getOtherLocation != null ? addOtherLocation(getOtherLocation) : addLocation();
                  },
                  child: Container(
                    width: 100.0.w,
                    color: Colors.green,
                    child: Center(
                        child: Text(
                      LocaleKeys.save_this_location.tr(),
                      style: const TextStyle(color: Colors.white, fontSize: 15.0),
                    )),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
