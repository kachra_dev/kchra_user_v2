import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/data/models/auth/auth_model.dart';
import 'package:kachra_user_app/src/data/models/auth/register_auth_model.dart';
import 'package:retrofit/retrofit.dart';

part 'auth_api_service.g.dart';

@RestApi(baseUrl: kLaravelUrl)
abstract class AuthApiService {
  factory AuthApiService(Dio dio, {String baseUrl}) = _AuthApiService;

  @POST('/auth/login')
  Future<HttpResponse<AuthModel>> login(
      @Field("email") String? email,
      @Field("password") String? password,
      @Field("activation_code") String? activationCode
  );

  @POST('/auth/login-google')
  Future<HttpResponse<AuthModel>> loginGoogle(
      @Field("email") String? email
  );

  @POST('/user/register')
  Future<HttpResponse<RegisterAuthModel>> register(
    @Field("name") String? name,
    @Field("email") String? email,
    @Field("password") String? password,
    @Field("password_confirmation") String? passwordConfirmation,
    @Field("address") AddressModel? address,
    @Field("phone_number") String? phoneNumber,
    @Field("country") String? country,
    @Field("role") String? role,
  );

  @POST('/user/register-using-google')
  Future<HttpResponse<RegisterAuthModel>> registerUsingGoogle(
      @Field("name") String? name,
      @Field("email") String? email,
      @Field("address") AddressModel? address,
      @Field("phone_number") String? phoneNumber,
      @Field("country") String? country,
      @Field("role") String? role,
  );

  @PUT('/user/update-user')
  Future<HttpResponse<AuthModel>> updateUserPhoneNumber(
      @Field("id") String? userId,
      @Field("phone_number") String? phoneNumber,
  );

  @POST('/user/register-guest')
  Future<HttpResponse<AuthModel>> registerAsGuest(
      @Field("name") String? name,
      @Field("address") AddressModel? address,
      @Field("phone_number") String? phoneNumber
  );

  @POST('/auth/login-phone-number')
  Future<HttpResponse<AuthModel>> loginPhoneNumber(
      @Field("phone_number") String? phoneNumber
  );
}
