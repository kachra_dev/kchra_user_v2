// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'site_visit_schedule_api_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps

class _SiteVisitScheduleApiService implements SiteVisitScheduleApiService {
  _SiteVisitScheduleApiService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://phpstack-708702-2355919.cloudwaysapps.com/api';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<HttpResponse<SiteVisitScheduleModel>> createSiteVisitSchedule(
      scheduledDate, userId, address) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _headers = <String, dynamic>{};
    final _data = {
      'scheduled_date': scheduledDate,
      'user_id': userId,
      'address': address
    };
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<HttpResponse<SiteVisitScheduleModel>>(
            Options(method: 'POST', headers: _headers, extra: _extra)
                .compose(_dio.options, '/estimator/create-site-visit',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SiteVisitScheduleModel.fromJson(_result.data!);
    final httpResponse = HttpResponse(value, _result);
    return httpResponse;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
