import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/data/models/site_visit_schedule/site_visit_schedule_model.dart';
import 'package:retrofit/retrofit.dart';

part 'site_visit_schedule_api_service.g.dart';

@RestApi(baseUrl: kLaravelUrl)
abstract class SiteVisitScheduleApiService {
  factory SiteVisitScheduleApiService(Dio dio, {String baseUrl}) = _SiteVisitScheduleApiService;

  @POST('/estimator/create-site-visit')
  Future<HttpResponse<SiteVisitScheduleModel>> createSiteVisitSchedule(
      @Field("scheduled_date") String? scheduledDate,
      @Field("user_id") String? userId,
      @Field("address") AddressModel? address,
  );
}
