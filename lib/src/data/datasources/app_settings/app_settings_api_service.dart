import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/data/models/app_settings/app_settings_model.dart';
import 'package:retrofit/retrofit.dart';

part 'app_settings_api_service.g.dart';

@RestApi(baseUrl: kLaravelUrl)
abstract class AppSettingsApiService {
  factory AppSettingsApiService(Dio dio, {String baseUrl}) = _AppSettingsApiService;

  @GET('/appSettings/get-appSettings')
  Future<HttpResponse<AppSettingsModel>> getAppSettings();
}
