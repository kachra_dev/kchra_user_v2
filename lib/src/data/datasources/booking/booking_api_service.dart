import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/booking/list_booking_model.dart';
import 'package:kachra_user_app/src/data/models/general_response.dart';
import 'package:kachra_user_app/src/data/models/notifications/notifications_model.dart';
import 'package:retrofit/retrofit.dart';

part 'booking_api_service.g.dart';

@RestApi(baseUrl: kLaravelUrl)
abstract class BookingApiService {
  factory BookingApiService(Dio dio, {String baseUrl}) = _BookingApiService;

  @POST('/booking/create-booking')
  Future<HttpResponse<BookingModel>> createBooking(
      @Field("user_id") String? userId,
      @Field("booking_status") String? bookingStatus,
      @Field("address") AddressModel? address,
      @Field("asset") String? asset,
      @Field("asset_type") String? assetType,
      @Field("booking_details") String? bookingDetails,
      @Field("booking_purpose") String? bookingPurpose
  );

  @POST('/booking/get-mobilelist-bookings')
  Future<HttpResponse<List<ListBookingModel>>> getBookings(
      @Field("user_id") String? userId
  );

  @PUT('/booking/accept-decline-booking')
  Future<HttpResponse<GeneralResponse>> acceptDeclineBooking(
      @Field("id") String? bookingId,
      @Field("quote_status") String? bookingQuoteStatus
  );

  @PUT('/booking/set-payment-method')
  Future<HttpResponse<GeneralResponse>> setPaymentMethod(
      @Field("id") String? bookingId,
      @Field("payment_method") String? paymentMethod
  );

  @POST('/booking/find-booking')
  Future<HttpResponse<BookingModel>> findBooking(
      @Field("id") String? userId
  );

  @POST('/notifications/get-notifications')
  Future<HttpResponse<List<NotificationsModel>>> getNotifications(
      @Field("user_id") String? userId
  );

  @POST('/booking/set-preferred-pickup')
  Future<HttpResponse<BookingModel>> setPreferredPickupDate(
      @Field("id") String? bookingId,
      @Field("preferred_pickup_date") String? preferredPickupDate
  );

  @POST('/booking/change-booking-purpose')
  Future<HttpResponse<BookingModel>> changeBookingPurpose(
      @Field("booking_id") String? bookingId,
      @Field("booking_purpose") String? bookingPurpose
  );
}
