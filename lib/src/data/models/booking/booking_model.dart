import 'package:kachra_user_app/src/data/models/auth/address_model.dart';
import 'package:kachra_user_app/src/data/models/profile/driver_model.dart';
import 'package:kachra_user_app/src/data/models/profile/profile_model.dart';

class BookingModel {
  final String? id;
  final String? userId;
  final AddressModel? address;
  final int? estimatedAmount;
  final String? bookingStatus;
  final String? quoteStatus;
  final String? assetType;
  final DriverModel? driverAssigned;
  final String? asset;
  final String? updatedAt;
  final String? bookingDetails;
  final String? paymentMethod;
  final int? additionalAmount;
  final DateTime? preferredPickupDate;
  final String? bookingPurpose;

  BookingModel(
      {this.id,
      this.userId,
      this.address,
      this.estimatedAmount,
      this.bookingStatus,
      this.quoteStatus,
      this.assetType,
      this.driverAssigned,
      this.asset,
      this.updatedAt,
      this.bookingDetails,
      this.paymentMethod,
      this.additionalAmount,
      this.preferredPickupDate,
      this.bookingPurpose
    });

  factory BookingModel.fromJson(Map<String, dynamic> json) {
    return BookingModel(
      id: json['id'] as String?,
      userId: json['user_id'] as String?,
      address: AddressModel.fromJson(json['address']),
      estimatedAmount: json['estimated_amount'] as int?,
      bookingStatus: json['booking_status'] as String?,
      quoteStatus: json['quote_status'] as String?,
      assetType: json['asset_type'] as String?,
      driverAssigned: json['driver_assigned'] != null ? DriverModel.fromJson(json['driver_assigned']) : null,
      asset: json['asset'] as String?,
      updatedAt: json['updated_at'] as String?,
      bookingDetails: json['booking_details'] as String?,
      paymentMethod: json['payment_method'] as String?,
      additionalAmount: json['additional_amount'] as int?,
      preferredPickupDate: json['preferred_pickup_date'] != null ? DateTime.parse(json['preferred_pickup_date'].toString()) : null,
      bookingPurpose: json['booking_purpose'] as String?
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "address": address,
    "estimated_amount": estimatedAmount,
    "booking_status": bookingStatus,
    "quote_status": quoteStatus,
    "asset_type": assetType,
    "driver_assigned": driverAssigned,
    "asset": asset,
    "updated_at": updatedAt,
    "booking_details": bookingDetails,
    "payment_method": paymentMethod,
    "additional_amount": additionalAmount,
    "preferred_pickup_date" : preferredPickupDate.toString(),
    "booking_purpose": bookingPurpose
  };
}