class ListBookingModel {
  final String? id;
  final int? estimatedAmount;
  final String? quoteStatus;
  final String? updatedAt;
  final String? bookingStatus;
  final DateTime? preferredPickupDate;
  final String? bookingPurpose;
  ListBookingModel({this.id, this.estimatedAmount, this.quoteStatus, this.updatedAt, this.bookingStatus, this.preferredPickupDate, this.bookingPurpose});

  factory ListBookingModel.fromJson(Map<String, dynamic> json) {
    return ListBookingModel(
      id: json['id'] as String?,
      estimatedAmount: json['estimated_amount'] as int?,
      quoteStatus: json['quote_status'] as String?,
      updatedAt: json['updated_at'] as String?,
      bookingStatus: json['booking_status'] as String?,
      preferredPickupDate: json['preferred_pickup_date'] != null ? DateTime.parse(json['preferred_pickup_date'].toString()) : null,
      bookingPurpose: json['booking_purpose'] as String?
    );
  }
}
