class SiteVisitScheduleModel{
  final DateTime? scheduledDate;
  final String? userId;
  final String? addressId;
  final String? scheduleStatus;

  SiteVisitScheduleModel({this.scheduledDate, this.userId, this.addressId, this.scheduleStatus});

  factory SiteVisitScheduleModel.fromJson(Map<String, dynamic> json){
    return SiteVisitScheduleModel(
      scheduledDate: json['scheduled_date'] != null ? DateTime.parse(json['scheduled_date']) : null,
      userId: json['user_id'] as String?,
      addressId: json['address'] as String?,
      scheduleStatus: json['schedule_status'] as String?
    );
  }
}