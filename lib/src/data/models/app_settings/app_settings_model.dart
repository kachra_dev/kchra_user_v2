class AppSettingsModel{
  final String? mobileUserVersion;
  final String? mobileUserUrlLink;

  AppSettingsModel({this.mobileUserVersion, this.mobileUserUrlLink});

  factory AppSettingsModel.fromJson(Map<String, dynamic> json){
    return AppSettingsModel(
      mobileUserVersion: json['mobile_user_version'] as String?,
      mobileUserUrlLink: json['mobile_user_url_link'] as String?
    );
  }
}