// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'locally_saved_booking_assets_hive.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LocallySavedBookingAssetsHiveAdapter
    extends TypeAdapter<LocallySavedBookingAssetsHive> {
  @override
  final int typeId = 3;

  @override
  LocallySavedBookingAssetsHive read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LocallySavedBookingAssetsHive(
      bookingId: fields[0] as String?,
      asset: fields[1] as String?,
      assetType: fields[2] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, LocallySavedBookingAssetsHive obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.bookingId)
      ..writeByte(1)
      ..write(obj.asset)
      ..writeByte(2)
      ..write(obj.assetType);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LocallySavedBookingAssetsHiveAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
