import 'package:hive/hive.dart';

part 'address_hive.g.dart';

@HiveType(typeId: 1)
class AddressHive {
  @HiveField(0)
  final String? id;

  @HiveField(1)
  final String? name;

  @HiveField(2)
  final String? street;

  @HiveField(3)
  final String? ISO_country_code;

  @HiveField(4)
  final String? country;

  @HiveField(5)
  final String? administrative_area;

  @HiveField(6)
  final String? locality;

  @HiveField(7)
  final String? latitude;

  @HiveField(8)
  final String? longitude;

  AddressHive(
      {this.id,
      this.name,
      this.street,
      this.ISO_country_code,
      this.country,
      this.administrative_area,
      this.locality,
      this.latitude,
      this.longitude});

  factory AddressHive.fromJson(Map<String, dynamic> json){
    return AddressHive(
      id: json['id'] as String?,
      name: json['name'] as String?,
      street: json['street'] as String?,
      ISO_country_code: json['ISO_country_code'] as String?,
      country: json['country'] as String?,
      administrative_area: json['administrative_area'] as String?,
      locality: json['locality'] as String?,
      latitude: json['latitude'] as String?,
      longitude: json['longitude'] as String?
    );
  }

}
