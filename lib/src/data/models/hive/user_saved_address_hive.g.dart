// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_saved_address_hive.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserSavedAddressHiveAdapter extends TypeAdapter<UserSavedAddressHive> {
  @override
  final int typeId = 2;

  @override
  UserSavedAddressHive read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserSavedAddressHive(
      userId: fields[0] as String?,
      name: fields[1] as String?,
      street: fields[2] as String?,
      iSOCountryCode: fields[3] as String?,
      country: fields[4] as String?,
      administrativeArea: fields[5] as String?,
      locality: fields[6] as String?,
      latitude: fields[7] as String?,
      longitude: fields[8] as String?,
      addressName: fields[9] as String?,
      apartmentNumber: fields[10] as String?,
      buildingNumber: fields[11] as String?,
      streetNumber: fields[12] as String?,
      city: fields[13] as String?,
      zoneNumber: fields[14] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, UserSavedAddressHive obj) {
    writer
      ..writeByte(15)
      ..writeByte(0)
      ..write(obj.userId)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.street)
      ..writeByte(3)
      ..write(obj.iSOCountryCode)
      ..writeByte(4)
      ..write(obj.country)
      ..writeByte(5)
      ..write(obj.administrativeArea)
      ..writeByte(6)
      ..write(obj.locality)
      ..writeByte(7)
      ..write(obj.latitude)
      ..writeByte(8)
      ..write(obj.longitude)
      ..writeByte(9)
      ..write(obj.addressName)
      ..writeByte(10)
      ..write(obj.apartmentNumber)
      ..writeByte(11)
      ..write(obj.buildingNumber)
      ..writeByte(12)
      ..write(obj.streetNumber)
      ..writeByte(13)
      ..write(obj.city)
      ..writeByte(14)
      ..write(obj.zoneNumber);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserSavedAddressHiveAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
