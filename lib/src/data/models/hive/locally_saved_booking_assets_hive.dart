import 'package:hive/hive.dart';

part 'locally_saved_booking_assets_hive.g.dart';

@HiveType(typeId: 3)
class LocallySavedBookingAssetsHive {
  @HiveField(0)
  final String? bookingId;

  @HiveField(1)
  final String? asset;

  @HiveField(2)
  final String? assetType;

  LocallySavedBookingAssetsHive({this.bookingId, this.asset, this.assetType});

  factory LocallySavedBookingAssetsHive.fromJson(Map<String, dynamic> json){
    return LocallySavedBookingAssetsHive(
      bookingId: json['bookingId'] as String?,
      asset: json['asset'] as String?,
      assetType: json['assetType'] as String?
    );
  }
}