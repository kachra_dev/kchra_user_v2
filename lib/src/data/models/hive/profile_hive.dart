import 'package:hive/hive.dart';
import 'package:kachra_user_app/src/data/models/hive/address_hive.dart';

part 'profile_hive.g.dart';

@HiveType(typeId: 0)
class ProfileHive {
  @HiveField(0)
  final String? id;

  @HiveField(1)
  final String? name;

  @HiveField(2)
  final String? email;

  @HiveField(3)
  final AddressHive? address;

  @HiveField(4)
  final String? phoneNumber;

  @HiveField(5)
  final String? country;

  @HiveField(6)
  final int? isGuest;

  ProfileHive({this.id, this.name, this.email, this.address, this.phoneNumber, this.country, this.isGuest});

  factory ProfileHive.fromJson(Map<String, dynamic> json){
    return ProfileHive(
      id: json['id'] as String?,
      name: json['name'] as String?,
      email: json['email'] as String?,
      address: AddressHive.fromJson(json['address']),
      phoneNumber: json['phoneNumber'] as String?,
      country: json['country'] as String?,
      isGuest: json['isGuest'] as int?
    );
  }
}
