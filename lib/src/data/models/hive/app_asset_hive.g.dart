// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_asset_hive.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AppAssetHiveAdapter extends TypeAdapter<AppAssetHive> {
  @override
  final int typeId = 4;

  @override
  AppAssetHive read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AppAssetHive(
      frontPageAssetArabic: fields[0] as String?,
      frontPageAssetEnglish: fields[1] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, AppAssetHive obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.frontPageAssetArabic)
      ..writeByte(1)
      ..write(obj.frontPageAssetEnglish);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppAssetHiveAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
