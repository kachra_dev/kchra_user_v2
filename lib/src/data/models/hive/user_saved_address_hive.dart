import 'package:hive/hive.dart';

part 'user_saved_address_hive.g.dart';

@HiveType(typeId: 2)
class UserSavedAddressHive {
  @HiveField(0)
  final String? userId;

  @HiveField(1)
  final String? name;

  @HiveField(2)
  final String? street;

  @HiveField(3)
  final String? iSOCountryCode;

  @HiveField(4)
  final String? country;

  @HiveField(5)
  final String? administrativeArea;

  @HiveField(6)
  final String? locality;

  @HiveField(7)
  final String? latitude;

  @HiveField(8)
  final String? longitude;

  @HiveField(9)
  final String? addressName;

  @HiveField(10)
  final String? apartmentNumber;

  @HiveField(11)
  final String? buildingNumber;

  @HiveField(12)
  final String? streetNumber;

  @HiveField(13)
  final String? city;

  @HiveField(14)
  final String? zoneNumber;

  UserSavedAddressHive(
      {this.userId,
        this.name,
        this.street,
        this.iSOCountryCode,
        this.country,
        this.administrativeArea,
        this.locality,
        this.latitude,
        this.longitude,
        this.addressName,
        this.apartmentNumber,
        this.buildingNumber,
        this.streetNumber,
        this.city,
        this.zoneNumber
      });

  factory UserSavedAddressHive.fromJson(Map<String, dynamic> json){
    return UserSavedAddressHive(
        userId: json['userId'] as String?,
        name: json['name'] as String?,
        street: json['street'] as String?,
        iSOCountryCode: json['ISO_country_code'] as String?,
        country: json['country'] as String?,
        administrativeArea: json['administrative_area'] as String?,
        locality: json['locality'] as String?,
        latitude: json['latitude'] as String?,
        longitude: json['longitude'] as String?,
        addressName: json['address_name'] as String?,
        apartmentNumber: json['apartment_number'] as String?,
        buildingNumber: json['building_number'] as String?,
        streetNumber: json['street_number'] as String?,
        city: json['city'] as String?,
        zoneNumber: json['zone_number'] as String?
    );
  }

}
