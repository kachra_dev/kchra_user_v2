// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_hive.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AddressHiveAdapter extends TypeAdapter<AddressHive> {
  @override
  final int typeId = 1;

  @override
  AddressHive read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AddressHive(
      id: fields[0] as String?,
      name: fields[1] as String?,
      street: fields[2] as String?,
      ISO_country_code: fields[3] as String?,
      country: fields[4] as String?,
      administrative_area: fields[5] as String?,
      locality: fields[6] as String?,
      latitude: fields[7] as String?,
      longitude: fields[8] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, AddressHive obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.street)
      ..writeByte(3)
      ..write(obj.ISO_country_code)
      ..writeByte(4)
      ..write(obj.country)
      ..writeByte(5)
      ..write(obj.administrative_area)
      ..writeByte(6)
      ..write(obj.locality)
      ..writeByte(7)
      ..write(obj.latitude)
      ..writeByte(8)
      ..write(obj.longitude);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddressHiveAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
