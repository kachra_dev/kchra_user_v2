import 'package:hive/hive.dart';

part 'app_asset_hive.g.dart';

@HiveType(typeId: 4)
class AppAssetHive {
  @HiveField(0)
  final String? frontPageAssetArabic;

  @HiveField(1)
  final String? frontPageAssetEnglish;

  AppAssetHive({this.frontPageAssetArabic, this.frontPageAssetEnglish});

  factory AppAssetHive.fromJson(Map<String, dynamic> json){
    return AppAssetHive(
      frontPageAssetArabic: json['frontPageAssetArabic'] as String?,
      frontPageAssetEnglish: json['frontPageAssetEnglish'] as String?,
    );
  }
}