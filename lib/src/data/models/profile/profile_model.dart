class ProfileModel{
  final String? id;
  final String? name;
  final String? email;
  final Map<String, dynamic>? address;
  final String? phoneNumber;
  final String? country;
  final String? role;
  final int? isGuest;

  ProfileModel(
      {required this.id, required this.name, required this.email, required this.address, required this.phoneNumber, required this.country, required this.role, this.isGuest});

  factory ProfileModel.fromJson(Map<String, dynamic>? json) {
    return ProfileModel(
        id: json!['id'] as String?,
        name: json['name'] as String?,
        email: json['email'] as String?,
        address: json['address'] as Map<String, dynamic>,
        phoneNumber: json['phone_number'] as String?,
        country: json['country'] as String?,
        role: json['role'] as String?,
        isGuest: json['is_guest'] as int?
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "address": address,
    "phoneNumber": phoneNumber,
    "country": country,
    "role": role,
    "isGuest": isGuest
  };
}