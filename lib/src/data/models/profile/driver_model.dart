import 'package:kachra_user_app/src/data/models/profile/profile_model.dart';

class DriverModel {
  final String? id;
  final String? userId;
  final String? availabilityStatus;
  final String? driverAccountStatus;
  final ProfileModel? user;

  DriverModel({this.id, this.userId, this.availabilityStatus, this.driverAccountStatus, this.user});

  factory DriverModel.fromJson(Map<String, dynamic> json) {
    return DriverModel(
      id: json['id'] as String?,
      userId: json['user_id'] as String?,
      availabilityStatus: json['availability_status'] as String?,
      driverAccountStatus: json['driver_account_status'] as String?,
      user: json['user'] != null ? ProfileModel.fromJson(json['user']) : null,
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "availability_status": availabilityStatus,
    "driver_account_status": driverAccountStatus,
    "user": user
  };
}