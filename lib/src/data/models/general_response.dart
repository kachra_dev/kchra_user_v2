class GeneralResponse{
  final String message;

  GeneralResponse({required this.message});

  factory GeneralResponse.fromJson(Map<String, dynamic> json){
    return GeneralResponse(
      message: json['message'] as String
    );
  }
}