class NotificationsModel{
  final String? id;
  final String? userId;
  final String? message;
  final String? updatedAt;

  NotificationsModel({this.id,  this.userId,  this.message, this.updatedAt});

  factory NotificationsModel.fromJson(Map<String, dynamic> json){
    return NotificationsModel(
      id: json['id'] as String?,
      userId: json['user_id'] as String?,
      message: json['message'] as String?,
      updatedAt: json['updated_at'] as String?
    );
  }
}