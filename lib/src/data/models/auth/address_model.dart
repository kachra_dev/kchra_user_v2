class AddressModel {
  final String? id;
  final String? name;
  final String? street;
  final String? isoCountryCode;
  final String? country;
  final String? administrativeArea;
  final String? locality;
  final String? latitude;
  final String? longitude;
  final int? hasBluePlateNumber;
  final String? apartmentNumber;
  final String? buildingNumber;
  final String? streetNumber;
  final String? city;
  final String? zone;

  AddressModel(
      {this.id,
      this.name,
      this.street,
      this.isoCountryCode,
      this.country,
      this.administrativeArea,
      this.locality,
      this.latitude,
      this.longitude,
      this.hasBluePlateNumber,
      this.apartmentNumber,
      this.buildingNumber,
      this.streetNumber,
      this.city,
      this.zone});

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return AddressModel(
      id: json['id'] as String?,
      name: json['name'] as String?,
      street: json['street'] as String?,
      isoCountryCode: json['ISO_country_code'] as String?,
      country: json['country'] as String?,
      administrativeArea: json['administrative_area'] as String?,
      locality: json['locality'] as String?,
      latitude: json['latitude'] as String?,
      longitude: json['longitude'] as String?,
      hasBluePlateNumber: json['has_blue_plate_number'] as int?,
      apartmentNumber: json['apartment_number'] as String?,
      buildingNumber: json['building_number'] as String?,
      streetNumber: json['street_number'] as String?,
      city: json['city'] as String?,
      zone: json['zone'] as String?
    );
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'street': street,
    'ISO_country_code': isoCountryCode,
    'country': country,
    'administrative_area': administrativeArea,
    'locality': locality,
    'latitude': latitude,
    'longitude': longitude,
    'apartment_number': apartmentNumber,
    'building_number': buildingNumber,
    'street_number': streetNumber,
    'city': city,
    'zone': zone
  };
}
