import 'package:kachra_user_app/src/data/models/profile/profile_model.dart';

class AuthModel {
  final String? accessToken;
  final String? tokenType;
  final int? expiresIn;
  final ProfileModel? user;

  AuthModel({this.accessToken, this.tokenType, this.expiresIn, this.user});

  factory AuthModel.fromJson(Map<String, dynamic> json) {
    return AuthModel(
        accessToken: json['access_token'] as String?,
        tokenType: json['token_type'] as String?,
        expiresIn: json['expires_in'] as int?,
        user: ProfileModel.fromJson(json['user']));
  }
}