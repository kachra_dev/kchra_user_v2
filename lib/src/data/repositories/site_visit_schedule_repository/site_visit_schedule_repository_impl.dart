import 'dart:io';

import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/params/site_visit_schedule_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/data/datasources/site_visit_schedule/site_visit_schedule_api_service.dart';
import 'package:kachra_user_app/src/data/models/site_visit_schedule/site_visit_schedule_model.dart';
import 'package:kachra_user_app/src/domain/repositories/site_visit_repository/site_visit_repository.dart';

class SiteVisitRepositoryImpl extends SiteVisitRepository{
  final SiteVisitScheduleApiService siteVisitScheduleApiService;

  SiteVisitRepositoryImpl(this.siteVisitScheduleApiService);

  @override
  Future<DataState<SiteVisitScheduleModel>> createSiteVisitSchedule(SiteVisitScheduleParams? params) async {
    try {
      final httpResponse = await siteVisitScheduleApiService.createSiteVisitSchedule(params!.scheduledDate, params.userId, params.address);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

}