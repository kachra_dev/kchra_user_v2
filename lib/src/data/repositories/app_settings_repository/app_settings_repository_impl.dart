import 'dart:io';

import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/data/datasources/app_settings/app_settings_api_service.dart';
import 'package:kachra_user_app/src/data/models/app_settings/app_settings_model.dart';
import 'package:kachra_user_app/src/domain/repositories/app_settings_repository/app_settings_repository.dart';

class AppSettingsRepositoryImpl extends AppSettingsRepository{
  final AppSettingsApiService appSettingsApiService;

  AppSettingsRepositoryImpl(this.appSettingsApiService);

  @override
  Future<DataState<AppSettingsModel>> getAppSettings() async {
    try {
      final httpResponse = await appSettingsApiService.getAppSettings();

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}