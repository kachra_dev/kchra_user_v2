import 'dart:io';

import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/params/auth_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/core/utils/hive/get_hive.dart';
import 'package:kachra_user_app/src/data/datasources/auth/auth_api_service.dart';
import 'package:kachra_user_app/src/data/models/auth/auth_model.dart';
import 'package:kachra_user_app/src/data/models/auth/register_auth_model.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/domain/repositories/user_repository/user_repository.dart';

class UserRepositoryImpl extends UserRepository {
  final AuthApiService authApiService;

  UserRepositoryImpl(this.authApiService);

  @override
  Future<DataState<AuthModel>> login(AuthParams? params) async {
    try {
      final httpResponse = await authApiService.login(params!.email, params.password, params.activationCode);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      } else if (httpResponse.response.statusCode == HttpStatus.unauthorized){
        return DataUnauthorized(
          DioError(
            error: httpResponse.response.statusMessage,
            response: httpResponse.response,
            requestOptions: httpResponse.response.requestOptions,
          ),
        );
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<RegisterAuthModel>> register(RegisterAuthParams? registerAuthParams) async {
    try {
      final httpResponse = await authApiService.register(
          registerAuthParams!.name,
          registerAuthParams.email,
          registerAuthParams.password,
          registerAuthParams.passwordConfirmation,
          registerAuthParams.address,
          registerAuthParams.phoneNumber,
          registerAuthParams.country,
          registerAuthParams.role
      );

      if (httpResponse.response.statusCode == HttpStatus.ok || httpResponse.response.statusCode == HttpStatus.created) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<AuthModel>> loginGoogle(String? params) async {
    try {
      final httpResponse = await authApiService.loginGoogle(params!);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<RegisterAuthModel>> registerUsingGoogle(RegisterUsingGoogleAuthParams? registerUsingGoogleAuthParams) async {
    try {
      final httpResponse = await authApiService.registerUsingGoogle(
          registerUsingGoogleAuthParams!.name,
          registerUsingGoogleAuthParams.email,
          registerUsingGoogleAuthParams.address,
          registerUsingGoogleAuthParams.phoneNumber,
          registerUsingGoogleAuthParams.country,
          registerUsingGoogleAuthParams.role
      );

      if (httpResponse.response.statusCode == HttpStatus.ok || httpResponse.response.statusCode == HttpStatus.created) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<AuthModel>> registerAsGuest(RegisterAsGuestAuthParams? registerAsGuestAuthParams) async {
    try {
      final httpResponse = await authApiService.registerAsGuest(registerAsGuestAuthParams!.name, registerAsGuestAuthParams.address, registerAsGuestAuthParams.phoneNumber);

      if (httpResponse.response.statusCode == HttpStatus.ok || httpResponse.response.statusCode == HttpStatus.created) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<AuthModel>> updateUserPhoneNumber(String? phoneNumber) async {
    try {
      ProfileHive profile = await GetHive().getCurrentUserProfile();
      final httpResponse = await authApiService.updateUserPhoneNumber(profile.id!, phoneNumber);

      if (httpResponse.response.statusCode == HttpStatus.ok || httpResponse.response.statusCode == HttpStatus.created) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<AuthModel>> loginPhoneNumber(String? phoneNumber) async {
    try {
      final httpResponse = await authApiService.loginPhoneNumber(phoneNumber);

      if (httpResponse.response.statusCode == HttpStatus.ok || httpResponse.response.statusCode == HttpStatus.created) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}
