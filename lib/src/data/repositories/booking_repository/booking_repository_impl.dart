import 'dart:io';

import 'package:dio/dio.dart';
import 'package:kachra_user_app/src/core/params/booking_params.dart';
import 'package:kachra_user_app/src/core/resources/data_state.dart';
import 'package:kachra_user_app/src/data/datasources/booking/booking_api_service.dart';
import 'package:kachra_user_app/src/data/models/booking/booking_model.dart';
import 'package:kachra_user_app/src/data/models/booking/list_booking_model.dart';
import 'package:kachra_user_app/src/data/models/general_response.dart';
import 'package:kachra_user_app/src/data/models/notifications/notifications_model.dart';
import 'package:kachra_user_app/src/domain/repositories/booking_repository/booking_repository.dart';

class BookingRepositoryImpl extends BookingRepository{
  final BookingApiService bookingApiService;

  BookingRepositoryImpl(this.bookingApiService);

  @override
  Future<DataState<BookingModel>> createBooking(BookingParams? params) async {
    try {
      final httpResponse = await bookingApiService.createBooking(params!.userId, params.bookingStatus, params.address, params.asset, params.assetType, params.bookingDetails, params.bookingPurpose);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<List<ListBookingModel>>> getBookings(String? userId) async {
    try {
      final httpResponse = await bookingApiService.getBookings(userId);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<GeneralResponse>> acceptDeclineBooking(AcceptDeclineBookingParams? params) async {
    try {
      final httpResponse = await bookingApiService.acceptDeclineBooking(params!.bookingId, params.bookingQuoteStatus);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<GeneralResponse>> setPaymentMethod(SetPaymentBookingParams? params) async {
    try {
      final httpResponse = await bookingApiService.setPaymentMethod(params!.bookingId, params.paymentMethod);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<BookingModel>> findBooking(String? params) async {
    try {
      final httpResponse = await bookingApiService.findBooking(params);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<List<NotificationsModel>>> getNotifications(String? userId) async {
    try {
      final httpResponse = await bookingApiService.getNotifications(userId);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<BookingModel>> setPreferredPickupDate(SetPreferredPickupDateParams? params) async {
    try {
      final httpResponse = await bookingApiService.setPreferredPickupDate(params!.bookingId, params.preferredPickupDate);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }

  @override
  Future<DataState<BookingModel>> changeBookingPurpose(ChangeBookingPurposeParams? params) async {
    try {
      final httpResponse = await bookingApiService.changeBookingPurpose(params!.bookingId, params.bookingPurpose);

      if (httpResponse.response.statusCode == HttpStatus.ok) {
        return DataSuccess(httpResponse.data);
      }

      return DataFailed(
        DioError(
          error: httpResponse.response.statusMessage,
          response: httpResponse.response,
          requestOptions: httpResponse.response.requestOptions,
        ),
      );
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}