import 'package:kachra_user_app/src/data/models/auth/address_model.dart';

class SiteVisitScheduleParams{
  final String? scheduledDate;
  final String? userId;
  final AddressModel? address;

  SiteVisitScheduleParams({this.scheduledDate, this.userId, this.address});
}