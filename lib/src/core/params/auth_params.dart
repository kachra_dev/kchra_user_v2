import 'package:kachra_user_app/src/data/models/auth/address_model.dart';

class AuthParams {
  final String email;
  final String password;
  final String? activationCode;

  AuthParams({required this.email, required this.password, this.activationCode});
}

class RegisterAuthParams {
  final String name;
  final String email;
  final String password;
  final String passwordConfirmation;
  final AddressModel address;
  final String phoneNumber;
  final String country;
  final String role;

  RegisterAuthParams(this.name, this.email, this.password,
      this.passwordConfirmation, this.address, this.phoneNumber, this.country, this.role);
}

class RegisterUsingGoogleAuthParams {
  final String name;
  final String email;
  final AddressModel address;
  final String phoneNumber;
  final String country;
  final String role;

  RegisterUsingGoogleAuthParams(this.name, this.email, this.address, this.phoneNumber, this.country, this.role);
}

class RegisterAsGuestAuthParams {
  final String name;
  final AddressModel address;
  final String? phoneNumber;

  RegisterAsGuestAuthParams(this.name, this.address, this.phoneNumber);
}
