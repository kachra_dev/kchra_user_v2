import 'package:flutter/cupertino.dart';
import 'package:kachra_user_app/src/data/models/auth/address_model.dart';

class BookingParams{
  final String? userId;
  final String? bookingStatus;
  final AddressModel? address;
  final String? asset;
  final String? assetType;
  final String? bookingDetails;
  final String? bookingPurpose;

  BookingParams({this.userId, this.bookingStatus, this.address, this.asset, this.assetType, this.bookingDetails, this.bookingPurpose});
}

class AcceptDeclineBookingParams{
  final BuildContext context;
  final String? bookingId;
  final String? bookingQuoteStatus;
  final int? amount;

  AcceptDeclineBookingParams({required this.context, this.bookingId, this.bookingQuoteStatus, this.amount});
}

class SetPaymentBookingParams{
  final String? bookingId;
  final String? paymentMethod;

  SetPaymentBookingParams({this.bookingId, this.paymentMethod});
}

class SetPreferredPickupDateParams{
  final String? bookingId;
  final String? preferredPickupDate;

  SetPreferredPickupDateParams({this.bookingId, this.preferredPickupDate});
}

class ChangeBookingPurposeParams{
  final String? bookingId;
  final String? bookingPurpose;

  ChangeBookingPurposeParams({this.bookingId, this.bookingPurpose});
}