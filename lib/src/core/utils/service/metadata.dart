class UserMetadata {
  final String? creationTime;
  final String? lastSignInTime;

  UserMetadata(this.creationTime, this.lastSignInTime);
}