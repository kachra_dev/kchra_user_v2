import 'package:hive/hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';

class EditHive{
  final userBox = Hive.box('userProfile');

  void editUser(ProfileHive profileHive){
    userBox.putAt(0, profileHive);
  }
}