import 'package:hive/hive.dart';

class DeleteHive{
  final userSavedAddressBox = Hive.box('userSavedAddresses');

  Future<void> deleteUserSavedAddress(int index) async {
    userSavedAddressBox.deleteAt(index);
  }
}