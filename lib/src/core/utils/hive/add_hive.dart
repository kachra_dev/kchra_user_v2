import 'package:hive/hive.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/locally_saved_booking_assets_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';

class AddHive{
  final userBox = Hive.box('userProfile');
  final userSavedAddressBox = Hive.box('userSavedAddresses');
  final locallySavedBookingAssetsBox = Hive.box('locallySavedBookingAssets');
  final appAssetBox = Hive.box('appAsset');

  void addUser(ProfileHive profileHive){
    userBox.add(profileHive);
  }

  void addUserSavedAddress(UserSavedAddressHive userSavedAddressHive){
    userSavedAddressBox.add(userSavedAddressHive);
  }

  void addLocallySavedBookingAsset(LocallySavedBookingAssetsHive locallySavedBookingAssetsHive){
    locallySavedBookingAssetsBox.add(locallySavedBookingAssetsHive);
  }

  void addAppAsset(AppAssetHive appAssetHive){
    if(appAssetBox.isNotEmpty){
      appAssetBox.putAt(0, appAssetHive);
    } else {
      appAssetBox.add(appAssetHive);
    }
  }
}