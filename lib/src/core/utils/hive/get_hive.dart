import 'package:hive/hive.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/locally_saved_booking_assets_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';

class GetHive{
  final userBox = Hive.box('userProfile');
  final userSavedAddressBox = Hive.box('userSavedAddresses');
  final locallySavedBookingAssetsBox = Hive.box('locallySavedBookingAssets');
  final appAssetBox = Hive.box('appAsset');

  Future<ProfileHive> getCurrentUserProfile() async {
    return userBox.getAt(0) as ProfileHive;
  }

  Future<List<UserSavedAddressHive>> getSavedAddressesOfCurrentUser() async {
    ProfileHive profile = await userBox.getAt(0) as ProfileHive;

    List<dynamic> savedAddresses = userSavedAddressBox.values.toList();
    List<UserSavedAddressHive> savedAddressesParsed = [];

    for(var address in savedAddresses){
      UserSavedAddressHive savedAddress = address as UserSavedAddressHive;

      if(savedAddress.userId == profile.id){
        savedAddressesParsed.add(savedAddress);
      }
    }

    return savedAddressesParsed;
  }

  Future<LocallySavedBookingAssetsHive?> getLocallySavedBookingAsset(String bookingId) async {
    List<dynamic> savedBookingAssets = locallySavedBookingAssetsBox.values.toList();

    for(var asset in savedBookingAssets){
      LocallySavedBookingAssetsHive locallySavedBookingAssetsHive = asset as LocallySavedBookingAssetsHive;

      if(locallySavedBookingAssetsHive.bookingId == bookingId){
          return locallySavedBookingAssetsHive;
      }
    }
  }

  Future<AppAssetHive?> getAppAssets() async {
    if(appAssetBox.isNotEmpty){
      return appAssetBox.getAt(0) as AppAssetHive;
    }
  }
}