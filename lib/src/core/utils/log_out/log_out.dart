import 'package:hive/hive.dart';
import 'package:kachra_user_app/src/core/utils/service/firebase_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogOut{
  Future<void> logOut() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    ///remove accessToken
    sharedPreferences.remove('accessToken');

    ///remove saved profile
    final userBox = Hive.box('userProfile');
    if(userBox.isNotEmpty){
      userBox.deleteAt(0);
    }

    //sign out from Google Sign In, if necessary
    await FirebaseService().signOutFromGoogle();
  }
}