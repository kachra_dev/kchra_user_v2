import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class TempValueHolder{
  final String bookingId;

  TempValueHolder(this.bookingId);
}

class TempValueHolderHelper{
  static ValueNotifier<TempValueHolder> tempValue = ValueNotifier<TempValueHolder>(TempValueHolder(''));
}

// class TempValueDatetimeCurrentValueHolder{
//   final DateTime currentValue;
//
//   TempValueDatetimeCurrentValueHolder(this.currentValue);
// }
//
// class TempValueDatetimeCurrentValueHolderHelper{
//   static ValueNotifier<TempValueDatetimeCurrentValueHolder> tempValue = ValueNotifier<TempValueDatetimeCurrentValueHolder>(TempValueDatetimeCurrentValueHolder(DateTime.now()));
// }