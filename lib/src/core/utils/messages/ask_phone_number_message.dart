import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/core/utils/helper/text_checker.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';

class AskPhoneNumberMessage extends StatelessWidget {
  const AskPhoneNumberMessage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController phoneNumberController = TextEditingController();

    return AlertDialog(
      title: Text(LocaleKeys.enter_phone_number.tr(),
        style: const TextStyle(fontWeight: FontWeight.w900),
      ),
      content: SizedBox(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset('assets/img/error.png', scale: 3.0),
              const SizedBox(height: 20.0),
              Text(LocaleKeys.please_add_your_phone_number_before_booking.tr(),
                style: const TextStyle(fontWeight: FontWeight.w500),
              ),
              const SizedBox(height: 30.0),
              Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 0, 30.0, 20.0),
                child: Directionality(
                  textDirection: ltrTextDirection,
                  child: TextFormField(
                    controller: phoneNumberController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: LocaleKeys.phone_number.tr(),
                        labelStyle: TextStyle(
                            color: Colors.black, fontSize: 10.0.sp),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(
                            color: Colors.grey,
                          ),
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return LocaleKeys.please_enter_some_text.tr();
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      actions: [
        BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if(state is AuthLoadingState){
              return const CircularProgressIndicator();
            } else {
              return TextButton(
                  onPressed: () {
                    String phoneNumber = '+974${phoneNumberController.text}';
                    final updatePhoneNumberBloc = BlocProvider.of<AuthBloc>(context);
                    updatePhoneNumberBloc.add(UpdateUserPhoneNumberEvent(phoneNumber));
                  },
                  child: Text(LocaleKeys.add_phone_number.tr())
              );
            }
          }
        ),
        BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if(state is AuthLoadingState){
              return const SizedBox(height: 0);
            } else {
              return TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text(LocaleKeys.cancel.tr())
              );
            }
          }
        )
      ],
    );
  }
}
