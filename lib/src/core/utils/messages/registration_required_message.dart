import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/views/unauthenticated/registration/registration_modal_sheet.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class RegistrationRequiredMessage extends StatelessWidget {
  const RegistrationRequiredMessage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Registration required',
        style: TextStyle(fontWeight: FontWeight.w900),
      ),
      content: SizedBox(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset('assets/img/error.png', scale: 3.0),
              const SizedBox(height: 20.0),
              const Text(
                'Please register to get full access of this app.',
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12.0),
              )
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(LocaleKeys.close.tr())
        )
      ],
    );
  }
}
