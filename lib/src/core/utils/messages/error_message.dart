import 'package:flutter/material.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

class ErrorMessage extends StatelessWidget {
  final String title;
  final String content;
  final Widget? errorMessage;
  // final Function onPressedFunction;

  const ErrorMessage(
      {Key? key,
      required this.title,
      required this.content,
      this.errorMessage,
      // required this.onPressedFunction
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        title,
        style: const TextStyle(fontWeight: FontWeight.w900),
      ),
      content: SizedBox(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset('assets/img/error.png', scale: 3.0),
              const SizedBox(height: 20.0),
              Text(
                content,
                style: const TextStyle(fontWeight: FontWeight.w500),
              ),
              const SizedBox(height: 30.0),
              errorMessage ?? const SizedBox(height: 0)
            ],
          ),
        ),
      ),
      // actions: [
      //   TextButton(
      //       onPressed: () {
      //         onPressedFunction();
      //       },
      //       child: Text(LocaleKeys.close.tr()))
      // ],
    );
  }
}
