import 'package:shared_preferences/shared_preferences.dart';

class GetPreference{
  Future<String?> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('accessToken');
  }

  Future<bool?> getSaveToPhoneSetting() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('saveToPhone');
  }

  Future<bool?> getIsForRemoval() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('isForRemoval');
  }
}