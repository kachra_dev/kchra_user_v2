import 'package:shared_preferences/shared_preferences.dart';

class StorePreference{
  Future<void> storeAccessToken(String accessToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('accessToken', accessToken);
  }

  Future<void> storeSaveToPhoneSetting(bool saveToPhone) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('saveToPhone', saveToPhone);
  }

  Future<void> storeIsForRemoval(bool isForRemoval) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isForRemoval', isForRemoval);
  }
}