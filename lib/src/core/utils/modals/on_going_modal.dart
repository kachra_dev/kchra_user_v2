import 'package:flutter/material.dart';

class OnGoingModalFit extends StatelessWidget {
  const OnGoingModalFit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        child: SafeArea(
          top: false,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const ListTile(
                title: Center(child: Text('What do you want to do?')),
              ),
              ListTile(
                title: const Text('Create Delivery Boy'),
                leading: const Icon(Icons.add),
                onTap: () {

                },
              ),
              ListTile(
                title: const Text('View All Delivery Boys'),
                leading: const Icon(Icons.source),
                onTap: () async {

                },
              ),
            ],
          ),
        ));
  }
}
