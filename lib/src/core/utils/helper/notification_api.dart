import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:kachra_user_app/main.dart';
import 'package:rxdart/rxdart.dart';

class FlutterLocalNotificationModel{
  final String title;
  final String body;
  final String? payload;
  FlutterLocalNotificationModel({required this.title, required this.body, this.payload});
}

class NotificationApi{
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  static BehaviorSubject<String?> onNotifications = BehaviorSubject<String?>();

  Future<void> initNotification({bool initScheduled = false}) async {
    const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/launcher_icon');
    const IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings();
    const InitializationSettings initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(
        initializationSettings,
        onSelectNotification: (payload) async {
          onNotifications.add(payload);
        }
    );
  }

  Future<void> showNotification(FlutterLocalNotificationModel flutterLocalNotificationModel) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
    AndroidNotificationDetails(
        'kchra',
        'kchra-channel',
        channelDescription: 'Kchra Notifications for Drivers',
        importance: Importance.max,
        priority: Priority.high,
        ticker: 'ticker',
    );
    const NotificationDetails platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      flutterLocalNotificationModel.title,
      flutterLocalNotificationModel.body,
      platformChannelSpecifics,
      payload: flutterLocalNotificationModel.payload,
    );
  }
}