class GetOtherLocation{
  final bool? isCurrentLocation;
  final bool? isBluePlateNumber;
  final double? latitude;
  final double? longitude;
  final String? name;
  final double? height;
  final double? width;
  final String? apartmentNumber;
  final String? buildingNumber;
  final String? streetNumber;
  final String? city;
  final String? zoneNumber;

  GetOtherLocation({this.isCurrentLocation, this.isBluePlateNumber, this.latitude, this.longitude, this.name, this.height, this.width, this.apartmentNumber, this.buildingNumber, this.streetNumber, this.city, this.zoneNumber});
}