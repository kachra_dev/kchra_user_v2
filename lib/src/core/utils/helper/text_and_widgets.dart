import 'package:emojis/emojis.dart';
import 'package:flutter/material.dart';
import 'package:kachra_user_app/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

const String confirmEmoji = Emojis.thinkingFace;
const String yesEmoji = Emojis.checkBoxWithCheck;
const String noEmoji = Emojis.crossMark;

Widget confirmWidget = Text('${LocaleKeys.confirm.tr()} $confirmEmoji');
Widget yesWidget = Text('${LocaleKeys.yes.tr()} $yesEmoji');
Widget noWidget = Text('${LocaleKeys.no.tr()} $noEmoji');