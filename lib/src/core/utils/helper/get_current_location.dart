import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';

class GetCurrentLocation{
  final Position location;
  final Placemark place;

  GetCurrentLocation({required this.location, required this.place});
}

class GetLocation{
  Future<GetCurrentLocation> getLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    Position location = await Geolocator.getCurrentPosition();
    List<Placemark> p = await placemarkFromCoordinates(
        location.latitude, location.longitude);
    Placemark place = p[0];

    return GetCurrentLocation(
        location: location,
        place: place
    );
  }
}