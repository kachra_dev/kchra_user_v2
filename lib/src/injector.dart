import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:kachra_user_app/src/data/datasources/app_settings/app_settings_api_service.dart';
import 'package:kachra_user_app/src/data/datasources/auth/auth_api_service.dart';
import 'package:kachra_user_app/src/data/datasources/booking/booking_api_service.dart';
import 'package:kachra_user_app/src/data/datasources/site_visit_schedule/site_visit_schedule_api_service.dart';
import 'package:kachra_user_app/src/data/repositories/app_settings_repository/app_settings_repository_impl.dart';
import 'package:kachra_user_app/src/data/repositories/booking_repository/booking_repository_impl.dart';
import 'package:kachra_user_app/src/data/repositories/site_visit_schedule_repository/site_visit_schedule_repository_impl.dart';
import 'package:kachra_user_app/src/data/repositories/user_repository/user_repository_impl.dart';
import 'package:kachra_user_app/src/domain/repositories/booking_repository/booking_repository.dart';
import 'package:kachra_user_app/src/domain/repositories/site_visit_repository/site_visit_repository.dart';
import 'package:kachra_user_app/src/domain/repositories/user_repository/user_repository.dart';
import 'package:kachra_user_app/src/domain/usecases/app_settings_usecase.dart';
import 'package:kachra_user_app/src/domain/usecases/booking_usecase.dart';
import 'package:kachra_user_app/src/domain/usecases/site_visit_repository_usecase.dart';
import 'package:kachra_user_app/src/domain/usecases/user_usecase.dart';
import 'package:kachra_user_app/src/presentation/bloc/app_settings/app_settings_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/accept_decline_booking/accept_decline_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/change_booking_purpose/change_booking_purpose_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_notifications/get_notifications_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_payment_method/set_payment_method_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_preferred_pickup_date/set_preferred_pickup_date_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/site_visit_schedule/site_visit_schedule_bloc.dart';

import 'domain/repositories/app_settings_repository/app_settings_repository.dart';

final injector = GetIt.instance;

Future<void> initializeDependencies() async {
  // Dio client
  injector.registerSingleton<Dio>(Dio());

  // Dependencies and API services
  injector.registerSingleton<AuthApiService>(AuthApiService(injector()));
  injector.registerSingleton<BookingApiService>(BookingApiService(injector()));
  injector.registerSingleton<SiteVisitScheduleApiService>(SiteVisitScheduleApiService(injector()));
  injector.registerSingleton<AppSettingsApiService>(AppSettingsApiService(injector()));

  //Repositories
  injector.registerSingleton<UserRepository>(UserRepositoryImpl(injector()));
  injector.registerSingleton<BookingRepository>(BookingRepositoryImpl(injector()));
  injector.registerSingleton<SiteVisitRepository>(SiteVisitRepositoryImpl(injector()));
  injector.registerSingleton<AppSettingsRepository>(AppSettingsRepositoryImpl(injector()));

  // UseCases
  injector.registerSingleton<UserLoginUseCase>(UserLoginUseCase(injector()));
  injector.registerSingleton<UserRegisterUseCase>(UserRegisterUseCase(injector()));
  injector.registerSingleton<CreateBookingUseCase>(CreateBookingUseCase(injector()));
  injector.registerSingleton<GetBookingsUseCase>(GetBookingsUseCase(injector()));
  injector.registerSingleton<AcceptDeclineBookingUseCase>(AcceptDeclineBookingUseCase(injector()));
  injector.registerSingleton<SetPaymentMethodBookingUseCase>(SetPaymentMethodBookingUseCase(injector()));
  injector.registerSingleton<FindBookingUseCase>(FindBookingUseCase(injector()));
  injector.registerSingleton<UserLoginGoogleUseCase>(UserLoginGoogleUseCase(injector()));
  injector.registerSingleton<UserRegisterUsingGoogleUseCase>(UserRegisterUsingGoogleUseCase(injector()));
  injector.registerSingleton<GetNotificationsUseCase>(GetNotificationsUseCase(injector()));
  injector.registerSingleton<CreateSiteVisitScheduleUseCase>(CreateSiteVisitScheduleUseCase(injector()));
  injector.registerSingleton<SetPreferredPickupDateUseCase>(SetPreferredPickupDateUseCase(injector()));
  injector.registerSingleton<AppSettingsUseCase>(AppSettingsUseCase(injector()));
  injector.registerSingleton<UserRegisterAsGuestUseCase>(UserRegisterAsGuestUseCase(injector()));
  injector.registerSingleton<UpdatePhoneNumberUseCase>(UpdatePhoneNumberUseCase(injector()));
  injector.registerSingleton<LoginPhoneNumberUseCase>(LoginPhoneNumberUseCase(injector()));
  injector.registerSingleton<ChangeBookingPurposeUseCase>(ChangeBookingPurposeUseCase(injector()));

  // Blocs
  injector.registerFactory<AuthBloc>(() => AuthBloc(injector(), injector(), injector(), injector(), injector(), injector(), injector()));
  injector.registerFactory<BookingBloc>(() => BookingBloc(injector(), injector()));
  injector.registerFactory<GetBookingBloc>(() => GetBookingBloc(injector()));
  injector.registerFactory<AcceptDeclineBookingBloc>(() => AcceptDeclineBookingBloc(injector()));
  injector.registerFactory<SetPaymentMethodBloc>(() => SetPaymentMethodBloc(injector()));
  injector.registerFactory<FindBookingBloc>(() => FindBookingBloc(injector()));
  injector.registerFactory<GetNotificationsBloc>(() => GetNotificationsBloc(injector()));
  injector.registerFactory<SiteVisitScheduleBloc>(() => SiteVisitScheduleBloc(injector()));
  injector.registerFactory<SetPreferredPickupDateBloc>(() => SetPreferredPickupDateBloc(injector()));
  injector.registerFactory<AppSettingsBloc>(() => AppSettingsBloc(injector()));
  injector.registerFactory<ChangeBookingPurposeBloc>(() => ChangeBookingPurposeBloc(injector()));
}
