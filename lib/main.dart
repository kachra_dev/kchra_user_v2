import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:hive/hive.dart';
import 'package:kachra_user_app/src/config/routes/app_routes.dart';
import 'package:kachra_user_app/src/config/themes/app_theme.dart';
import 'package:kachra_user_app/src/core/utils/constants.dart';
import 'package:kachra_user_app/src/data/models/hive/address_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/app_asset_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/locally_saved_booking_assets_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/profile_hive.dart';
import 'package:kachra_user_app/src/data/models/hive/user_saved_address_hive.dart';
import 'package:kachra_user_app/src/injector.dart';
import 'package:kachra_user_app/src/presentation/bloc/app_settings/app_settings_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/auth/auth_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/accept_decline_booking/accept_decline_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/change_booking_purpose/change_booking_purpose_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/find_booking/find_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_booking/get_booking_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/get_notifications/get_notifications_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_payment_method/set_payment_method_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/booking/set_preferred_pickup_date/set_preferred_pickup_date_bloc.dart';
import 'package:kachra_user_app/src/presentation/bloc/site_visit_schedule/site_visit_schedule_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:kachra_user_app/src/core/utils/.env.dart';
import 'translations/codegen_loader.g.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDir.path);
  await initializeDependencies();

  ///Register Hive TypeAdapters
  Hive.registerAdapter(ProfileHiveAdapter());
  Hive.registerAdapter(AddressHiveAdapter());
  Hive.registerAdapter(UserSavedAddressHiveAdapter());
  Hive.registerAdapter(LocallySavedBookingAssetsHiveAdapter());
  Hive.registerAdapter(AppAssetHiveAdapter());

  ///initialize stripe settings
  Stripe.publishableKey = stripePublishableKey;
  Stripe.merchantIdentifier = 'merchant.flutter.stripe.test';
  Stripe.urlScheme = 'flutterstripe';
  await Stripe.instance.applySettings();

  ///initialize firebase
  await Firebase.initializeApp();

  ///initialize easy localization
  await EasyLocalization.ensureInitialized();

  ///initialize flutter downloader
  await FlutterDownloader.initialize();

  runApp(EasyLocalization(
      supportedLocales: const [
        Locale('en'),
        Locale('ar')
      ],
      path: 'assets/translations', // <-- change the path of the translation files
      fallbackLocale: const Locale('en'),
      assetLoader: const CodegenLoader(),
      child: const MyApp()
    )
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_MyAppState>()!.restartApp();
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<AuthBloc>(
              create: (_) => injector()..add(const GetStoredAuthEvent())),
          BlocProvider<BookingBloc>(
              create: (_) => injector()..add(const InitialBookingEvent())),
          BlocProvider<GetBookingBloc>(
              create: (_) => injector()..add(const GetBookingsEvent())),
          BlocProvider<AcceptDeclineBookingBloc>(
              create: (_) => injector()..add(const AcceptDeclineEvent())),
          BlocProvider<SetPaymentMethodBloc>(
              create: (_) => injector()..add(const SetPaymentEvent(bookingModel: null, setPaymentBookingParams: null))),
          BlocProvider<FindBookingBloc>(
              create: (_) => injector()..add(const FindBooking(null))),
          BlocProvider<GetNotificationsBloc>(
              create: (_) => injector()..add(const GetBookingNotificationsEvent())),
          BlocProvider<SiteVisitScheduleBloc>(
              create: (_) => injector()..add(const CreateSiteVisitScheduleEvent(null))),
          BlocProvider<SetPreferredPickupDateBloc>(
              create: (_) => injector()..add(const SetPreferredDateEvent(null))),
          BlocProvider<AppSettingsBloc>(
              create: (_) => injector()..add(const GetAppSettingsEvent())),
          BlocProvider<ChangeBookingPurposeBloc>(
              create: (_) => injector()..add(const ChangePurposeEvent(null))),
        ],
        child: Sizer(builder: (context, orientation, deviceType) {
          return KeyedSubtree(
            key: key,
            child: WillPopScope(
              onWillPop:() async => false,
              child: MaterialApp(
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                locale: context.locale,
                debugShowCheckedModeBanner: false,
                title: kMaterialAppTitle,
                onGenerateRoute: AppRoutes.onGenerateRoutes,
                theme: AppTheme.light,
                navigatorKey: MyApp.navigatorKey,
              ),
            ),
          );
        }));
  }
}
